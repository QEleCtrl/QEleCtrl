#ifndef QELEINFODLG_H
#define QELEINFODLG_H

#include <QDialog>
#include <QMutex>
#include "config.h"
#include "types.h"
#include "util.h"

namespace Ui {
class QEleInfoDlg;
}

class QEleInfoDlg : public QDialog
{
    Q_OBJECT

public:
    explicit QEleInfoDlg(QWidget *parent = nullptr);
    void destroyDlg();
    ~QEleInfoDlg();

private:
    Ui::QEleInfoDlg *ui;
private:
    QMutex eleInfoStatuMtx;
    QMutex eleInfoParaMtx;
    QMutex eleInfoReserveMtx;
    bool   bDestroy;
private:
    void initEleInfoDlg();

private slots:
    void showStatus(int index,LS_U8 *buf);
    void showPara(int index,LS_U8 *buf);
    void showReserve(int index,float *fValue,int iCount);
};

#endif // QELEINFODLG_H
