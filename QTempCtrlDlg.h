#ifndef QTEMPCTRLDLG_H
#define QTEMPCTRLDLG_H

#include <QDialog>
#include "types.h"
#include "config.h"
#include "util.h"
#include "temProto.h"
#include "sendoutBuffer.h"

namespace Ui {
class QTempCtrlDlg;
}

class QTempCtrlDlg : public QDialog
{
    Q_OBJECT

public:
    explicit QTempCtrlDlg(QWidget *parent = nullptr);
    ~QTempCtrlDlg();

private:
    Ui::QTempCtrlDlg *ui;

private:
    TTemArg tGlobalTempArg;
    Ele_Ctrl_Dev    tTempDev;

signals:
    void signalTempMsg(QString str,MsgType type=MSG_ERR);

private:
    void initTempDlg();
    int sendCtrlCmd(void *buf,int len);
    int sendCtrlCmd_index(int index,void *buf,int len);

public:
    void setTempDev(Ele_Ctrl_Dev mDev);
    void QTempShowOutVoltage(int index,double fValue);
    void QTempShowOutCurrent(int index,double fValue);
    void QTempShowProCurrent(int index,double fValue);
    void QTempShowProVoltage(int index,double fValue);
private slots:
    void on_PB_check_protect_current0_clicked();
    void on_PB_check_protect_voltage0_clicked();
    void on_PB_check_protect_current1_clicked();
    void on_PB_check_protect_voltage1_clicked();
    void on_PB_temp_set_clicked();
    void on_PB_protect_current_clicked();
    void on_PB_portect_voltage_clicked();
    void on_PB_protect_temp_hh_clicked();
    void on_RB_temp_output_on_clicked();
    void on_RB_temp_output_off_clicked();
    void on_PB_protect_temp_ll_clicked();
};

#endif // QTEMPCTRLDLG_H
