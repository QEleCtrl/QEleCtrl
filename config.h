#ifndef _CONFIG_H_
#define _CONFIG_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <QDebug>
#include "QApplication"
#include "util.h"
#include "types.h"
#include "etc.h"
typedef struct TEleSysArg_{
    int		iSysValue[13];
}TEleSysArg;

typedef struct TEleSwitch_{
    bool	bSwitch[13];
}TEleSwitch;

typedef struct TTemArg_{
    int		iTempValue[6];
}TTemArg;

enum OperaINI{SET_OPERA=0,GET_OPERA};

int operaSoftTime(enum OperaINI mOpera,LS_U32 *mValue);

int operaFDBTime(Ele_Ctrl_Dev devid,enum OperaINI mOpera,LS_U32 *mValue);

int operaSaveDataCount(Ele_Ctrl_Dev devid,enum OperaINI mOpera,LS_U32 *mValue);

int operaCheckCount(Ele_Ctrl_Dev devid,enum OperaINI mOpera,LS_U32 *mValue);

int operaSoftRect(enum OperaINI mOpera,TSoftRect *tSoftRect);

int operaSavePath(enum OperaINI mOpera,LS_S8 *path);

int operaSysArg(enum OperaINI mOpera,TSysSet *tSysArg);

int operaEleSysArg(Ele_Ctrl_Dev devid,enum OperaINI mOpera,TEleSysArg *tArg,int index = -1);

int operaEleSwitch(Ele_Ctrl_Dev devid,enum OperaINI mOpera,TEleSwitch *tEleSwitch,int index=-1);

int operaTemArg(Ele_Ctrl_Dev devid,enum OperaINI mOpera,TTemArg *tArg,int index=-1);
#endif
