#ifndef QELESWITCHDLG_H
#define QELESWITCHDLG_H

#include <QDialog>
#include <QListView>
#include "types.h"
#include "config.h"
#include "util.h"
#include "sendoutBuffer.h"
#include "eleProto.h"

namespace Ui {
class QEleSwitchDlg;
}

class QEleSwitchDlg : public QDialog
{
    Q_OBJECT

public:
    explicit QEleSwitchDlg(QWidget *parent = 0);
    ~QEleSwitchDlg();

private:
    Ui::QEleSwitchDlg *ui;

private:
    Ele_Ctrl_Dev    eEleSysDev;
    TEleSwitch      tGlobalEleSwitch;
    TSysSet         tSysPara;

public:
    void setCtrlDev(Ele_Ctrl_Dev mDev);
    void setGlobalSysPara(TSysSet *para);
private:
    void initEleSwitchDlg();
    int sendCtrlCmd(LS_U8 *buf,int len);
    void setEleSwitchDlgShow(TEleSwitch *para);
    void setEleDevOnOff(EleSwitch,bool onoff);
signals:
    void signalEleSwitchMsg(QString str,MsgType type=MSG_ERR);

public:
private slots:
    void on_RB_loop_pump_on_clicked();
    void on_RB_loop_pump_off_clicked();
    void on_RB_boost_pump_on_clicked();
    void on_RB_boost_pump_off_clicked();
    void on_RB_vacuum_pump_on_clicked();
    void on_RB_vacuum_pump_off_clicked();
    void on_RB_dfb_laser_on_clicked();
    void on_RB_dfb_laser_off_clicked();
    void on_RB_temp_ctrl_on_clicked();
    void on_RB_temp_ctrl_off_clicked();
    void on_RB_gate1_on_clicked();
    void on_RB_gate1_off_clicked();
    void on_RB_gate2_on_clicked();
    void on_RB_gate2_off_clicked();
    void on_RB_gate3_on_clicked();
    void on_RB_gate3_off_clicked();
    void on_RB_solenoid1_on_clicked();
    void on_RB_solenoid1_off_clicked();
    void on_RB_solenoid2_on_clicked();
    void on_RB_solenoid2_off_clicked();
    void on_RB_solenoid3_on_clicked();
    void on_RB_solenoid3_off_clicked();
    void on_RB_solenoid4_on_clicked();
    void on_RB_solenoid4_off_clicked();
    void on_RB_data_collect_on_clicked();
    void on_RB_data_collect_off_clicked();
};

#endif // QELESWITCHDLG_H
