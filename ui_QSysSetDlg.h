/********************************************************************************
** Form generated from reading UI file 'QSysSetDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QSYSSETDLG_H
#define UI_QSYSSETDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_QSysSetDlg
{
public:
    QGridLayout *gridLayout_3;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QComboBox *CB_ele_dev0_com;
    QLabel *label_3;
    QComboBox *CB_ele_temp0_com;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QComboBox *CB_ele_dev1_com;
    QLabel *label_2;
    QLabel *label_5;
    QComboBox *CB_ele_temp1_com;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_7;
    QComboBox *CB_GPS_COM;
    QHBoxLayout *horizontalLayout;
    QPushButton *PB_save_path;
    QLineEdit *LE_save_path;
    QSpacerItem *horizontalSpacer;
    QPushButton *PB_SYSSET;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *QSysSetDlg)
    {
        if (QSysSetDlg->objectName().isEmpty())
            QSysSetDlg->setObjectName(QString::fromUtf8("QSysSetDlg"));
        QSysSetDlg->setWindowModality(Qt::ApplicationModal);
        QSysSetDlg->resize(377, 442);
        QSysSetDlg->setMinimumSize(QSize(0, 0));
        QSysSetDlg->setMaximumSize(QSize(65535, 65535));
        QSysSetDlg->setFocusPolicy(Qt::NoFocus);
        QSysSetDlg->setModal(true);
        gridLayout_3 = new QGridLayout(QSysSetDlg);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_2 = new QGroupBox(QSysSetDlg);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 20, -1, -1);
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        CB_ele_dev0_com = new QComboBox(groupBox_2);
        CB_ele_dev0_com->setObjectName(QString::fromUtf8("CB_ele_dev0_com"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CB_ele_dev0_com->sizePolicy().hasHeightForWidth());
        CB_ele_dev0_com->setSizePolicy(sizePolicy);
        CB_ele_dev0_com->setMinimumSize(QSize(0, 30));
        CB_ele_dev0_com->setIconSize(QSize(64, 64));

        gridLayout_2->addWidget(CB_ele_dev0_com, 0, 1, 1, 1);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 1, 0, 1, 1);

        CB_ele_temp0_com = new QComboBox(groupBox_2);
        CB_ele_temp0_com->setObjectName(QString::fromUtf8("CB_ele_temp0_com"));
        sizePolicy.setHeightForWidth(CB_ele_temp0_com->sizePolicy().hasHeightForWidth());
        CB_ele_temp0_com->setSizePolicy(sizePolicy);
        CB_ele_temp0_com->setMinimumSize(QSize(0, 30));
        CB_ele_temp0_com->setIconSize(QSize(64, 64));

        gridLayout_2->addWidget(CB_ele_temp0_com, 1, 1, 1, 1);


        verticalLayout->addWidget(groupBox_2);

        groupBox = new QGroupBox(QSysSetDlg);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(-1, 20, -1, -1);
        CB_ele_dev1_com = new QComboBox(groupBox);
        CB_ele_dev1_com->setObjectName(QString::fromUtf8("CB_ele_dev1_com"));
        sizePolicy.setHeightForWidth(CB_ele_dev1_com->sizePolicy().hasHeightForWidth());
        CB_ele_dev1_com->setSizePolicy(sizePolicy);
        CB_ele_dev1_com->setMinimumSize(QSize(0, 30));
        CB_ele_dev1_com->setIconSize(QSize(64, 64));

        gridLayout->addWidget(CB_ele_dev1_com, 0, 1, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 1, 0, 1, 1);

        CB_ele_temp1_com = new QComboBox(groupBox);
        CB_ele_temp1_com->setObjectName(QString::fromUtf8("CB_ele_temp1_com"));
        sizePolicy.setHeightForWidth(CB_ele_temp1_com->sizePolicy().hasHeightForWidth());
        CB_ele_temp1_com->setSizePolicy(sizePolicy);
        CB_ele_temp1_com->setMinimumSize(QSize(0, 30));
        CB_ele_temp1_com->setIconSize(QSize(64, 64));

        gridLayout->addWidget(CB_ele_temp1_com, 1, 1, 1, 1);


        verticalLayout->addWidget(groupBox);

        groupBox_3 = new QGroupBox(QSysSetDlg);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        horizontalLayout_2 = new QHBoxLayout(groupBox_3);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 20, -1, -1);
        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(label_7);

        CB_GPS_COM = new QComboBox(groupBox_3);
        CB_GPS_COM->setObjectName(QString::fromUtf8("CB_GPS_COM"));
        sizePolicy.setHeightForWidth(CB_GPS_COM->sizePolicy().hasHeightForWidth());
        CB_GPS_COM->setSizePolicy(sizePolicy);
        CB_GPS_COM->setMinimumSize(QSize(0, 30));
        CB_GPS_COM->setIconSize(QSize(64, 64));

        horizontalLayout_2->addWidget(CB_GPS_COM);


        verticalLayout->addWidget(groupBox_3);

        verticalLayout->setStretch(0, 2);
        verticalLayout->setStretch(1, 2);
        verticalLayout->setStretch(2, 1);

        gridLayout_3->addLayout(verticalLayout, 0, 0, 1, 3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        PB_save_path = new QPushButton(QSysSetDlg);
        PB_save_path->setObjectName(QString::fromUtf8("PB_save_path"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(PB_save_path->sizePolicy().hasHeightForWidth());
        PB_save_path->setSizePolicy(sizePolicy2);
        PB_save_path->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout->addWidget(PB_save_path);

        LE_save_path = new QLineEdit(QSysSetDlg);
        LE_save_path->setObjectName(QString::fromUtf8("LE_save_path"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(LE_save_path->sizePolicy().hasHeightForWidth());
        LE_save_path->setSizePolicy(sizePolicy3);

        horizontalLayout->addWidget(LE_save_path);


        gridLayout_3->addLayout(horizontalLayout, 1, 0, 1, 3);

        horizontalSpacer = new QSpacerItem(121, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 2, 0, 1, 1);

        PB_SYSSET = new QPushButton(QSysSetDlg);
        PB_SYSSET->setObjectName(QString::fromUtf8("PB_SYSSET"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(PB_SYSSET->sizePolicy().hasHeightForWidth());
        PB_SYSSET->setSizePolicy(sizePolicy4);
        PB_SYSSET->setMinimumSize(QSize(100, 30));
        PB_SYSSET->setMaximumSize(QSize(100, 65535));
        PB_SYSSET->setSizeIncrement(QSize(100, 30));

        gridLayout_3->addWidget(PB_SYSSET, 2, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(120, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_2, 2, 2, 1, 1);


        retranslateUi(QSysSetDlg);

        QMetaObject::connectSlotsByName(QSysSetDlg);
    } // setupUi

    void retranslateUi(QDialog *QSysSetDlg)
    {
        QSysSetDlg->setWindowTitle(QApplication::translate("QSysSetDlg", "\350\275\257\344\273\266\350\256\276\347\275\256", nullptr));
        groupBox_2->setTitle(QApplication::translate("QSysSetDlg", "\350\256\276\345\244\2071\347\253\257\345\217\243\350\256\276\347\275\256", nullptr));
        label->setText(QApplication::translate("QSysSetDlg", "\350\256\276\345\244\2071\347\224\265\346\216\247\347\253\257\345\217\243", nullptr));
        label_3->setText(QApplication::translate("QSysSetDlg", "\350\256\276\345\244\2071\346\270\251\346\216\247\347\253\257\345\217\243", nullptr));
        groupBox->setTitle(QApplication::translate("QSysSetDlg", "\350\256\276\345\244\2072\347\253\257\345\217\243\350\256\276\347\275\256", nullptr));
        label_2->setText(QApplication::translate("QSysSetDlg", "\350\256\276\345\244\2072\347\224\265\346\216\247\347\253\257\345\217\243", nullptr));
        label_5->setText(QApplication::translate("QSysSetDlg", "\350\256\276\345\244\2072\346\270\251\346\216\247\347\253\257\345\217\243", nullptr));
        groupBox_3->setTitle(QApplication::translate("QSysSetDlg", "GPS\347\253\257\345\217\243\350\256\276\347\275\256", nullptr));
        label_7->setText(QApplication::translate("QSysSetDlg", "\351\200\211\346\213\251GPS\347\253\257\345\217\243", nullptr));
        PB_save_path->setText(QApplication::translate("QSysSetDlg", "\345\255\230\345\202\250\347\233\256\345\275\225", nullptr));
        PB_SYSSET->setText(QApplication::translate("QSysSetDlg", "\350\256\276\347\275\256", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QSysSetDlg: public Ui_QSysSetDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QSYSSETDLG_H
