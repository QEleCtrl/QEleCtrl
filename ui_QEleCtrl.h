/********************************************************************************
** Form generated from reading UI file 'QEleCtrl.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QELECTRL_H
#define UI_QELECTRL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "QcwIndicatorLamp.h"
#include "qchartview.h"

QT_BEGIN_NAMESPACE

class Ui_QEleCtrl
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QChartView *GV_CHART_0;
    QChartView *GV_CHART_1;
    QGroupBox *GB_Ctrl;
    QGridLayout *gridLayout_2;
    QRadioButton *RB_ELE_0;
    QRadioButton *RB_ELE_1;
    QRadioButton *RB_ELE_BOTH;
    QHBoxLayout *horizontalLayout_2;
    QcwIndicatorLamp *LA_dev0_online_light;
    QLabel *LA_dev0_online;
    QHBoxLayout *horizontalLayout_3;
    QcwIndicatorLamp *LA_dev1_online_light;
    QLabel *LA_dev1_online;
    QHBoxLayout *horizontalLayout_4;
    QcwIndicatorLamp *LA_temp0_online_light;
    QLabel *LA_temp0_value;
    QHBoxLayout *horizontalLayout_5;
    QcwIndicatorLamp *LA_temp1_online_light;
    QLabel *LA_temp1_value;
    QTextEdit *TE_SYS_MSG;
    QHBoxLayout *horizontalLayout;
    QLabel *LA_saveCount_0;
    QLabel *LA_checkCount_0;
    QLabel *LA_saveCount_1;
    QLabel *LA_checkCount_1;
    QLabel *LA_BD_LA;
    QLabel *LA_BD_LO;
    QLabel *LA_SOFT_RUN;

    void setupUi(QWidget *QEleCtrl)
    {
        if (QEleCtrl->objectName().isEmpty())
            QEleCtrl->setObjectName(QString::fromUtf8("QEleCtrl"));
        QEleCtrl->resize(847, 596);
        QEleCtrl->setMinimumSize(QSize(0, 0));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush1(QColor(240, 240, 240, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        QEleCtrl->setPalette(palette);
        gridLayout = new QGridLayout(QEleCtrl);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(1);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        GV_CHART_0 = new QChartView(QEleCtrl);
        GV_CHART_0->setObjectName(QString::fromUtf8("GV_CHART_0"));

        verticalLayout->addWidget(GV_CHART_0);

        GV_CHART_1 = new QChartView(QEleCtrl);
        GV_CHART_1->setObjectName(QString::fromUtf8("GV_CHART_1"));

        verticalLayout->addWidget(GV_CHART_1);


        gridLayout->addLayout(verticalLayout, 0, 0, 6, 1);

        GB_Ctrl = new QGroupBox(QEleCtrl);
        GB_Ctrl->setObjectName(QString::fromUtf8("GB_Ctrl"));
        GB_Ctrl->setMinimumSize(QSize(200, 120));
        GB_Ctrl->setMaximumSize(QSize(200, 120));
        gridLayout_2 = new QGridLayout(GB_Ctrl);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 20, -1, -1);
        RB_ELE_0 = new QRadioButton(GB_Ctrl);
        RB_ELE_0->setObjectName(QString::fromUtf8("RB_ELE_0"));
        RB_ELE_0->setMinimumSize(QSize(180, 30));

        gridLayout_2->addWidget(RB_ELE_0, 0, 0, 1, 1);

        RB_ELE_1 = new QRadioButton(GB_Ctrl);
        RB_ELE_1->setObjectName(QString::fromUtf8("RB_ELE_1"));
        RB_ELE_1->setMinimumSize(QSize(180, 30));

        gridLayout_2->addWidget(RB_ELE_1, 1, 0, 1, 1);

        RB_ELE_BOTH = new QRadioButton(GB_Ctrl);
        RB_ELE_BOTH->setObjectName(QString::fromUtf8("RB_ELE_BOTH"));
        RB_ELE_BOTH->setMinimumSize(QSize(180, 30));

        gridLayout_2->addWidget(RB_ELE_BOTH, 2, 0, 1, 1);


        gridLayout->addWidget(GB_Ctrl, 0, 1, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        LA_dev0_online_light = new QcwIndicatorLamp(QEleCtrl);
        LA_dev0_online_light->setObjectName(QString::fromUtf8("LA_dev0_online_light"));
        LA_dev0_online_light->setMinimumSize(QSize(30, 30));
        LA_dev0_online_light->setMaximumSize(QSize(30, 30));

        horizontalLayout_2->addWidget(LA_dev0_online_light);

        LA_dev0_online = new QLabel(QEleCtrl);
        LA_dev0_online->setObjectName(QString::fromUtf8("LA_dev0_online"));

        horizontalLayout_2->addWidget(LA_dev0_online);


        gridLayout->addLayout(horizontalLayout_2, 1, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        LA_dev1_online_light = new QcwIndicatorLamp(QEleCtrl);
        LA_dev1_online_light->setObjectName(QString::fromUtf8("LA_dev1_online_light"));
        LA_dev1_online_light->setMinimumSize(QSize(30, 30));
        LA_dev1_online_light->setMaximumSize(QSize(30, 30));

        horizontalLayout_3->addWidget(LA_dev1_online_light);

        LA_dev1_online = new QLabel(QEleCtrl);
        LA_dev1_online->setObjectName(QString::fromUtf8("LA_dev1_online"));

        horizontalLayout_3->addWidget(LA_dev1_online);


        gridLayout->addLayout(horizontalLayout_3, 2, 1, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        LA_temp0_online_light = new QcwIndicatorLamp(QEleCtrl);
        LA_temp0_online_light->setObjectName(QString::fromUtf8("LA_temp0_online_light"));
        LA_temp0_online_light->setMinimumSize(QSize(30, 30));
        LA_temp0_online_light->setMaximumSize(QSize(30, 30));

        horizontalLayout_4->addWidget(LA_temp0_online_light);

        LA_temp0_value = new QLabel(QEleCtrl);
        LA_temp0_value->setObjectName(QString::fromUtf8("LA_temp0_value"));
        LA_temp0_value->setMinimumSize(QSize(30, 30));
        LA_temp0_value->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_4->addWidget(LA_temp0_value);


        gridLayout->addLayout(horizontalLayout_4, 3, 1, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        LA_temp1_online_light = new QcwIndicatorLamp(QEleCtrl);
        LA_temp1_online_light->setObjectName(QString::fromUtf8("LA_temp1_online_light"));
        LA_temp1_online_light->setMinimumSize(QSize(30, 30));
        LA_temp1_online_light->setMaximumSize(QSize(30, 30));

        horizontalLayout_5->addWidget(LA_temp1_online_light);

        LA_temp1_value = new QLabel(QEleCtrl);
        LA_temp1_value->setObjectName(QString::fromUtf8("LA_temp1_value"));
        LA_temp1_value->setMinimumSize(QSize(0, 30));
        LA_temp1_value->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_5->addWidget(LA_temp1_value);


        gridLayout->addLayout(horizontalLayout_5, 4, 1, 1, 1);

        TE_SYS_MSG = new QTextEdit(QEleCtrl);
        TE_SYS_MSG->setObjectName(QString::fromUtf8("TE_SYS_MSG"));
        TE_SYS_MSG->setMinimumSize(QSize(200, 0));
        TE_SYS_MSG->setMaximumSize(QSize(200, 16777215));
        TE_SYS_MSG->setReadOnly(true);

        gridLayout->addWidget(TE_SYS_MSG, 5, 1, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        LA_saveCount_0 = new QLabel(QEleCtrl);
        LA_saveCount_0->setObjectName(QString::fromUtf8("LA_saveCount_0"));

        horizontalLayout->addWidget(LA_saveCount_0);

        LA_checkCount_0 = new QLabel(QEleCtrl);
        LA_checkCount_0->setObjectName(QString::fromUtf8("LA_checkCount_0"));
        LA_checkCount_0->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(LA_checkCount_0);

        LA_saveCount_1 = new QLabel(QEleCtrl);
        LA_saveCount_1->setObjectName(QString::fromUtf8("LA_saveCount_1"));
        LA_saveCount_1->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(LA_saveCount_1);

        LA_checkCount_1 = new QLabel(QEleCtrl);
        LA_checkCount_1->setObjectName(QString::fromUtf8("LA_checkCount_1"));
        LA_checkCount_1->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(LA_checkCount_1);

        LA_BD_LA = new QLabel(QEleCtrl);
        LA_BD_LA->setObjectName(QString::fromUtf8("LA_BD_LA"));
        LA_BD_LA->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(LA_BD_LA);

        LA_BD_LO = new QLabel(QEleCtrl);
        LA_BD_LO->setObjectName(QString::fromUtf8("LA_BD_LO"));
        LA_BD_LO->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(LA_BD_LO);

        LA_SOFT_RUN = new QLabel(QEleCtrl);
        LA_SOFT_RUN->setObjectName(QString::fromUtf8("LA_SOFT_RUN"));
        LA_SOFT_RUN->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(LA_SOFT_RUN);


        gridLayout->addLayout(horizontalLayout, 6, 0, 1, 2);


        retranslateUi(QEleCtrl);

        QMetaObject::connectSlotsByName(QEleCtrl);
    } // setupUi

    void retranslateUi(QWidget *QEleCtrl)
    {
        QEleCtrl->setWindowTitle(QApplication::translate("QEleCtrl", "\347\224\265\346\216\247\347\273\210\347\253\257", nullptr));
        GB_Ctrl->setTitle(QApplication::translate("QEleCtrl", "\346\216\247\345\210\266", nullptr));
        RB_ELE_0->setText(QApplication::translate("QEleCtrl", "\350\256\276\345\244\2071", nullptr));
        RB_ELE_1->setText(QApplication::translate("QEleCtrl", "\350\256\276\345\244\2072", nullptr));
        RB_ELE_BOTH->setText(QApplication::translate("QEleCtrl", "\345\220\214\346\227\266\346\216\247\345\210\266", nullptr));
        LA_dev0_online_light->setText(QString());
        LA_dev0_online->setText(QApplication::translate("QEleCtrl", "\350\256\276\345\244\2071\347\246\273\347\272\277", nullptr));
        LA_dev1_online_light->setText(QString());
        LA_dev1_online->setText(QApplication::translate("QEleCtrl", "\350\256\276\345\244\2072\347\246\273\347\272\277", nullptr));
        LA_temp0_online_light->setText(QString());
        LA_temp0_value->setText(QApplication::translate("QEleCtrl", "\350\256\276\345\244\2071\346\270\251\345\272\246:0.00", nullptr));
        LA_temp1_online_light->setText(QString());
        LA_temp1_value->setText(QApplication::translate("QEleCtrl", "\350\256\276\345\244\2072\346\270\251\345\272\246:0.00", nullptr));
        LA_saveCount_0->setText(QApplication::translate("QEleCtrl", "TextLabel", nullptr));
        LA_checkCount_0->setText(QApplication::translate("QEleCtrl", "TextLabel", nullptr));
        LA_saveCount_1->setText(QApplication::translate("QEleCtrl", "TextLabel", nullptr));
        LA_checkCount_1->setText(QApplication::translate("QEleCtrl", "TextLabel", nullptr));
        LA_BD_LA->setText(QApplication::translate("QEleCtrl", "TextLabel", nullptr));
        LA_BD_LO->setText(QApplication::translate("QEleCtrl", "TextLabel", nullptr));
        LA_SOFT_RUN->setText(QApplication::translate("QEleCtrl", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QEleCtrl: public Ui_QEleCtrl {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QELECTRL_H
