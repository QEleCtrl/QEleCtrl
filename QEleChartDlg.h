#ifndef QELECHARTDLG_H
#define QELECHARTDLG_H

#include <QDialog>
#include <QtCharts>
#include <QTimer>
#include <QMutex>
#include <QContextMenuEvent>
#include <QtCharts/QSplineSeries>
#include "types.h"

QT_CHARTS_USE_NAMESPACE

namespace Ui {
class QEleChartDlg;
}

class QEleChartDlg : public QDialog
{
    Q_OBJECT

public:
    explicit QEleChartDlg(QWidget *parent = nullptr);
    void destroyDlg();
    ~QEleChartDlg();

private:
    Ui::QEleChartDlg *ui;

private:
    QChart          *eleChart[2];
    QSplineSeries   *eleSeries[2];
    QMutex           eleChartsMtx;
    bool             bDestroy;
    double          chartMaxValue[ELE_MAX_NUM];
    double          chartMinValue[ELE_MAX_NUM];

private:
    void initEleChart();
    void initEleChartDlg();

public:
    void eleChartDrawLines(int index,double *dValue);
signals:
    void signalEleChartMsg(QString str,MsgType type=MSG_ERR);
};

#endif // QELECHARTDLG_H
