#ifndef QCWINDCCATORLAMP_H
#define QCWINDCCATORLAMP_H

#include <QWidget>
#include <QLabel>
class QColor;

typedef enum {
    Alarm_none,
    Alarm_green,
    Alarm_yellow,
    Alarm_red,
    Alarm_gray,
} AlarmType;

class Q_DECL_EXPORT QcwIndicatorLamp: public QLabel
{
 Q_OBJECT
    Q_PROPERTY(AlarmType alarm READ isAlarm WRITE setAlarm);
	
public:    
    explicit QcwIndicatorLamp(QWidget *parent = 0);
    AlarmType isAlarm() const {return m_alarm;}
		       	            
Q_SIGNALS:

public Q_SLOTS:
    void setAlarm(AlarmType type);

protected:
	void paintEvent(QPaintEvent *);
  
public:
    AlarmType m_alarm;
};

#endif
