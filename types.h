#ifndef TYPES_H
#define TYPES_H
#include <QDebug>
#include <QColor>
typedef char                LS_S8;
typedef unsigned char       LS_U8;
typedef short               LS_S16;
typedef unsigned short      LS_U16;
typedef int                 LS_S32;
typedef unsigned int        LS_U32;
typedef long long           LS_S64;
typedef unsigned long long  LS_U64;
typedef float               LS_F32;
typedef double              LS_F64;
typedef void                LS_VOID;

#define LS_SUCCESS  0
#define LS_FAILURE  (-1)
#define LS_SUCC     LS_SUCCESS
#define LS_FAIL     LS_FAILURE

#define LISHI_DEBUG
#ifdef LISHI_DEBUG
#define ls_info_debug(...) printf("\033[32m [%s %s line:%d]  ", __FILE__,__func__,__LINE__);printf(__VA_ARGS__);printf("\033[0m\n");fflush(stdout);
#define ls_err_debug(...) printf("\033[31m [%s %s line:%d]  ", __FILE__,__func__,__LINE__);printf(__VA_ARGS__);printf("\033[0m\n");fflush(stdout);
#else
#define	ls_info_debug(...)
#endif
/*显示控件样式*/
#define LABEL_STYLE         ("QLabel{font-family:华文仿宋;color:#000000;font-size:15px;padding:10px;}")
/*按钮控件样式*/
#define BUTTON_STYLE        ("QPushButton{font-family:华文仿宋;background:#AAAAAA;font-size:18px;\
                            color:#080808;   border-radius: 10px;  border: 2px groove gray; border-style: outset;}" \
                            "QPushButton:pressed{background-color:rgb(85, 170, 255); border-style: inset; }")

#define CONCENT_MAX_VLUAE   (1.0)
#define CONCENT_MIN_VLUAE   (0)

#define ELE_MAX_NUM             2           //设备数量
#define ELE_CURVE_WIDTH         2           //曲线宽度
#ifndef DATA_COUNT
#define DATA_COUNT              8000        //单次数据采样次数
#endif

#define MAX_DATA_VALUE          12000

#ifndef DATA_LEN
#define DATA_LEN		(DATA_COUNT*2+10)
#endif

#ifndef MAX_PATH
#define MAX_PATH    260
#endif

typedef struct _LS_U_DATA_PACKET {
    LS_U8              *data;       // 数据地址
    LS_S32              size;       // 数据长度
} LS_U8_DATA_PACKET;


typedef struct _LS_S_DATA_PACKET {
    LS_S8              *data;       // 数据地址
    LS_S32              size;       // 数据长度
} LS_S8_DATA_PACKET;

typedef struct _LS_U8_OUT_PACKET {
    LS_S32             devid;   // 数据地址
    LS_U8              *data;       // 数据地址
    LS_S32              size;       // 数据长度
} LS_U8_OUT_PACKET;

typedef struct _LS_S8_OUT_PACKET {
    LS_S32             devid;   // 数据地址
    LS_S8              *data;       // 数据地址
    LS_S32              size;       // 数据长度
} LS_S8_OUT_PACKET;

typedef struct __TRect
{
    LS_S32 x;
    LS_S32 y;
    LS_S32 w;
    LS_S32 h;
}TRect;

typedef struct __TSoftRect
{
    TRect eleRect[6];
}TSoftRect;


typedef enum {
    DLG_ele_ctr,
    Dlg_ele_switch,
    Dlg_ele_sys_ctrl,
    Dlg_ele_sys_set,
    Dlg_ele_chart,
    Dlg_ele_info
} softDlg;

typedef struct __TSysSet
{
    LS_S32	eleDevPort[ELE_MAX_NUM];
    LS_S32	eleTempPort[ELE_MAX_NUM];
    LS_S32  gpsPort;
}TSysSet;

typedef enum {
    Ele_DevID_0,
    Ele_DevID_1,
    Ele_DevID_All
} Ele_Ctrl_Dev;

typedef enum {
    MSG_ERR=0,
    MSG_INFO=1
} MsgType;

typedef enum {
    Ele_ld_para,
    Ele_las_tem,
    Ele_throld,
    Ele_preshh,
    Ele_presll,
    Ele_coll_count,
    Ele_volta_a,
    Ele_volta_b,
    Ele_volta_c,
    Ele_judge,
    Ele_workmode,
    Ele_rankse_vol,
    Ele_rankse_value,
    Ele_pumpai_start,
    Ele_pumpai_stop,
    Ele_dev_start,
    Ele_dev_stop,
    Ele_save_para,
    Ele_dev_reset,
    Ele_request_para,
    Ele_request_status
}EleSysPara;

typedef enum {
    Ele_boost_pump,
    Ele_vacuum_pump,
    Ele_solenoid1,
    Ele_solenoid2,
    Ele_solenoid3,
    Ele_solenoid4,
    Ele_gate1,
    Ele_gate2,
    Ele_gate3,
    Ele_loop_pump,
    Ele_data_collect,
    Ele_temp_ctrl,
    Ele_dfb_laser
}EleSwitch;

typedef enum {
    Temp_output,
    Temp_set_value,
    Temp_pro_current,
    Temp_pro_voltage,
    Temp_temp_hh,
    Temp_temp_ll
}Temp_ctrl;

#define DLG_BK_COLOR                    QColor(248,248,248)// QColor(74,112,139)      //软件背景色
#define MSG_ERROR_COLOR                 QColor(255,0,0)         //错误信息字体颜色
#define MSG_INFO_COLOR                  QColor(0,0,0)         //普通信息字体颜色


#endif // TYPES_H
