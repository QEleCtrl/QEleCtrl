#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>

//#include "netdefine.h"
#include "etc.h"

void
__mg_rewind (FILE * fp)
{
#if 1
	fseek (fp, 0, SEEK_SET);
#else
	rewind (fp);
#endif
}

FILE *
__mg_tmpfile (void)
{
	return fopen ("./mg-etc-tmp", "w+");
}

int
__mg_close_tmpfile (FILE * tmp_fp)
{
	return fclose (tmp_fp);
}

/****************************** ETC file support ******************************/

static char *
get_section_name (char *section_line)
{
	char *current;
	char *name;

	if (!section_line)
		return NULL;

	current = section_line;

	while (*current == ' ' || *current == '\t')
		current++;

	if (*current == ';' || *current == '#')
		return NULL;

	if (*current++ == '[')
		while (*current == ' ' || *current == '\t')
			current++;
	else
		return NULL;

	name = current;
	while (*current != ']' && *current != '\n' &&
		   *current != ';' && *current != '#' && *current != '\0')
		current++;
	*current = '\0';
	while (*current == ' ' || *current == '\t')
	  {
		  *current = '\0';
		  current--;
	  }

	return name;
}

static int
get_key_value (char *key_line, char **mykey, char **myvalue)
{
	char *current;
	char *tail;
	char *value;

	if (!key_line)
		return -1;

	current = key_line;

	while (*current == ' ' || *current == '\t')
		current++;

	if (*current == ';' || *current == '#')
		return -1;

	if (*current == '[')
		return 1;

	if (*current == '\n' || *current == '\0')
		return -1;

	tail = current;
	while (*tail != '=' && *tail != '\n' &&
		   *tail != ';' && *tail != '#' && *tail != '\0')
		tail++;

	value = tail + 1;
	if (*tail != '=')
		*value = '\0';

	*tail-- = '\0';
	while (*tail == ' ' || *tail == '\t')
	  {
		  *tail = '\0';
		  tail--;
	  }

	tail = value;
	while (*tail != '\n' && *tail != '\0')
		tail++;
	*tail = '\0';

	if (mykey)
		*mykey = current;
	if (myvalue)
		*myvalue = value;

	return 0;
}

/* This function locate the specified section in the etc file. */
static int
etc_LocateSection (FILE * fp, const char *pSection, FILE * bak_fp)
{
	char szBuff[ETC_MAXLINE + 1];
	char *name;

	while (TRUE)
	  {
		  if (!fgets (szBuff, ETC_MAXLINE, fp))
			{
				if (feof (fp))
					return ETC_SECTIONNOTFOUND;
				else
					return ETC_FILEIOFAILED;
			}
		  else if (bak_fp && fputs (szBuff, bak_fp) == EOF)
			  return ETC_FILEIOFAILED;

		  name = get_section_name (szBuff);
		  if (!name)
			  continue;

		  if (strcmp (name, pSection) == 0)
			  return ETC_OK;
	  }

	return ETC_SECTIONNOTFOUND;
}

/* This function locate the specified key in the etc file. */
static int
etc_LocateKeyValue (FILE * fp, const char *pKey,
                    ETC_BOOL bCurSection, char *pValue, int iLen,
					FILE * bak_fp, char *nextSection)
{
	char szBuff[ETC_MAXLINE + 1 + 1];
	char *current;
	char *value;
	int ret;

	while (TRUE)
	  {
		  int bufflen;

		  if (!fgets (szBuff, ETC_MAXLINE, fp))
			  return ETC_FILEIOFAILED;
		  bufflen = strlen (szBuff);
		  if (szBuff[bufflen - 1] == '\n')
			  szBuff[bufflen - 1] = '\0';

		  ret = get_key_value (szBuff, &current, &value);
		  if (ret < 0)
			  continue;
		  else if (ret > 0)
			{
				fseek (fp, -bufflen, SEEK_CUR);
				return ETC_KEYNOTFOUND;
			}

		  if (strcmp (current, pKey) == 0)
			{
				if (pValue)
					strncpy (pValue, value, iLen);

				return ETC_OK;
			}
		  else if (bak_fp && *current != '\0')
			{
#if	0
//FIXME, unknown problem with fprintf
				char tmp_nam[256];
				sprintf (tmp_nam, "%s=%s\n", pKey, pValue);
				fputs (tmp_nam, bak_fp);
#else
				fprintf (bak_fp, "%s=%s\n", current, value);
#endif
			}
	  }

	return ETC_KEYNOTFOUND;
}

/* Function: GetValueFromEtcFile(const char* pEtcFile, const char* pSection,
 *                               const char* pKey, char* pValue, int iLen);
 * Parameter:
 *     pEtcFile: etc file path name.
 *     pSection: Section name.
 *     pKey:     Key name.
 *     pValue:   The buffer will store the value of the key.
 *     iLen:     The max length of value string.
 * Return:
 *     int               meaning
 *     ETC_FILENOTFOUND           The etc file not found. 
 *     ETC_SECTIONNOTFOUND        The section is not found. 
 *     ETC_EKYNOTFOUND        The Key is not found.
 *     ETC_OK            OK.
 */
int
GetValueFromEtcFile (const char *pEtcFile, const char *pSection,
					 const char *pKey, char *pValue, int iLen)
{
	FILE *fp;
	char tempSection[ETC_MAXLINE + 2];

	if (!(fp = fopen (pEtcFile, "r")))
		return ETC_FILENOTFOUND;

	if (pSection)
		if (etc_LocateSection (fp, pSection, NULL) != ETC_OK)
		  {
			  fclose (fp);
			  return ETC_SECTIONNOTFOUND;
		  }

	if (etc_LocateKeyValue (fp, pKey, pSection != NULL,
							pValue, iLen, NULL, tempSection) != ETC_OK)
	  {
		  fclose (fp);
		  return ETC_KEYNOTFOUND;
	  }

	fclose (fp);
	return ETC_OK;
}


/* Function: GetIntValueFromEtcFile(const char* pEtcFile, const char* pSection,
 *                               const char* pKey);
 * Parameter:
 *     pEtcFile: etc file path name.
 *     pSection: Section name.
 *     pKey:     Key name.
 * Return:
 *     int                      meaning
 *     ETC_FILENOTFOUND             The etc file not found. 
 *     ETC_SECTIONNOTFOUND          The section is not found. 
 *     ETC_EKYNOTFOUND              The Key is not found.
 *     ETC_OK                       OK.
 */
int
GetIntValueFromEtcFile (const char *pEtcFile, const char *pSection,
						const char *pKey, int *value)
{
	int ret;
	char szBuff[51];

	ret = GetValueFromEtcFile (pEtcFile, pSection, pKey, szBuff, 50);
	if (ret < 0)
		return ret;

	*value = strtol (szBuff, NULL, 0);
	if ((*value == LONG_MIN || *value == LONG_MAX) && errno == ERANGE)
		return ETC_INTCONV;

	return ETC_OK;
}

static int
etc_CopyAndLocate (FILE * etc_fp, FILE * tmp_fp,
				   const char *pSection, const char *pKey, char *tempSection)
{
	if (pSection && etc_LocateSection (etc_fp, pSection, tmp_fp) != ETC_OK)
		return ETC_SECTIONNOTFOUND;

	if (etc_LocateKeyValue (etc_fp, pKey, pSection != NULL,
							NULL, 0, tmp_fp, tempSection) != ETC_OK)
		return ETC_KEYNOTFOUND;

	return ETC_OK;
}

static int
etc_FileCopy (FILE * sf, FILE * df)
{
	char line[ETC_MAXLINE + 1];

	while (fgets (line, ETC_MAXLINE + 1, sf) != NULL)
		if (fputs (line, df) == EOF)
		  {
			  return ETC_FILEIOFAILED;
		  }

	return ETC_OK;
}


/* Function: SetValueToEtcFile(const char* pEtcFile, const char* pSection,
 *                               const char* pKey, char* pValue);
 * Parameter:
 *     pEtcFile: etc file path name.
 *     pSection: Section name.
 *     pKey:     Key name.
 *     pValue:   Value.
 * Return:
 *     int                      meaning
 *     ETC_FILENOTFOUND         The etc file not found.
 *     ETC_TMPFILEFAILED        Create tmp file failure.
 *     ETC_OK                   OK.
 */
int
SetValueToEtcFile (const char *pEtcFile, const char *pSection,
				   const char *pKey, char *pValue)
{
	FILE *etc_fp;
	FILE *tmp_fp;
	int rc;
	char tempSection[ETC_MAXLINE + 2] = { 0 };

	if ((tmp_fp = __mg_tmpfile ()) == NULL)
		return ETC_TMPFILEFAILED;

	if (!(etc_fp = fopen (pEtcFile, "r+")))
	  {
		  __mg_close_tmpfile (tmp_fp);

		  if (!(etc_fp = fopen (pEtcFile, "w")))
			{
				return ETC_FILEIOFAILED;
			}
		  fprintf (etc_fp, "[%s]\n", pSection);
		  fprintf (etc_fp, "%s=%s\n", pKey, pValue);
		  fclose (etc_fp);
		  return ETC_OK;
	  }

	switch (etc_CopyAndLocate (etc_fp, tmp_fp, pSection, pKey, tempSection))
	  {
	  case ETC_SECTIONNOTFOUND:
		  fprintf (tmp_fp, "\n[%s]\n", pSection);
		  fprintf (tmp_fp, "%s=%s\n", pKey, pValue);
		  break;

	  case ETC_KEYNOTFOUND:
		  fprintf (tmp_fp, "%s=%s\n", pKey, pValue);
		  fprintf (tmp_fp, "%s\n", tempSection);
		  break;

	  default:
#if 0
//FIXME, unknown problem with fprintf
		  {
			  char tmp_nam[256];
			  sprintf (tmp_nam, "%s=%s\n", pKey, pValue);
			  fputs (tmp_nam, tmp_fp);
		  }
#else
		  fprintf (tmp_fp, "%s=%s\n", pKey, pValue);
#endif
		  break;
	  }

	if ((rc = etc_FileCopy (etc_fp, tmp_fp)) != ETC_OK)
		goto error;

	// replace etc content with tmp file content
	// truncate etc content first
	fclose (etc_fp);
	if (!(etc_fp = fopen (pEtcFile, "w")))
	  {
		  __mg_close_tmpfile (tmp_fp);
		  return ETC_FILEIOFAILED;
	  }

	__mg_rewind (tmp_fp);
	rc = etc_FileCopy (tmp_fp, etc_fp);

  error:
	fclose (etc_fp);
	__mg_close_tmpfile (tmp_fp);
	return rc;
}

static void etc_NewKeyValue (PETCSECTION psect, 
                const char* key, const char* value)
{
    if (psect->key_nr == psect->key_nr_alloc) {
        psect->key_nr_alloc += NR_KEYS_INC_ALLOC;
        psect->keys = (char **)realloc (psect->keys, 
                            sizeof (char*) * psect->key_nr_alloc);
        psect->values = (char **)realloc (psect->values, 
                            sizeof (char*) * psect->key_nr_alloc);
    }

    psect->keys [psect->key_nr] = strdup (key);
    psect->values [psect->key_nr] = strdup (value);
    psect->key_nr ++;
}

static int etc_ReadSection (FILE* fp, PETCSECTION psect)
{
    char szBuff[ETC_MAXLINE + 1 + 1];
    char* sect_name;

    psect->name = NULL;
    psect->key_nr = 0;
    psect->keys = NULL;
    psect->values = NULL;

    while (TRUE) {
        int bufflen;

        if (!fgets(szBuff, ETC_MAXLINE, fp)) {
            if (feof (fp)) {
                if (psect->name)
                    break;
                else
                    return ETC_SECTIONNOTFOUND;
            }
            else
                return ETC_FILEIOFAILED;
        }

        bufflen = strlen (szBuff);
        if (szBuff [bufflen - 1] == '\n')
            szBuff [bufflen - 1] = '\0';

        if (!psect->name) { /* read section name */
            sect_name = get_section_name (szBuff);
            if (!sect_name)
                continue;

            psect->name = strdup (sect_name); 
			psect->key_nr = 0;
            psect->key_nr_alloc = NR_KEYS_INIT_ALLOC;
            psect->keys = (char **)malloc (sizeof (char*) * NR_KEYS_INIT_ALLOC);
            psect->values = (char **)malloc (sizeof (char*) * NR_KEYS_INIT_ALLOC);
        }
        else { /* read key and value */
            int ret;
            char *key, *value;

            ret = get_key_value (szBuff, &key, &value);
            if (ret < 0)
                continue;
            else if (ret > 0) {  /* another section begins */
                fseek (fp, -bufflen, SEEK_CUR);
                break;
            }

            etc_NewKeyValue (psect, key, value);
        }
    }

    return ETC_OK;
}


static int etc_WriteSection (FILE* fp, PETCSECTION psect, ETC_BOOL bSectName)
{
    int i;

    if (psect->name == NULL)
        return ETC_OK;

    if (bSectName)
        fprintf (fp, "[%s]\n", psect->name);

    for (i = 0; i < psect->key_nr; i++) {
        fprintf (fp, "%s=%s\n", psect->keys [i], psect->values [i]);
    }

    if (fprintf (fp, "\n") != 1)
        return ETC_FILEIOFAILED;

    return ETC_OK;
}


int SaveEtcToFile (GHANDLE hEtc, const char* file_name)
{
    int i;
    FILE* fp;
    ETC_S *petc = (ETC_S*) hEtc;

    if (petc == NULL)
        return ETC_INVALIDOBJ;

    if (!(fp = fopen (file_name, "w"))) {
        return ETC_FILEIOFAILED;
    }

    for (i = 0; i < petc->section_nr; i++) {
        if (etc_WriteSection (fp, petc->sections + i, TRUE)) {
            fclose (fp);
            return ETC_FILEIOFAILED;
        }
    }

    fclose (fp);
    return ETC_OK;
}


static PETCSECTION etc_NewSection (ETC_S* petc)
{
    PETCSECTION psect;

    if (petc->section_nr == petc->sect_nr_alloc) {
        /* add 5 sections each time we realloc */
        petc->sect_nr_alloc += NR_SECTS_INC_ALLOC;
        petc->sections = (PETCSECTION)realloc (petc->sections, 
                    sizeof (ETCSECTION)*petc->sect_nr_alloc);
    }
    psect = petc->sections + petc->section_nr;

    psect->name = NULL;

    petc->section_nr ++;

    return psect;
}

static int etc_GetSectionValue (PETCSECTION psect, const char* pKey, 
                          char* pValue, int iLen)
{
    int i;

    for (i=0; i<psect->key_nr; i++) {
        if (strcmp (psect->keys [i], pKey) == 0) {
            break;
        }
    }

    if (iLen > 0) { /* get value */
        if (i >= psect->key_nr)
            return ETC_KEYNOTFOUND;

        strncpy (pValue, psect->values [i], iLen);
    }
    else { /* set value */
        if (psect->key_nr_alloc <= 0)
            return ETC_READONLYOBJ;

        if (i >= psect->key_nr) {
            etc_NewKeyValue (psect, pKey, pValue);
        }
        else {
            free (psect->values [i]);
            psect->values [i] = strdup (pValue);
        }
    }

    return ETC_OK;
}


int GetValueFromEtc (GHANDLE hEtc, const char* pSection,
                            const char* pKey, char* pValue, int iLen)
{
    int i, empty_section = -1;
    ETC_S *petc = (ETC_S*) hEtc;
    PETCSECTION psect = NULL;

    if (!petc || !pValue)
        return -1;

    for (i=0; i<petc->section_nr; i++) {
        psect = petc->sections + i;
        if (!psect->name) {
           empty_section = i;
           continue;
        }

        if (strcmp (psect->name, pSection) == 0) {
            break;
        }
    }

    if (i >= petc->section_nr) {
        if (iLen > 0)
            return ETC_SECTIONNOTFOUND;
        else {
            if (petc->sect_nr_alloc <= 0)
                return ETC_READONLYOBJ;

            if (empty_section >= 0)
                psect = petc->sections + empty_section;
            else {
                psect = etc_NewSection (petc);
            }

            if (psect->name == NULL) {
                psect->key_nr = 0;
                psect->name = strdup (pSection);
                psect->key_nr_alloc = NR_KEYS_INIT_ALLOC;
                psect->keys = (char **)malloc (sizeof (char* ) * NR_KEYS_INIT_ALLOC);
                psect->values = (char **)malloc (sizeof (char* ) * NR_KEYS_INIT_ALLOC);
            }
        }
    }

    return etc_GetSectionValue (psect, pKey, pValue, iLen);
}

int GetIntValueFromEtc (GHANDLE hEtc, const char* pSection,
                               const char* pKey, int* value)
{
    int ret;
    char szBuff [51];

    ret = GetValueFromEtc (hEtc, pSection, pKey, szBuff, 50);
    if (ret < 0) {
        return ret;
    }

    *value = strtol (szBuff, NULL, 0);
    if ((*value == LONG_MIN || *value == LONG_MAX) && errno == ERANGE)
        return ETC_INTCONV;

    return ETC_OK;
}


