#ifndef __SENDOUT_BUFFER_H__
#define __SENDOUT_BUFFER_H__

#include <stdlib.h>
#include <queue>
#include <mutex>
#include "types.h"
#include "util.h"

using namespace std;

class SendoutPtrBuffer
{
public:
    SendoutPtrBuffer(int bufsize = 35) : buf_size(bufsize)
    {

    }
    virtual ~SendoutPtrBuffer( )
    {
        for (int i = 0; i < data_buf.size(); i++)
        {
            LS_S8_OUT_PACKET *packet = nullptr;
            packet = data_buf.front();
            data_buf.pop( );
            if(packet){
                if(packet->data){
                    free(packet->data);
                    packet->data = nullptr;
                }
                delete packet;
                packet=nullptr;
            }
        }
    }

    bool PushPacket(LS_S8_OUT_PACKET *next_packet)
    {
        // do NOT add if the next_frame has already been added
        mtx_mp.lock();
        if (!data_buf.empty() && data_buf.back( ) == next_packet)
        {
            mtx_mp.unlock();
            return false;
        }

        // check if the buffer is full
        if(data_buf.size() == buf_size)
        {
            LS_S8_OUT_PACKET *packet = nullptr;
            packet = data_buf.front();
            data_buf.pop();
            free(packet->data);
            delete packet;
        }
        // add the next_frame
        LS_S8_OUT_PACKET *packet = new LS_S8_OUT_PACKET;
        packet->data = (LS_S8 *)malloc(next_packet->size);
        if(packet->data== nullptr){
            printf("=== %d %s malloc err ===\n",__LINE__,__FILE__);
            delete packet;
            packet = nullptr;
            return false;
        }
        memcpy(packet->data,next_packet->data,next_packet->size);
        packet->size = next_packet->size;
        packet->devid = next_packet->devid;
        data_buf.push(packet);
        mtx_mp.unlock( );
        return true;
    }

    LS_S8_OUT_PACKET *PopPacket()
    {
        LS_S8_OUT_PACKET *packet = nullptr;
        mtx_mp.lock( );
        if(!data_buf.empty( ))
        {
            packet = data_buf.front( );
            data_buf.pop( );
        }
        mtx_mp.unlock( );

        return packet;
    }

    int BufLen(int index)
    {
        int size = 0;
        mtx_mp.lock( );
        size = data_buf.size();
        mtx_mp.lock( );
        return size;
    }
private:
    queue<LS_S8_OUT_PACKET*> data_buf;
    int   buf_size;
    mutex mtx_mp;
};

#endif
