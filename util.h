#ifndef _UTIL_H_
#define _UTIL_H_
#include <io.h>
#include <string>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <qglobal.h>
#include <QDir>
#include <QDateTime>
#ifdef Q_OS_LINUX
#include <unistd.h>
#include <fcntl.h>
#elif defined (Q_OS_WIN)
#include <Windows.h>
#endif
#include "types.h"

void Int2Hex(int num,void *p,int n);

int  Hex2Int(void *p,int n);

int getBufSum(void *p,int n);

void printBufByHex(LS_S8 *file,LS_S32 line,void *buf,int len);

void QString2Char(QString str,char *buf);

int createNewDir(char *dirPath);

void getTimeFromNum(LS_U32 mcount,int *h,int *m,int *s);

int checkEleHeadCount(void *buf,int len,int *mCount,int *mLocation);

int checkGpsHeadCount(void *buf,int len,int *mCount,int *mLocation);

int checkTempHeadCount(void *tmp,int len,int *mCount,int *mLocation);

bool isIpAddr(const QString &ip);

bool isSysArgChanged(TSysSet *arg1,TSysSet *arg2);

int getBufXor(LS_U8 *buf,int len);

void myMSleep(LS_U64 usec);

int str2HexStr(char *srcBuf,int len,char *dstStr);

void getCurrentDateTime(int *year,int *month,int *day,int *hour,int *min,int *sec);

#endif
