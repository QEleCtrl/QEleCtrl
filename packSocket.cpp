#include "types.h"
#include "packSocket.h"

#if !defined(_WIN32)
#define closesocketsocket closesocket
#endif

#if defined(__WIN32__) || defined(_WIN32)
#pragma comment(lib,"ws2_32.lib")
#define WS_VERSION_CHOICE1 0x202/*MAKEWORD(2,2)*/
#define WS_VERSION_CHOICE2 0x101/*MAKEWORD(1,1)*/
int initializeWinsockIfNecessary(void) {
	/* We need to call an initialization routine before
	* we can do anything with winsock.  (How fucking lame!):
	*/
	static int _haveInitializedWinsock = 0;
	WSADATA	wsadata;

	if (!_haveInitializedWinsock) {
		if ((WSAStartup(WS_VERSION_CHOICE1, &wsadata) != 0)
			&& ((WSAStartup(WS_VERSION_CHOICE2, &wsadata)) != 0)) {
				return 0; /* error in initialization */
		}
		if ((wsadata.wVersion != WS_VERSION_CHOICE1)
			&& (wsadata.wVersion != WS_VERSION_CHOICE2)) {
				WSACleanup();
				return 0; /* desired Winsock version was not available */
		}
		_haveInitializedWinsock = 1;
	}
	
	return 1;
}
#else
int initializeWinsockIfNecessary(void) { return 1; }
#endif

int myselect(int sockfd, int usec, enum SelectType mtype)
{
	struct timeval tv;
	fd_set readfds, writefds, exceptfds;
	int receive = 0;
	FD_ZERO(&readfds);
	FD_ZERO(&writefds);
	FD_ZERO(&exceptfds);
	tv.tv_sec = usec/1000000;
	tv.tv_usec = usec%1000000;
	FD_SET(sockfd, &exceptfds);
	if(mtype == READ_SELECT){
		FD_SET(sockfd, &readfds);
		receive = select(sockfd+1, &readfds, NULL, &exceptfds, &tv);
	}else{
		FD_SET(sockfd, &writefds);
		receive = select(sockfd+1, NULL, &writefds, &exceptfds, &tv);
	}
	if(receive < 0){
		perror("select error:");
		return SOCK_FALSE;
	}else if(receive == 0)	{
		return SOCK_TIMEOUT;
	}else{
		if(mtype == READ_SELECT){
			if(FD_ISSET(sockfd, &readfds) > 0)
				return SOCK_TRUE;
		}else{
			if(FD_ISSET(sockfd, &writefds) > 0)
				return SOCK_TRUE;
		}
		return SOCK_FALSE;
	}
	return SOCK_TRUE;
}

int createSocket(enum SocketType mtype)
{
	int reuseFlag = 1;
	int socketLen = 64*1024;
	int mSock = -1;
	if (!initializeWinsockIfNecessary()) {
        ls_err_debug("Failed to initialize 'winsock'\n");
		return SOCK_FALSE;
	}
    if(mtype == UDP_SOCK){
		mSock = socket(AF_INET, SOCK_DGRAM, 0);
    }else{
		mSock = socket(AF_INET, SOCK_STREAM, 0);
	}
	if(mSock < 0){ 
	    perror("socket");
		return SOCK_FALSE;
	}
	if(setsockopt(mSock,SOL_SOCKET,SO_RCVBUF,(const char *)&socketLen,sizeof(int))<0){
        ls_err_debug("setsockopt(SO_RCVBUF) error\n");
		goto ERR_LEAVE;
	} 
	if(setsockopt(mSock,SOL_SOCKET,SO_SNDBUF,(const char *)&socketLen,sizeof(int))<0){
        ls_err_debug("setsockopt(SO_RCVBUF) error\n");
		goto ERR_LEAVE;
	} 
	if(setsockopt(mSock, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuseFlag, sizeof(reuseFlag)) < 0){
        ls_err_debug("setsockopt(SO_REUSEADDR) error\n");
		goto ERR_LEAVE;
	}
	return mSock;
ERR_LEAVE:
	closesocket(mSock);
	mSock = SOCK_FALSE;
	return SOCK_FALSE;
}

void setSocketNonblock(int sock)
{
#if defined(__WIN32__) || defined(_WIN32)
	unsigned long non_block = 1;
	ioctlsocket(sock, FIONBIO, &non_block);
#else
	int flags = 0;
	flags = fcntl(sock,F_GETFL,0);）
		fcntl(sock,F_SETFL,flags|O_NONBLOCK);
#endif
}

int joinGroupSock(int sockfd, char *groupip)
{
	struct hostent *group;
	struct ip_mreq mreq;
	struct in_addr ia;
	memset(&mreq, 0, sizeof(struct ip_mreq));
	//bzero(&mreq, sizeof(struct ip_mreq));
	if ((group = gethostbyname(groupip)) == (struct hostent *) 0) {
        ls_err_debug("=====gethostbyname ERR======\n");
        return SOCK_FALSE;
	}
	memcpy((void *)&ia, (void *)group->h_addr, group->h_length);
	memcpy(&mreq.imr_multiaddr.s_addr, &ia, sizeof(struct in_addr));
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if (setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&mreq, sizeof(struct ip_mreq)) == -1) {
        ls_err_debug("=====setsockopt ERR======\n");
        return SOCK_FALSE;
	}
    return SOCK_TRUE;
}

int writeUdpSocket(int sockfd, char* dstIPAddr, int port, void *sendBuf, int bufLen)
{
	struct sockaddr_in var;
	int ret = 0;
	if(bufLen<=0){
        ls_err_debug("write udp client bufLen <= 0\n");
		return SOCK_FALSE;
	}
	if(sockfd < 0) {
        ls_err_debug("write udp client sockfd < 0\n");
		return SOCK_FALSE;
	}
	memset(&var,0,sizeof(var)); 
	var.sin_family = AF_INET;
	var.sin_port = htons(port);
	var.sin_addr.s_addr = inet_addr(dstIPAddr);
	ret = sendto(sockfd,(char *)sendBuf,bufLen,0,(struct sockaddr *)&var,sizeof(var));
	if(ret != bufLen){
        ls_err_debug("send %d ERR\n",ret);
		return SOCK_FALSE;
	}
	return SOCK_TRUE;  
}

int bindSocket (int sockfd,int port)
{
	struct sockaddr_in servAddr;
    memset(&servAddr,0,sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(port);
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	if(bind(sockfd, (struct sockaddr*)&servAddr, sizeof(struct sockaddr)) != 0){
        ls_err_debug("binding err!");
		return SOCK_FALSE;
	}
	return SOCK_TRUE;
}

int listenTcpSocket(int sockfd)
{
	if(listen(sockfd, 1) != 0){
        ls_err_debug("listen TCP socket err!");
		return SOCK_FALSE;
	}
	return SOCK_TRUE;
}
 
int acceptTcpSocket(int sockfd,char *ipAddr)
{
	struct sockaddr_in cliAddr;
	int cliAddrLen = sizeof(cliAddr);
	char clientIP[INET_ADDRSTRLEN]={0};
	int clientFd = accept(sockfd, (struct sockaddr*)&cliAddr, &cliAddrLen);
	if(clientFd < 0){
        ls_err_debug("TCP Socket accept ERR\n");
		return SOCK_FALSE;
	}
#if !defined(_WIN32)
	inet_ntop(AF_INET, &cliAddr.sin_addr.s_addr, clientIP, INET_ADDRSTRLEN); 
	memcpy(ipAddr,clientIP,INET_ADDRSTRLEN);
#else
	strcpy(ipAddr,inet_ntoa(cliAddr.sin_addr));
#endif
	return clientFd;
}

int connectTcpSocket(int sockfd, char *dstIPAddr, int port)
{
	struct sockaddr_in servAddr;
	memset(&servAddr,0,sizeof(struct sockaddr_in*));
	servAddr.sin_addr.s_addr =  inet_addr(dstIPAddr);
	servAddr.sin_family = AF_INET;	
	servAddr.sin_port = htons(port);	
	if (connect(sockfd , (struct sockaddr*)&servAddr, sizeof(struct sockaddr_in))!= 0){		
        ls_err_debug("TCP Socket connect ERR\n");
		return SOCK_FALSE;
	}
	return SOCK_TRUE;
}

int writeTcpSocket(int sockfd, void *sendBuf, int bufLen)
{
	int ret = -1;
	if(sockfd <= 0){
        ls_err_debug("TCP Socket write sockfd <= 0 %d\n",sockfd);
		return SOCK_FALSE;
	}
	if(bufLen <=0){
        ls_err_debug("TCP Socket write bufLen %d<=0 \n",bufLen);
		return SOCK_FALSE;
	}
	ret = myselect(sockfd,2*1000*1000,WRITE_SELECT);
	if(ret<0){
        ls_err_debug("tcp write select ERR\n");
		Sleep(100);
		return SOCK_FALSE;
	}else if(ret == 0){
        ls_err_debug("tcp write select timeout\n");
		return SOCK_TIMEOUT;
	}
	ret = send(sockfd,(char *)sendBuf,bufLen,0);
	if(ret != bufLen){
		return SOCK_FALSE;
	}
	return SOCK_TRUE;
}

int readTCPSocket(int sockfd, char* recvBuf,int bufLen, int *recvLen)
{
	int ret = -1;
	if(sockfd < 0){
        ls_err_debug("TCP Socket write sockfd %d <= 0\n",sockfd);
		return SOCK_FALSE;
	}
	if(bufLen <=0){
        ls_err_debug("TCP Socket write  bufLen %d <=0\n",bufLen);
		return SOCK_FALSE;
	}
	ret = myselect(sockfd,2*1000*1000,READ_SELECT);
	if(ret<0){
        ls_err_debug("tcp recv select ERR\n");
		Sleep(1000);
		return SOCK_FALSE;
	}else if(ret == 0){
        ls_err_debug("tcp recv select timeout\n");
		return SOCK_TIMEOUT;
	}
	*recvLen = recv(sockfd,recvBuf,bufLen,0);
	if(*recvLen <=0){
		return SOCK_FALSE;
	}
	return SOCK_TRUE;
}

void closePackSocket(int *sockfd)
{
	if(*sockfd > 0){
		closesocket(*sockfd);
		*sockfd = SOCK_FALSE;
	}
}

LS_S8 getIPType(char *ipAddr)
{
    unsigned int my_ip;
    unsigned char first;
    my_ip = (inet_addr(ipAddr));
    first = (unsigned char )my_ip;
    if((first&0x80) == 0){
        return 'a';
    }else if((first&0xc0) == 0x80){
        return 'b';
    }else if((first&0xe0) == 0xc0){
        return 'c';
    }else if((first&0xf0) == 0xe0){
        return 'd';
    }else if((first&0xf0) == 0xf0){
        return 'e';
    }
    return 0;
}
