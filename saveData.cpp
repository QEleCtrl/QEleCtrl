#include "saveData.h"

saveData::saveData(int index)
{
    saveIndex = index;
    concentFp=NULL;
}

saveData::~saveData()
{
    if(concentFp){
        fclose(concentFp);
        concentFp=NULL;
    }
}

void saveData::setSavePath(LS_S8 *path)
{
    if(!path)return;
    QString str = "";
    saveMtx.lock();
    memset(saveDirPath,0,sizeof(saveDirPath));
    str = QString("%1/dev%2").arg(path).arg(saveIndex+1);
    str.replace("//","/");
    QString2Char(str,saveDirPath);
    createNewDir(saveDirPath);
    //ls_info_debug("%s",saveDirPath);
    if(concentFp){
        fclose(concentFp);
        concentFp = NULL;
    }
    saveMtx.unlock();
}

int saveData::saveRecvPressData(LS_U8 *buf,int len)
{
    char fileName_data[260]={0};
    char fileName_txt[260]={0};
    int txt_len = 0,dstLen=len*4;
    char buf_txt[1024*200]={0};
    FILE *fp_data=NULL,*fp_txt=NULL;
    int year,month,day,hour,min,sec=0;
    saveMtx.lock();
    getCurrentDateTime(&year,&month,&day,&hour,&min,&sec);
    memset(fileName_data,0,sizeof(fileName_data));
    sprintf(fileName_data,"%s/%.4d_%.2d_%.2d_%.2d_%.2d_%.2d.data",saveDirPath,year,month,day,hour,min,sec);
    memset(fileName_txt,0,sizeof(fileName_txt));
    sprintf(fileName_txt,"%s/%.4d_%.2d_%.2d_%.2d_%.2d_%.2d.txt",saveDirPath,year,month,day,hour,min,sec);
    fp_data = fopen(fileName_data,"wb");
    if(!fp_data){
        ls_err_debug("fopen %s ERR\n",fileName_data);
        saveMtx.unlock();
        return LS_FAIL;
    }
    fwrite((char *)buf,1,len,fp_data);
    fflush(fp_data);
    fclose(fp_data);
    fp_data=NULL;
    memset(buf_txt,0,dstLen);
    txt_len = str2HexStr((char *)buf,len,buf_txt);
    fp_txt = fopen(fileName_txt,"wb");
    if(!fp_txt){
        ls_err_debug("fopen %s ERR\n",fileName_txt);
        saveMtx.unlock();
        return LS_FAIL;
    }
    fwrite((char *)buf_txt,1,txt_len,fp_txt);
    fflush(fp_txt);
    fclose(fp_txt);
    fp_txt=NULL;
    saveMtx.unlock();
    return LS_SUCC;
}

int saveData::saveRecvPressValue(double *dValue,int valueCount)
{
    FILE *fp =NULL;
    char valueBuf[64]={0};
    int valueLen=0;
    char fileName[MAX_PATH]={0};
    int year,month,day,hour,min,sec=0;
    saveMtx.lock();
    getCurrentDateTime(&year,&month,&day,&hour,&min,&sec);
    memset(fileName,0,sizeof(fileName));
    sprintf(fileName,"%s/%.4d_%.2d_%.2d_%.2d_%.2d_%.2d_value.txt",saveDirPath,year,month,day,hour,min,sec);
    fp = fopen(fileName,"wb");
    if(!fp){
        ls_err_debug("fopen %s ERR\n",fileName);
        saveMtx.unlock();
        return LS_FAIL;
    }
    for(int i=0;i<valueCount;i++){
        int num = (int)dValue[i];
        memset(valueBuf,0,sizeof(valueBuf));
        valueLen = sprintf(valueBuf,"%d\n",num);
        fwrite(valueBuf,1,valueLen,fp);
    }
    fflush(fp);
    fclose(fp);
    fp=NULL;
    saveMtx.unlock();
    return LS_SUCC;
}

int saveData::saveConcentValue(int saveCount,float fTempValue,float *fConcentValue,int concentCount)
{
    int len=0;
    char fileName[MAX_PATH]={0};
    char saveBuf[1024]={0};
    char *concentValueBuf=NULL;
    int concentValueLen = 0;
    int year,month,day,hour,min,sec=0;
    saveMtx.lock();
    getCurrentDateTime(&year,&month,&day,&hour,&min,&sec);
    if(concentFp==NULL){
        memset(fileName,0,sizeof(fileName));
        sprintf(fileName,"%s/%.4d_%.2d_%.2d_%.2d_%.2d_%.2d_save.txt",saveDirPath,year,month,day,hour,min,sec);
        concentFp = fopen(fileName,"wb");
        if(!concentFp){
            ls_info_debug("fopen %s ERR\n",fileName);
            saveMtx.unlock();
            return LS_FAIL;
        }
    }
    concentValueLen =(len+1)*64;
    concentValueBuf = (char *)malloc(sizeof(char)*concentValueLen);
    if(!concentValueBuf){
        ls_err_debug("malloc %d ERR\n",concentValueLen);
        saveMtx.unlock();
        return LS_FAIL;
    }
    memset(concentValueBuf,0,concentValueLen);
    len = 0;
    for(int i=0;i<concentCount;i++){
        len += sprintf(concentValueBuf+len,"    %.4f",fConcentValue[i]);
    }
    memset(saveBuf,0,sizeof(saveBuf));
    len = sprintf(saveBuf,"%.4d/%.2d/%.2d/%.2d/%.2d/%.2d	%.6d	%s 	%.4f\n",year,month,day,hour,min,sec,\
        saveCount,concentValueBuf,fTempValue);
    if(!concentFp){
        ls_info_debug("concentFp NULL\n");
        free(concentValueBuf);
        concentValueBuf = NULL;
        saveMtx.unlock();
        return LS_FAIL;
    }
    fwrite(saveBuf,1,len,concentFp);
    fflush(concentFp);
    free(concentValueBuf);
    concentValueBuf = NULL;
    saveMtx.unlock();
    return LS_SUCC;
}
