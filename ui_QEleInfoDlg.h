/********************************************************************************
** Form generated from reading UI file 'QEleInfoDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QELEINFODLG_H
#define UI_QELEINFODLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_QEleInfoDlg
{
public:
    QGridLayout *gridLayout_16;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_9;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout;
    QLabel *LA_laser_current_1;
    QLabel *LA_laser_current_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *LA_press_ll;
    QLabel *LA_press_hh;
    QHBoxLayout *horizontalLayout_3;
    QLabel *LA_vol_step;
    QLabel *LA_aver_count;
    QHBoxLayout *horizontalLayout_4;
    QLabel *LA_coll_count;
    QLabel *LA_workmode;
    QGridLayout *gridLayout_3;
    QLabel *LA_voltage_a;
    QLabel *LA_voltage_b;
    QLabel *LA_voltage_c;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QLabel *LA_ch1_press;
    QLabel *LA_ch1_temp;
    QLabel *LA_ch2_press;
    QLabel *LA_ch2_temp;
    QLabel *LA_humidity;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_2;
    QLabel *LA_reserve_0;
    QLabel *LA_reserve_1;
    QLabel *LA_reserve_2;
    QLabel *LA_reserve_3;
    QLabel *LA_reserve_4;
    QLabel *LA_reserve_5;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_15;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_5;
    QHBoxLayout *horizontalLayout_5;
    QLabel *LA_laser_current_1_2;
    QLabel *LA_laser_current_2_2;
    QHBoxLayout *horizontalLayout_6;
    QLabel *LA_press_ll_2;
    QLabel *LA_press_hh_2;
    QHBoxLayout *horizontalLayout_7;
    QLabel *LA_vol_step_2;
    QLabel *LA_aver_count_2;
    QHBoxLayout *horizontalLayout_8;
    QLabel *LA_coll_count_2;
    QLabel *LA_workmode_2;
    QGridLayout *gridLayout_6;
    QLabel *LA_voltage_a_2;
    QLabel *LA_voltage_b_2;
    QLabel *LA_voltage_c_2;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_7;
    QLabel *LA_ch1_press_2;
    QLabel *LA_ch1_temp_2;
    QLabel *LA_ch2_press_2;
    QLabel *LA_ch2_temp_2;
    QLabel *LA_humidity_2;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_8;
    QLabel *LA_reserve_0_2;
    QLabel *LA_reserve_1_2;
    QLabel *LA_reserve_2_2;
    QLabel *LA_reserve_3_2;
    QLabel *LA_reserve_4_2;
    QLabel *LA_reserve_5_2;

    void setupUi(QDialog *QEleInfoDlg)
    {
        if (QEleInfoDlg->objectName().isEmpty())
            QEleInfoDlg->setObjectName(QString::fromUtf8("QEleInfoDlg"));
        QEleInfoDlg->resize(553, 437);
        gridLayout_16 = new QGridLayout(QEleInfoDlg);
        gridLayout_16->setObjectName(QString::fromUtf8("gridLayout_16"));
        groupBox_7 = new QGroupBox(QEleInfoDlg);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        gridLayout_9 = new QGridLayout(groupBox_7);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        gridLayout_9->setContentsMargins(-1, 30, -1, -1);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(groupBox_7);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_4 = new QGridLayout(groupBox);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(-1, 30, -1, -1);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        LA_laser_current_1 = new QLabel(groupBox);
        LA_laser_current_1->setObjectName(QString::fromUtf8("LA_laser_current_1"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(LA_laser_current_1->sizePolicy().hasHeightForWidth());
        LA_laser_current_1->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(LA_laser_current_1);

        LA_laser_current_2 = new QLabel(groupBox);
        LA_laser_current_2->setObjectName(QString::fromUtf8("LA_laser_current_2"));
        sizePolicy.setHeightForWidth(LA_laser_current_2->sizePolicy().hasHeightForWidth());
        LA_laser_current_2->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(LA_laser_current_2);


        gridLayout_4->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        LA_press_ll = new QLabel(groupBox);
        LA_press_ll->setObjectName(QString::fromUtf8("LA_press_ll"));
        sizePolicy.setHeightForWidth(LA_press_ll->sizePolicy().hasHeightForWidth());
        LA_press_ll->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(LA_press_ll);

        LA_press_hh = new QLabel(groupBox);
        LA_press_hh->setObjectName(QString::fromUtf8("LA_press_hh"));
        sizePolicy.setHeightForWidth(LA_press_hh->sizePolicy().hasHeightForWidth());
        LA_press_hh->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(LA_press_hh);


        gridLayout_4->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        LA_vol_step = new QLabel(groupBox);
        LA_vol_step->setObjectName(QString::fromUtf8("LA_vol_step"));
        sizePolicy.setHeightForWidth(LA_vol_step->sizePolicy().hasHeightForWidth());
        LA_vol_step->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(LA_vol_step);

        LA_aver_count = new QLabel(groupBox);
        LA_aver_count->setObjectName(QString::fromUtf8("LA_aver_count"));
        sizePolicy.setHeightForWidth(LA_aver_count->sizePolicy().hasHeightForWidth());
        LA_aver_count->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(LA_aver_count);


        gridLayout_4->addLayout(horizontalLayout_3, 2, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        LA_coll_count = new QLabel(groupBox);
        LA_coll_count->setObjectName(QString::fromUtf8("LA_coll_count"));
        sizePolicy.setHeightForWidth(LA_coll_count->sizePolicy().hasHeightForWidth());
        LA_coll_count->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(LA_coll_count);

        LA_workmode = new QLabel(groupBox);
        LA_workmode->setObjectName(QString::fromUtf8("LA_workmode"));
        sizePolicy.setHeightForWidth(LA_workmode->sizePolicy().hasHeightForWidth());
        LA_workmode->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(LA_workmode);


        gridLayout_4->addLayout(horizontalLayout_4, 3, 0, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        LA_voltage_a = new QLabel(groupBox);
        LA_voltage_a->setObjectName(QString::fromUtf8("LA_voltage_a"));
        sizePolicy.setHeightForWidth(LA_voltage_a->sizePolicy().hasHeightForWidth());
        LA_voltage_a->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(LA_voltage_a, 0, 0, 1, 1);

        LA_voltage_b = new QLabel(groupBox);
        LA_voltage_b->setObjectName(QString::fromUtf8("LA_voltage_b"));
        sizePolicy.setHeightForWidth(LA_voltage_b->sizePolicy().hasHeightForWidth());
        LA_voltage_b->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(LA_voltage_b, 0, 1, 1, 1);

        LA_voltage_c = new QLabel(groupBox);
        LA_voltage_c->setObjectName(QString::fromUtf8("LA_voltage_c"));
        sizePolicy.setHeightForWidth(LA_voltage_c->sizePolicy().hasHeightForWidth());
        LA_voltage_c->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(LA_voltage_c, 0, 2, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 4, 0, 1, 1);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(groupBox_7);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(-1, 30, -1, -1);
        LA_ch1_press = new QLabel(groupBox_2);
        LA_ch1_press->setObjectName(QString::fromUtf8("LA_ch1_press"));
        sizePolicy.setHeightForWidth(LA_ch1_press->sizePolicy().hasHeightForWidth());
        LA_ch1_press->setSizePolicy(sizePolicy);

        gridLayout->addWidget(LA_ch1_press, 0, 0, 1, 1);

        LA_ch1_temp = new QLabel(groupBox_2);
        LA_ch1_temp->setObjectName(QString::fromUtf8("LA_ch1_temp"));
        sizePolicy.setHeightForWidth(LA_ch1_temp->sizePolicy().hasHeightForWidth());
        LA_ch1_temp->setSizePolicy(sizePolicy);

        gridLayout->addWidget(LA_ch1_temp, 0, 1, 1, 1);

        LA_ch2_press = new QLabel(groupBox_2);
        LA_ch2_press->setObjectName(QString::fromUtf8("LA_ch2_press"));
        sizePolicy.setHeightForWidth(LA_ch2_press->sizePolicy().hasHeightForWidth());
        LA_ch2_press->setSizePolicy(sizePolicy);

        gridLayout->addWidget(LA_ch2_press, 1, 0, 1, 1);

        LA_ch2_temp = new QLabel(groupBox_2);
        LA_ch2_temp->setObjectName(QString::fromUtf8("LA_ch2_temp"));
        sizePolicy.setHeightForWidth(LA_ch2_temp->sizePolicy().hasHeightForWidth());
        LA_ch2_temp->setSizePolicy(sizePolicy);

        gridLayout->addWidget(LA_ch2_temp, 1, 1, 1, 1);

        LA_humidity = new QLabel(groupBox_2);
        LA_humidity->setObjectName(QString::fromUtf8("LA_humidity"));
        sizePolicy.setHeightForWidth(LA_humidity->sizePolicy().hasHeightForWidth());
        LA_humidity->setSizePolicy(sizePolicy);
        LA_humidity->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(LA_humidity, 2, 0, 1, 2);


        verticalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(groupBox_7);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_2 = new QGridLayout(groupBox_3);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 30, -1, -1);
        LA_reserve_0 = new QLabel(groupBox_3);
        LA_reserve_0->setObjectName(QString::fromUtf8("LA_reserve_0"));
        sizePolicy.setHeightForWidth(LA_reserve_0->sizePolicy().hasHeightForWidth());
        LA_reserve_0->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(LA_reserve_0, 0, 0, 1, 1);

        LA_reserve_1 = new QLabel(groupBox_3);
        LA_reserve_1->setObjectName(QString::fromUtf8("LA_reserve_1"));
        sizePolicy.setHeightForWidth(LA_reserve_1->sizePolicy().hasHeightForWidth());
        LA_reserve_1->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(LA_reserve_1, 0, 1, 1, 1);

        LA_reserve_2 = new QLabel(groupBox_3);
        LA_reserve_2->setObjectName(QString::fromUtf8("LA_reserve_2"));
        sizePolicy.setHeightForWidth(LA_reserve_2->sizePolicy().hasHeightForWidth());
        LA_reserve_2->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(LA_reserve_2, 0, 2, 1, 1);

        LA_reserve_3 = new QLabel(groupBox_3);
        LA_reserve_3->setObjectName(QString::fromUtf8("LA_reserve_3"));
        sizePolicy.setHeightForWidth(LA_reserve_3->sizePolicy().hasHeightForWidth());
        LA_reserve_3->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(LA_reserve_3, 1, 0, 1, 1);

        LA_reserve_4 = new QLabel(groupBox_3);
        LA_reserve_4->setObjectName(QString::fromUtf8("LA_reserve_4"));
        sizePolicy.setHeightForWidth(LA_reserve_4->sizePolicy().hasHeightForWidth());
        LA_reserve_4->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(LA_reserve_4, 1, 1, 1, 1);

        LA_reserve_5 = new QLabel(groupBox_3);
        LA_reserve_5->setObjectName(QString::fromUtf8("LA_reserve_5"));
        sizePolicy.setHeightForWidth(LA_reserve_5->sizePolicy().hasHeightForWidth());
        LA_reserve_5->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(LA_reserve_5, 1, 2, 1, 1);


        verticalLayout->addWidget(groupBox_3);

        verticalLayout->setStretch(0, 5);
        verticalLayout->setStretch(1, 3);
        verticalLayout->setStretch(2, 2);

        gridLayout_9->addLayout(verticalLayout, 0, 0, 1, 1);


        gridLayout_16->addWidget(groupBox_7, 0, 0, 1, 1);

        groupBox_8 = new QGroupBox(QEleInfoDlg);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        gridLayout_15 = new QGridLayout(groupBox_8);
        gridLayout_15->setObjectName(QString::fromUtf8("gridLayout_15"));
        gridLayout_15->setContentsMargins(-1, 30, -1, -1);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox_4 = new QGroupBox(groupBox_8);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_5 = new QGridLayout(groupBox_4);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(-1, 30, -1, -1);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        LA_laser_current_1_2 = new QLabel(groupBox_4);
        LA_laser_current_1_2->setObjectName(QString::fromUtf8("LA_laser_current_1_2"));
        sizePolicy.setHeightForWidth(LA_laser_current_1_2->sizePolicy().hasHeightForWidth());
        LA_laser_current_1_2->setSizePolicy(sizePolicy);

        horizontalLayout_5->addWidget(LA_laser_current_1_2);

        LA_laser_current_2_2 = new QLabel(groupBox_4);
        LA_laser_current_2_2->setObjectName(QString::fromUtf8("LA_laser_current_2_2"));
        sizePolicy.setHeightForWidth(LA_laser_current_2_2->sizePolicy().hasHeightForWidth());
        LA_laser_current_2_2->setSizePolicy(sizePolicy);

        horizontalLayout_5->addWidget(LA_laser_current_2_2);


        gridLayout_5->addLayout(horizontalLayout_5, 0, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        LA_press_ll_2 = new QLabel(groupBox_4);
        LA_press_ll_2->setObjectName(QString::fromUtf8("LA_press_ll_2"));
        sizePolicy.setHeightForWidth(LA_press_ll_2->sizePolicy().hasHeightForWidth());
        LA_press_ll_2->setSizePolicy(sizePolicy);

        horizontalLayout_6->addWidget(LA_press_ll_2);

        LA_press_hh_2 = new QLabel(groupBox_4);
        LA_press_hh_2->setObjectName(QString::fromUtf8("LA_press_hh_2"));
        sizePolicy.setHeightForWidth(LA_press_hh_2->sizePolicy().hasHeightForWidth());
        LA_press_hh_2->setSizePolicy(sizePolicy);

        horizontalLayout_6->addWidget(LA_press_hh_2);


        gridLayout_5->addLayout(horizontalLayout_6, 1, 0, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        LA_vol_step_2 = new QLabel(groupBox_4);
        LA_vol_step_2->setObjectName(QString::fromUtf8("LA_vol_step_2"));
        sizePolicy.setHeightForWidth(LA_vol_step_2->sizePolicy().hasHeightForWidth());
        LA_vol_step_2->setSizePolicy(sizePolicy);

        horizontalLayout_7->addWidget(LA_vol_step_2);

        LA_aver_count_2 = new QLabel(groupBox_4);
        LA_aver_count_2->setObjectName(QString::fromUtf8("LA_aver_count_2"));
        sizePolicy.setHeightForWidth(LA_aver_count_2->sizePolicy().hasHeightForWidth());
        LA_aver_count_2->setSizePolicy(sizePolicy);

        horizontalLayout_7->addWidget(LA_aver_count_2);


        gridLayout_5->addLayout(horizontalLayout_7, 2, 0, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        LA_coll_count_2 = new QLabel(groupBox_4);
        LA_coll_count_2->setObjectName(QString::fromUtf8("LA_coll_count_2"));
        sizePolicy.setHeightForWidth(LA_coll_count_2->sizePolicy().hasHeightForWidth());
        LA_coll_count_2->setSizePolicy(sizePolicy);

        horizontalLayout_8->addWidget(LA_coll_count_2);

        LA_workmode_2 = new QLabel(groupBox_4);
        LA_workmode_2->setObjectName(QString::fromUtf8("LA_workmode_2"));
        sizePolicy.setHeightForWidth(LA_workmode_2->sizePolicy().hasHeightForWidth());
        LA_workmode_2->setSizePolicy(sizePolicy);

        horizontalLayout_8->addWidget(LA_workmode_2);


        gridLayout_5->addLayout(horizontalLayout_8, 3, 0, 1, 1);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        LA_voltage_a_2 = new QLabel(groupBox_4);
        LA_voltage_a_2->setObjectName(QString::fromUtf8("LA_voltage_a_2"));
        sizePolicy.setHeightForWidth(LA_voltage_a_2->sizePolicy().hasHeightForWidth());
        LA_voltage_a_2->setSizePolicy(sizePolicy);

        gridLayout_6->addWidget(LA_voltage_a_2, 0, 0, 1, 1);

        LA_voltage_b_2 = new QLabel(groupBox_4);
        LA_voltage_b_2->setObjectName(QString::fromUtf8("LA_voltage_b_2"));
        sizePolicy.setHeightForWidth(LA_voltage_b_2->sizePolicy().hasHeightForWidth());
        LA_voltage_b_2->setSizePolicy(sizePolicy);

        gridLayout_6->addWidget(LA_voltage_b_2, 0, 1, 1, 1);

        LA_voltage_c_2 = new QLabel(groupBox_4);
        LA_voltage_c_2->setObjectName(QString::fromUtf8("LA_voltage_c_2"));
        sizePolicy.setHeightForWidth(LA_voltage_c_2->sizePolicy().hasHeightForWidth());
        LA_voltage_c_2->setSizePolicy(sizePolicy);

        gridLayout_6->addWidget(LA_voltage_c_2, 0, 2, 1, 1);


        gridLayout_5->addLayout(gridLayout_6, 4, 0, 1, 1);


        verticalLayout_2->addWidget(groupBox_4);

        groupBox_5 = new QGroupBox(groupBox_8);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        gridLayout_7 = new QGridLayout(groupBox_5);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout_7->setContentsMargins(-1, 30, -1, -1);
        LA_ch1_press_2 = new QLabel(groupBox_5);
        LA_ch1_press_2->setObjectName(QString::fromUtf8("LA_ch1_press_2"));
        sizePolicy.setHeightForWidth(LA_ch1_press_2->sizePolicy().hasHeightForWidth());
        LA_ch1_press_2->setSizePolicy(sizePolicy);

        gridLayout_7->addWidget(LA_ch1_press_2, 0, 0, 1, 1);

        LA_ch1_temp_2 = new QLabel(groupBox_5);
        LA_ch1_temp_2->setObjectName(QString::fromUtf8("LA_ch1_temp_2"));
        sizePolicy.setHeightForWidth(LA_ch1_temp_2->sizePolicy().hasHeightForWidth());
        LA_ch1_temp_2->setSizePolicy(sizePolicy);

        gridLayout_7->addWidget(LA_ch1_temp_2, 0, 1, 1, 1);

        LA_ch2_press_2 = new QLabel(groupBox_5);
        LA_ch2_press_2->setObjectName(QString::fromUtf8("LA_ch2_press_2"));
        sizePolicy.setHeightForWidth(LA_ch2_press_2->sizePolicy().hasHeightForWidth());
        LA_ch2_press_2->setSizePolicy(sizePolicy);

        gridLayout_7->addWidget(LA_ch2_press_2, 1, 0, 1, 1);

        LA_ch2_temp_2 = new QLabel(groupBox_5);
        LA_ch2_temp_2->setObjectName(QString::fromUtf8("LA_ch2_temp_2"));
        sizePolicy.setHeightForWidth(LA_ch2_temp_2->sizePolicy().hasHeightForWidth());
        LA_ch2_temp_2->setSizePolicy(sizePolicy);

        gridLayout_7->addWidget(LA_ch2_temp_2, 1, 1, 1, 1);

        LA_humidity_2 = new QLabel(groupBox_5);
        LA_humidity_2->setObjectName(QString::fromUtf8("LA_humidity_2"));
        sizePolicy.setHeightForWidth(LA_humidity_2->sizePolicy().hasHeightForWidth());
        LA_humidity_2->setSizePolicy(sizePolicy);
        LA_humidity_2->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(LA_humidity_2, 2, 0, 1, 2);


        verticalLayout_2->addWidget(groupBox_5);

        groupBox_6 = new QGroupBox(groupBox_8);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        gridLayout_8 = new QGridLayout(groupBox_6);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_8->setContentsMargins(-1, 30, -1, -1);
        LA_reserve_0_2 = new QLabel(groupBox_6);
        LA_reserve_0_2->setObjectName(QString::fromUtf8("LA_reserve_0_2"));
        sizePolicy.setHeightForWidth(LA_reserve_0_2->sizePolicy().hasHeightForWidth());
        LA_reserve_0_2->setSizePolicy(sizePolicy);

        gridLayout_8->addWidget(LA_reserve_0_2, 0, 0, 1, 1);

        LA_reserve_1_2 = new QLabel(groupBox_6);
        LA_reserve_1_2->setObjectName(QString::fromUtf8("LA_reserve_1_2"));
        sizePolicy.setHeightForWidth(LA_reserve_1_2->sizePolicy().hasHeightForWidth());
        LA_reserve_1_2->setSizePolicy(sizePolicy);

        gridLayout_8->addWidget(LA_reserve_1_2, 0, 1, 1, 1);

        LA_reserve_2_2 = new QLabel(groupBox_6);
        LA_reserve_2_2->setObjectName(QString::fromUtf8("LA_reserve_2_2"));
        sizePolicy.setHeightForWidth(LA_reserve_2_2->sizePolicy().hasHeightForWidth());
        LA_reserve_2_2->setSizePolicy(sizePolicy);

        gridLayout_8->addWidget(LA_reserve_2_2, 0, 2, 1, 1);

        LA_reserve_3_2 = new QLabel(groupBox_6);
        LA_reserve_3_2->setObjectName(QString::fromUtf8("LA_reserve_3_2"));
        sizePolicy.setHeightForWidth(LA_reserve_3_2->sizePolicy().hasHeightForWidth());
        LA_reserve_3_2->setSizePolicy(sizePolicy);

        gridLayout_8->addWidget(LA_reserve_3_2, 1, 0, 1, 1);

        LA_reserve_4_2 = new QLabel(groupBox_6);
        LA_reserve_4_2->setObjectName(QString::fromUtf8("LA_reserve_4_2"));
        sizePolicy.setHeightForWidth(LA_reserve_4_2->sizePolicy().hasHeightForWidth());
        LA_reserve_4_2->setSizePolicy(sizePolicy);

        gridLayout_8->addWidget(LA_reserve_4_2, 1, 1, 1, 1);

        LA_reserve_5_2 = new QLabel(groupBox_6);
        LA_reserve_5_2->setObjectName(QString::fromUtf8("LA_reserve_5_2"));
        sizePolicy.setHeightForWidth(LA_reserve_5_2->sizePolicy().hasHeightForWidth());
        LA_reserve_5_2->setSizePolicy(sizePolicy);

        gridLayout_8->addWidget(LA_reserve_5_2, 1, 2, 1, 1);


        verticalLayout_2->addWidget(groupBox_6);

        verticalLayout_2->setStretch(0, 5);
        verticalLayout_2->setStretch(1, 3);
        verticalLayout_2->setStretch(2, 2);

        gridLayout_15->addLayout(verticalLayout_2, 0, 0, 1, 1);


        gridLayout_16->addWidget(groupBox_8, 0, 1, 1, 1);


        retranslateUi(QEleInfoDlg);

        QMetaObject::connectSlotsByName(QEleInfoDlg);
    } // setupUi

    void retranslateUi(QDialog *QEleInfoDlg)
    {
        QEleInfoDlg->setWindowTitle(QApplication::translate("QEleInfoDlg", "\345\217\202\346\225\260\345\222\214\347\212\266\346\200\201\346\230\276\347\244\272", nullptr));
        groupBox_7->setTitle(QApplication::translate("QEleInfoDlg", "\350\256\276\345\244\2071\346\230\276\347\244\272", nullptr));
        groupBox->setTitle(QApplication::translate("QEleInfoDlg", "\345\217\202\346\225\260\346\230\276\347\244\272", nullptr));
        LA_laser_current_1->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_laser_current_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_press_ll->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_press_hh->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_vol_step->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_aver_count->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_coll_count->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_workmode->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_voltage_a->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_voltage_b->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_voltage_c->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        groupBox_2->setTitle(QApplication::translate("QEleInfoDlg", "\345\216\213\345\212\233\344\274\240\346\204\237\345\231\250", nullptr));
        LA_ch1_press->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_ch1_temp->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_ch2_press->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_ch2_temp->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_humidity->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        groupBox_3->setTitle(QApplication::translate("QEleInfoDlg", "\351\242\204\347\225\231\344\275\215", nullptr));
        LA_reserve_0->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_1->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_3->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_4->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_5->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        groupBox_8->setTitle(QApplication::translate("QEleInfoDlg", "\350\256\276\345\244\2072\346\230\276\347\244\272", nullptr));
        groupBox_4->setTitle(QApplication::translate("QEleInfoDlg", "\345\217\202\346\225\260\346\230\276\347\244\272", nullptr));
        LA_laser_current_1_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_laser_current_2_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_press_ll_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_press_hh_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_vol_step_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_aver_count_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_coll_count_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_workmode_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_voltage_a_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_voltage_b_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_voltage_c_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        groupBox_5->setTitle(QApplication::translate("QEleInfoDlg", "\345\216\213\345\212\233\344\274\240\346\204\237\345\231\250", nullptr));
        LA_ch1_press_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_ch1_temp_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_ch2_press_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_ch2_temp_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_humidity_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        groupBox_6->setTitle(QApplication::translate("QEleInfoDlg", "\351\242\204\347\225\231\344\275\215", nullptr));
        LA_reserve_0_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_1_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_2_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_3_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_4_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
        LA_reserve_5_2->setText(QApplication::translate("QEleInfoDlg", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QEleInfoDlg: public Ui_QEleInfoDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QELEINFODLG_H
