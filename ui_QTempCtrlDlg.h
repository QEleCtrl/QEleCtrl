/********************************************************************************
** Form generated from reading UI file 'QTempCtrlDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTEMPCTRLDLG_H
#define UI_QTEMPCTRLDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_QTempCtrlDlg
{
public:
    QGridLayout *gridLayout_6;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QRadioButton *RB_temp_output_on;
    QRadioButton *RB_temp_output_off;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLabel *LA_output_voltage_0;
    QLabel *LA_output_current_0;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_3;
    QLabel *LA_output_voltage_1;
    QLabel *LA_output_current_1;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLineEdit *LA_temp_set;
    QPushButton *PB_temp_set;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *LE_protect_current;
    QPushButton *PB_protect_current;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *LE_protect_voltage;
    QPushButton *PB_portect_voltage;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *LE_protect_temp_hh;
    QPushButton *PB_protect_temp_hh;
    QHBoxLayout *horizontalLayout_5;
    QLineEdit *LE_protect_temp_ll;
    QPushButton *PB_protect_temp_ll;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_4;
    QLabel *LA_check_protect_current0;
    QPushButton *PB_check_protect_current0;
    QLabel *LA_check_protect_voltage0;
    QPushButton *PB_check_protect_voltage0;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_5;
    QLabel *LA_check_protect_current1;
    QPushButton *PB_check_protect_current1;
    QLabel *LA_check_protect_voltage1;
    QPushButton *PB_check_protect_voltage1;

    void setupUi(QDialog *QTempCtrlDlg)
    {
        if (QTempCtrlDlg->objectName().isEmpty())
            QTempCtrlDlg->setObjectName(QString::fromUtf8("QTempCtrlDlg"));
        QTempCtrlDlg->resize(407, 349);
        gridLayout_6 = new QGridLayout(QTempCtrlDlg);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        groupBox = new QGroupBox(QTempCtrlDlg);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(-1, 20, -1, -1);
        RB_temp_output_on = new QRadioButton(groupBox);
        RB_temp_output_on->setObjectName(QString::fromUtf8("RB_temp_output_on"));

        gridLayout->addWidget(RB_temp_output_on, 0, 0, 1, 1);

        RB_temp_output_off = new QRadioButton(groupBox);
        RB_temp_output_off->setObjectName(QString::fromUtf8("RB_temp_output_off"));

        gridLayout->addWidget(RB_temp_output_off, 1, 0, 1, 1);


        gridLayout_6->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(QTempCtrlDlg);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 20, -1, -1);
        LA_output_voltage_0 = new QLabel(groupBox_2);
        LA_output_voltage_0->setObjectName(QString::fromUtf8("LA_output_voltage_0"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(LA_output_voltage_0->sizePolicy().hasHeightForWidth());
        LA_output_voltage_0->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(LA_output_voltage_0, 0, 0, 1, 1);

        LA_output_current_0 = new QLabel(groupBox_2);
        LA_output_current_0->setObjectName(QString::fromUtf8("LA_output_current_0"));
        sizePolicy.setHeightForWidth(LA_output_current_0->sizePolicy().hasHeightForWidth());
        LA_output_current_0->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(LA_output_current_0, 1, 0, 1, 1);


        gridLayout_6->addWidget(groupBox_2, 0, 1, 1, 2);

        groupBox_3 = new QGroupBox(QTempCtrlDlg);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_3 = new QGridLayout(groupBox_3);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(-1, 20, -1, -1);
        LA_output_voltage_1 = new QLabel(groupBox_3);
        LA_output_voltage_1->setObjectName(QString::fromUtf8("LA_output_voltage_1"));
        sizePolicy.setHeightForWidth(LA_output_voltage_1->sizePolicy().hasHeightForWidth());
        LA_output_voltage_1->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(LA_output_voltage_1, 0, 0, 1, 1);

        LA_output_current_1 = new QLabel(groupBox_3);
        LA_output_current_1->setObjectName(QString::fromUtf8("LA_output_current_1"));
        sizePolicy.setHeightForWidth(LA_output_current_1->sizePolicy().hasHeightForWidth());
        LA_output_current_1->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(LA_output_current_1, 1, 0, 1, 1);


        gridLayout_6->addWidget(groupBox_3, 0, 3, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        LA_temp_set = new QLineEdit(QTempCtrlDlg);
        LA_temp_set->setObjectName(QString::fromUtf8("LA_temp_set"));

        horizontalLayout->addWidget(LA_temp_set);

        PB_temp_set = new QPushButton(QTempCtrlDlg);
        PB_temp_set->setObjectName(QString::fromUtf8("PB_temp_set"));

        horizontalLayout->addWidget(PB_temp_set);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        LE_protect_current = new QLineEdit(QTempCtrlDlg);
        LE_protect_current->setObjectName(QString::fromUtf8("LE_protect_current"));

        horizontalLayout_2->addWidget(LE_protect_current);

        PB_protect_current = new QPushButton(QTempCtrlDlg);
        PB_protect_current->setObjectName(QString::fromUtf8("PB_protect_current"));

        horizontalLayout_2->addWidget(PB_protect_current);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        LE_protect_voltage = new QLineEdit(QTempCtrlDlg);
        LE_protect_voltage->setObjectName(QString::fromUtf8("LE_protect_voltage"));

        horizontalLayout_3->addWidget(LE_protect_voltage);

        PB_portect_voltage = new QPushButton(QTempCtrlDlg);
        PB_portect_voltage->setObjectName(QString::fromUtf8("PB_portect_voltage"));

        horizontalLayout_3->addWidget(PB_portect_voltage);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        LE_protect_temp_hh = new QLineEdit(QTempCtrlDlg);
        LE_protect_temp_hh->setObjectName(QString::fromUtf8("LE_protect_temp_hh"));

        horizontalLayout_4->addWidget(LE_protect_temp_hh);

        PB_protect_temp_hh = new QPushButton(QTempCtrlDlg);
        PB_protect_temp_hh->setObjectName(QString::fromUtf8("PB_protect_temp_hh"));

        horizontalLayout_4->addWidget(PB_protect_temp_hh);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        LE_protect_temp_ll = new QLineEdit(QTempCtrlDlg);
        LE_protect_temp_ll->setObjectName(QString::fromUtf8("LE_protect_temp_ll"));

        horizontalLayout_5->addWidget(LE_protect_temp_ll);

        PB_protect_temp_ll = new QPushButton(QTempCtrlDlg);
        PB_protect_temp_ll->setObjectName(QString::fromUtf8("PB_protect_temp_ll"));

        horizontalLayout_5->addWidget(PB_protect_temp_ll);


        verticalLayout->addLayout(horizontalLayout_5);


        gridLayout_6->addLayout(verticalLayout, 1, 0, 2, 2);

        groupBox_4 = new QGroupBox(QTempCtrlDlg);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_4 = new QGridLayout(groupBox_4);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(-1, 20, -1, -1);
        LA_check_protect_current0 = new QLabel(groupBox_4);
        LA_check_protect_current0->setObjectName(QString::fromUtf8("LA_check_protect_current0"));

        gridLayout_4->addWidget(LA_check_protect_current0, 0, 0, 1, 1);

        PB_check_protect_current0 = new QPushButton(groupBox_4);
        PB_check_protect_current0->setObjectName(QString::fromUtf8("PB_check_protect_current0"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(PB_check_protect_current0->sizePolicy().hasHeightForWidth());
        PB_check_protect_current0->setSizePolicy(sizePolicy1);

        gridLayout_4->addWidget(PB_check_protect_current0, 0, 1, 1, 1);

        LA_check_protect_voltage0 = new QLabel(groupBox_4);
        LA_check_protect_voltage0->setObjectName(QString::fromUtf8("LA_check_protect_voltage0"));

        gridLayout_4->addWidget(LA_check_protect_voltage0, 1, 0, 1, 1);

        PB_check_protect_voltage0 = new QPushButton(groupBox_4);
        PB_check_protect_voltage0->setObjectName(QString::fromUtf8("PB_check_protect_voltage0"));
        sizePolicy1.setHeightForWidth(PB_check_protect_voltage0->sizePolicy().hasHeightForWidth());
        PB_check_protect_voltage0->setSizePolicy(sizePolicy1);

        gridLayout_4->addWidget(PB_check_protect_voltage0, 1, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_4, 1, 2, 1, 2);

        groupBox_5 = new QGroupBox(QTempCtrlDlg);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        gridLayout_5 = new QGridLayout(groupBox_5);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(-1, 20, -1, -1);
        LA_check_protect_current1 = new QLabel(groupBox_5);
        LA_check_protect_current1->setObjectName(QString::fromUtf8("LA_check_protect_current1"));

        gridLayout_5->addWidget(LA_check_protect_current1, 0, 0, 1, 1);

        PB_check_protect_current1 = new QPushButton(groupBox_5);
        PB_check_protect_current1->setObjectName(QString::fromUtf8("PB_check_protect_current1"));
        sizePolicy1.setHeightForWidth(PB_check_protect_current1->sizePolicy().hasHeightForWidth());
        PB_check_protect_current1->setSizePolicy(sizePolicy1);

        gridLayout_5->addWidget(PB_check_protect_current1, 0, 1, 1, 1);

        LA_check_protect_voltage1 = new QLabel(groupBox_5);
        LA_check_protect_voltage1->setObjectName(QString::fromUtf8("LA_check_protect_voltage1"));

        gridLayout_5->addWidget(LA_check_protect_voltage1, 1, 0, 1, 1);

        PB_check_protect_voltage1 = new QPushButton(groupBox_5);
        PB_check_protect_voltage1->setObjectName(QString::fromUtf8("PB_check_protect_voltage1"));
        sizePolicy1.setHeightForWidth(PB_check_protect_voltage1->sizePolicy().hasHeightForWidth());
        PB_check_protect_voltage1->setSizePolicy(sizePolicy1);

        gridLayout_5->addWidget(PB_check_protect_voltage1, 1, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_5, 2, 2, 1, 2);


        retranslateUi(QTempCtrlDlg);

        QMetaObject::connectSlotsByName(QTempCtrlDlg);
    } // setupUi

    void retranslateUi(QDialog *QTempCtrlDlg)
    {
        QTempCtrlDlg->setWindowTitle(QApplication::translate("QTempCtrlDlg", "Dialog", nullptr));
        groupBox->setTitle(QApplication::translate("QTempCtrlDlg", "\346\270\251\346\216\247\350\276\223\345\207\272\346\216\247\345\210\266", nullptr));
        RB_temp_output_on->setText(QApplication::translate("QTempCtrlDlg", "\346\270\251\346\216\247\350\276\223\345\207\272\344\275\277\350\203\275", nullptr));
        RB_temp_output_off->setText(QApplication::translate("QTempCtrlDlg", "\346\270\251\346\216\247\350\276\223\345\207\272\345\205\263\351\227\255", nullptr));
        groupBox_2->setTitle(QApplication::translate("QTempCtrlDlg", "\346\270\251\346\216\2471\350\276\223\345\207\272\346\230\276\347\244\272", nullptr));
        LA_output_voltage_0->setText(QApplication::translate("QTempCtrlDlg", "\350\276\223\345\207\272\347\224\265\345\216\213\357\274\2320.00V", nullptr));
        LA_output_current_0->setText(QApplication::translate("QTempCtrlDlg", "\350\276\223\345\207\272\347\224\265\346\265\201\357\274\2320.00A", nullptr));
        groupBox_3->setTitle(QApplication::translate("QTempCtrlDlg", "\346\270\251\346\216\2472\350\276\223\345\207\272\346\230\276\347\244\272", nullptr));
        LA_output_voltage_1->setText(QApplication::translate("QTempCtrlDlg", "\350\276\223\345\207\272\347\224\265\345\216\213\357\274\2320.00V", nullptr));
        LA_output_current_1->setText(QApplication::translate("QTempCtrlDlg", "\350\276\223\345\207\272\347\224\265\346\265\201\357\274\2320.00A", nullptr));
        PB_temp_set->setText(QApplication::translate("QTempCtrlDlg", "\346\270\251\345\272\246\350\256\276\347\275\256", nullptr));
        PB_protect_current->setText(QApplication::translate("QTempCtrlDlg", "\350\277\207\346\265\201\344\277\235\346\212\244\n"
"\351\230\210\345\200\274\350\256\276\347\275\256", nullptr));
        PB_portect_voltage->setText(QApplication::translate("QTempCtrlDlg", "\350\277\207\345\216\213\344\277\235\346\212\244\n"
"\351\230\210\345\200\274\350\256\276\347\275\256", nullptr));
        PB_protect_temp_hh->setText(QApplication::translate("QTempCtrlDlg", "\350\277\207\346\270\251\344\277\235\346\212\244\n"
"\351\253\230\351\230\210\345\200\274", nullptr));
        PB_protect_temp_ll->setText(QApplication::translate("QTempCtrlDlg", "\350\277\207\346\270\251\344\277\235\346\212\244\n"
"\344\275\216\351\230\210\345\200\274", nullptr));
        groupBox_4->setTitle(QApplication::translate("QTempCtrlDlg", "\346\237\245\350\257\242\346\270\251\346\216\2471", nullptr));
        LA_check_protect_current0->setText(QApplication::translate("QTempCtrlDlg", "0.00A", nullptr));
        PB_check_protect_current0->setText(QApplication::translate("QTempCtrlDlg", "\346\237\245\350\257\242\350\277\207\346\265\201\n"
"\344\277\235\346\212\244\351\230\210\345\200\274", nullptr));
        LA_check_protect_voltage0->setText(QApplication::translate("QTempCtrlDlg", "0.00V", nullptr));
        PB_check_protect_voltage0->setText(QApplication::translate("QTempCtrlDlg", "\346\237\245\350\257\242\350\277\207\345\216\213\n"
"\344\277\235\346\212\244\351\230\210\345\200\274", nullptr));
        groupBox_5->setTitle(QApplication::translate("QTempCtrlDlg", "\346\237\245\350\257\242\346\270\251\346\216\2472", nullptr));
        LA_check_protect_current1->setText(QApplication::translate("QTempCtrlDlg", "0.00A", nullptr));
        PB_check_protect_current1->setText(QApplication::translate("QTempCtrlDlg", "\346\237\245\350\257\242\350\277\207\346\265\201\n"
"\344\277\235\346\212\244\351\230\210\345\200\274", nullptr));
        LA_check_protect_voltage1->setText(QApplication::translate("QTempCtrlDlg", "0.00V", nullptr));
        PB_check_protect_voltage1->setText(QApplication::translate("QTempCtrlDlg", "\346\237\245\350\257\242\350\277\207\345\216\213\n"
"\344\277\235\346\212\244\351\230\210\345\200\274", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QTempCtrlDlg: public Ui_QTempCtrlDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTEMPCTRLDLG_H
