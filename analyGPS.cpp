#include "analyGPS.h"
char mUsefulBuf[128];

static int GetComma(int num,char *str){
	int i,j=0;
	int len=strlen(str);
	for(i=0;i<len;i++)
	{
		if(str[i]==',')
		{
			j++;
		}
		if(j==num)
		{
			return i+1;
		}
	}
	return 0;
} 

static double get_double_number(char *s)
{
	char buf[128];
	int i;
	double rev;
	i=GetComma(1,s);
	strncpy(buf,s,i);
	buf[i]=0;
	rev=atof(buf);
	return rev;
}

static void UTC2BTC(date_time *GPS)
{
	/*如果秒号先出，再出时间数据，则将时间数据+1秒*/
	GPS->second++;
	if(GPS->second>59){
		GPS->second=0;
		GPS->minute++;
		if(GPS->minute>59){
			GPS->minute=0;
			GPS->hour++;
		}
	}
	GPS->hour+=8;
	/*北京时间与世界时间相差8个时区，即相差8个小时*/
	if(GPS->hour>23){
		GPS->hour=0;
		GPS->day+=1;
	}
	if(GPS->month==2){
		if(GPS->year%100==0){
			if(GPS->year%400==0){
				if(GPS->day>29){
					GPS->day=1;
					GPS->month++;
				}
			}else{
				if(GPS->day>28){
					GPS->day=1;
					GPS->month++;
				}
			}

		}else{
			if(GPS->year%4==0){
				if(GPS->day>29){
					GPS->day=1;
					GPS->month++;
				}
			}else{
				if(GPS->day>28){
					GPS->day=1;
					GPS->month++;
				}
			}
		}
	}else if(GPS->month==4||GPS->month==6||GPS->month==9||GPS->month==11){
		if(GPS->day>30){
			GPS->day=1;
			GPS->month++;
		}
	}else if(GPS->month==1||GPS->month==3||GPS->month==5||GPS->month==7||GPS->month==8||GPS->month==10||GPS->month==12){
		if(GPS->day>31){
			GPS->day=1;
			GPS->month++;
		}
	}
	if(GPS->month>12){
		GPS->month=1;
		GPS->year++;
	}
}

int getTimeFromGPS(char *inBuf,int len,GPS_INFO *mGPS){
	char *pS = inBuf;
    char *pU = mUsefulBuf;
	int tmp=0;
	if(len <0) return -1;
    if(!(pU = strstr(pS,"$GNRMC"))){
		return -1;
    }
	mGPS->D.hour=(pU[7]-'0')*10+(pU[8]-'0');
	mGPS->D.minute=(pU[9]-'0')*10+(pU[10]-'0');
	mGPS->D.second=(pU[11]-'0')*10+(pU[12]-'0');
	tmp=GetComma(9,pU);
	mGPS->D.day=(pU[tmp+0]-'0')*10+(pU[tmp+1]-'0');
	mGPS->D.month=(pU[tmp+2]-'0')*10+(pU[tmp+3]-'0');
	mGPS->D.year=(pU[tmp+4]-'0')*10+(pU[tmp+5]-'0')+2000;
	tmp= GetComma(2,pU);
	if(pU[tmp]=='V'){
		mGPS->Valid = FALSE;
	}else if(pU[tmp]=='A'){
		mGPS->Valid = TRUE;
	}else{
        ls_info_debug("==========     %c\n",pU[tmp]);
		mGPS->Valid = FALSE;
	}
	UTC2BTC(&mGPS->D);
	return 1;
}

int getPlaceFromGPS(char *inBuf,int len,GPS_INFO *mGPS){
	char *pS = inBuf;
	char *pU = mUsefulBuf;
	int tmp = 0;
	double actualNum = 0.0;
    double actualDegree = 0.0;
    double actualMin = 0.0;
	int intNum = 0;
	double decNum = 0.0;
	if(len <0) return -1;
    if(!(pU = strstr(pS,"$GNRMC"))){
		return -1;
    }
	tmp= GetComma(2,pU);
	if(pU[tmp]=='V'){
		mGPS->Valid = FALSE;
	}else if(pU[tmp]=='A'){
		mGPS->Valid = TRUE;
	}else{
        printf("GPS data     %c\n",pU[tmp]);
		mGPS->Valid = FALSE;
    }
    if(!(pU = strstr(pS,"A"))&&!(pU = strstr(pS,"V"))){
        ls_info_debug("");
		return -1;
    }
    actualNum = get_double_number(&pU[GetComma(1,pU)]);
    mGPS->NS=pU[GetComma(2,pU)];
    mGPS->latitude.gpsData = actualNum;
    intNum = (int)(actualNum/1);
    actualDegree = intNum/100;
    actualMin = (intNum%100)*1.0+(actualNum - (intNum*1.0));
    mGPS->latitude.gpsDeg = actualDegree+(actualMin/60.0);
	if(intNum > 90){
		decNum = actualNum - (intNum*1.0);
		mGPS->latitude.degree = intNum/100;
		mGPS->latitude.minute = intNum%100;
		mGPS->latitude.second = (int)(decNum*60);
	}else{
		mGPS->latitude.degree = intNum;

		decNum = actualNum - (intNum*1.0);
		actualNum = decNum*60;
		intNum = (int)(actualNum/1);
		mGPS->latitude.minute = intNum;

		decNum = actualNum - (intNum*1.0);
		actualNum = decNum*60;
		intNum = (int)(actualNum/1);
		mGPS->latitude.second = (int)(decNum*60);
	}


    actualNum = get_double_number(&pU[GetComma(3,pU)]);
    mGPS->EW=pU[GetComma(4,pU)];
    mGPS->longitude.gpsData = actualNum;
    intNum = (int)(actualNum/1);
    actualDegree = intNum/100;
    actualMin = (intNum%100)*1.0+(actualNum - (intNum*1.0));
    mGPS->longitude.gpsDeg = actualDegree+(actualMin/60.0);
    if(intNum > 180){
        decNum = actualNum - (intNum*1.0);
        mGPS->longitude.degree = intNum/100;
        mGPS->longitude.minute = intNum%100;
        mGPS->longitude.second = (int)(decNum*60);
    }else{
        mGPS->longitude.degree = intNum;

        decNum = actualNum - (intNum*1.0);
        actualNum = decNum*60;
        intNum = (int)(actualNum/1);
        mGPS->longitude.minute = intNum;

        decNum = actualNum - (intNum*1.0);
        actualNum = decNum*60;
        intNum = (int)(actualNum/1);
        mGPS->longitude.second = (int)(decNum*60);
    }
	return 1;
}

void show_gps(GPS_INFO *GPS)
{
	if(GPS->Valid > 0){
        ls_info_debug("---------GPS unvaild--------");
	}else{
        ls_info_debug("---------GPS vaild--------");
	}
    ls_info_debug("DATE			:%ld-%02d-%02d",GPS->D.year,GPS->D.month,GPS->D.day);
    ls_info_debug("TIME			:%02d:%02d:%02d",GPS->D.hour,GPS->D.minute,GPS->D.second);
    ls_info_debug("Latitude		:%10.4f  %10.6f %c   %d %d %d ",GPS->latitude.gpsData,GPS->latitude.gpsDeg,GPS->NS,GPS->latitude.degree,GPS->latitude.minute,GPS->latitude.second);
    ls_info_debug("Longitude		:%10.4f  %10.6f %c   %d %d %d ",GPS->longitude.gpsData,GPS->longitude.gpsDeg,GPS->EW,GPS->longitude.degree,GPS->longitude.minute,GPS->longitude.second);
}

