#include "QEleSysCtrlDlg.h"
#include "ui_QEleSysCtrlDlg.h"

extern SendoutPtrBuffer eleOutQue;
QEleSysCtrlDlg::QEleSysCtrlDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QEleSysCtrlDlg)
{
    ui->setupUi(this);
    initEleSysDlg();
}

QEleSysCtrlDlg::~QEleSysCtrlDlg()
{
    delete ui;
}

void QEleSysCtrlDlg::initEleSysDlg()
{
    QString str="";
    LS_S32 ret = 0;
    QPalette palette(this->palette());
    palette.setColor(QPalette::Background,QColor(DLG_BK_COLOR));
    this->setPalette(palette);
    eEleSysDev = Ele_DevID_0;
    ui->CB_workmode->setView(new QListView());
    ui->CB_judge->setView(new QListView());
    ui->CB_workmode->insertItem(0,"不工作");
    ui->CB_workmode->insertItem(1,"封闭采集");
    ui->CB_workmode->insertItem(2,"联通采集");
    ui->CB_workmode->insertItem(3,"循环采集");
    ui->CB_workmode->setCurrentIndex(0);
    for(int i=0;i<16;i++){
        if(i<8){
            str=QString("上升沿%1次").arg(i+1);
        }else{
            str=QString("无上升沿%1次").arg(i-7);
        }
        ui->CB_judge->insertItem(i,str);
    }
    ui->CB_judge->setCurrentIndex(0);
    ret = operaEleSysArg(eEleSysDev,GET_OPERA,&tGlobalEleArg);
    if(ret !=LS_SUCC){
        ls_err_debug("operaEleSysArg ERR\n");
        emit this->signalEleSysCtrlMsg("读取电控参数配置失败，显示默认配置");
        tGlobalEleArg.iSysValue[Ele_ld_para]=128;
        tGlobalEleArg.iSysValue[Ele_las_tem]=0;
        tGlobalEleArg.iSysValue[Ele_throld]=12000;
        tGlobalEleArg.iSysValue[Ele_preshh]=0;
        tGlobalEleArg.iSysValue[Ele_presll]=0;
        tGlobalEleArg.iSysValue[Ele_coll_count]=10;
        tGlobalEleArg.iSysValue[Ele_volta_a]=17112;
        tGlobalEleArg.iSysValue[Ele_volta_b]=13631;
        tGlobalEleArg.iSysValue[Ele_volta_c]=16112;
        tGlobalEleArg.iSysValue[Ele_judge]=0;
        tGlobalEleArg.iSysValue[Ele_workmode]=0;
        tGlobalEleArg.iSysValue[Ele_rankse_vol]=16;
        tGlobalEleArg.iSysValue[Ele_rankse_value]=1;
    }
    setEleSysDlgShow(&tGlobalEleArg);
}

void QEleSysCtrlDlg::setEleSysDlgShow(TEleSysArg *para)
{
    QString str = "";
    str=QString("%1").arg(para->iSysValue[Ele_ld_para]);
    ui->LE_ld_para->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_las_tem]);
    ui->LE_las_tem->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_throld]);
    ui->LE_throld->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_preshh]);
    ui->LE_preshh->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_presll]);
    ui->LE_presll->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_coll_count]);
    ui->LE_coll_count->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_volta_a]);
    ui->LE_volta_a->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_volta_b]);
    ui->LE_volta_b->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_volta_c]);
    ui->LE_volta_c->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_rankse_vol]);
    ui->LE_rankse_vol->setText(str);

    str=QString("%1").arg(para->iSysValue[Ele_rankse_value]);
    ui->LE_rankse_value->setText(str);

    if(para->iSysValue[Ele_judge]>=0&&para->iSysValue[Ele_judge]<16)
    {
        ui->CB_judge->setCurrentIndex(para->iSysValue[Ele_judge]);
    }

    if(para->iSysValue[Ele_workmode]>=0&&para->iSysValue[Ele_workmode]<4)
    {
        ui->CB_workmode->setCurrentIndex(para->iSysValue[Ele_workmode]);
    }
}

void QEleSysCtrlDlg::setGlobalSysPara(TSysSet *para)
{
    memcpy(&tSysPara,para,sizeof(TSysSet));
}

void QEleSysCtrlDlg::setCtrlDev(Ele_Ctrl_Dev mDev)
{
    LS_S32 ret = 0;
    this->eEleSysDev = mDev;
    ret = operaEleSysArg(eEleSysDev,GET_OPERA,&tGlobalEleArg);
    if(ret !=LS_SUCC){
        ls_err_debug("operaEleSysArg ERR\n");
        emit this->signalEleSysCtrlMsg("读取电控参数配置失败，显示默认配置");
        tGlobalEleArg.iSysValue[Ele_ld_para]=128;
        tGlobalEleArg.iSysValue[Ele_las_tem]=0;
        tGlobalEleArg.iSysValue[Ele_throld]=12000;
        tGlobalEleArg.iSysValue[Ele_preshh]=0;
        tGlobalEleArg.iSysValue[Ele_presll]=0;
        tGlobalEleArg.iSysValue[Ele_coll_count]=10;
        tGlobalEleArg.iSysValue[Ele_volta_a]=17112;
        tGlobalEleArg.iSysValue[Ele_volta_b]=13631;
        tGlobalEleArg.iSysValue[Ele_volta_c]=16112;
        tGlobalEleArg.iSysValue[Ele_judge]=0;
        tGlobalEleArg.iSysValue[Ele_workmode]=0;
        tGlobalEleArg.iSysValue[Ele_rankse_vol]=16;
        tGlobalEleArg.iSysValue[Ele_rankse_value]=1;
    }
    setEleSysDlgShow(&tGlobalEleArg);
}

int QEleSysCtrlDlg::sendCtrlCmd(LS_U8 *buf,int len)
{
    LS_S32 ret = 0;
    if(len <= 0){
        ls_err_debug("len %d ERR",len);
        emit this->signalEleSysCtrlMsg("命令长度错误，无法发送控制指令");
        return LS_FAIL;
    }
    LS_S8_OUT_PACKET *mData=new LS_S8_OUT_PACKET;
    if(!mData){
        ls_info_debug("new LS_U8_OUT_PACKET ERR");
        return LS_FAIL;
    }
    if(eEleSysDev == Ele_DevID_0|| eEleSysDev == Ele_DevID_1){
        mData->data = (LS_S8 *)buf;
        mData->size = len;
        mData->devid = (int)eEleSysDev;
        eleOutQue.PushPacket(mData);
    }else{
        mData->data = (LS_S8 *)buf;
        mData->size = len;
        mData->devid = (int)Ele_DevID_0;
        eleOutQue.PushPacket(mData);
        mData->devid = (int)Ele_DevID_1;
        eleOutQue.PushPacket(mData);
    }
    return LS_SUCC;
}

void QEleSysCtrlDlg::on_PB_ld_para_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_ld_para;
    str = ui->LE_ld_para->text();
    value = str.toInt();
    if(value<0 || value > 255){
        ls_err_debug("ld_para %d ERR",value);
        str = QString("LD电流设定值%1应当介于0~255之间").arg(value);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_las_tem_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_las_tem;
    str = ui->LE_las_tem->text();
    value = str.toInt();
    if(value<0 || value > 255){
        ls_err_debug("ld_para %d ERR",value);
        str = QString("Laser温度设定值%1应当介于0~255之间").arg(value);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_throld_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_throld;
    str = ui->LE_throld->text();
    value = str.toInt();
    if(value<0 || value > 65535){
        ls_err_debug("Ele_throld %d ERR",value);
        str = QString("采集阈值%1应当介于0~65535之间").arg(value);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_preshh_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_preshh;
    str = ui->LE_preshh->text();
    value = str.toInt();
    if(value<0 || value > 65535){
        ls_err_debug("Ele_preshh %d ERR",value);
        str = QString("Laser压力高阈值%1应当介于0~65535之间").arg(value);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_presll_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_presll;
    str = ui->LE_presll->text();
    value = str.toInt();
    if(value<0 || value > 65535){
        ls_err_debug("Ele_presll %d ERR",value);
        str = QString("Laser压力低阈值%1应当介于0~65535之间").arg(value);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_coll_count_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_coll_count;
    str = ui->LE_coll_count->text();
    value = str.toInt();
    if(value<0 || value > 255){
        ls_err_debug("Ele_coll_count %d ERR",value);
        str = QString("采集次数%1应当介于0~255").arg(value);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_volta_a_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_volta_a;
    str = ui->LE_volta_a->text();
    value = str.toInt();
    if(value<0 || value > 65535){
        ls_err_debug("Ele_volta_a %d ERR",value);
        str = QString("DA-A值%1应当介于0~65535之间").arg(value);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_volta_b_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_volta_b;
    str = ui->LE_volta_b->text();
    value = str.toInt();
    if(value<0 || value > 65535){
        ls_err_debug("Ele_volta_b %d ERR",value);
        str = QString("DA-B值%1应当介于0~65535之间").arg(value);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_volta_c_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_volta_c;
    str = ui->LE_volta_c->text();
    value = str.toInt();
    if(value<0 || value > 65535){
        ls_err_debug("Ele_volta_c %d ERR",value);
        str = QString("DA-C值%1应当介于0~65535之间").arg(value);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_judge_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_judge;
    value = ui->CB_judge->currentIndex();
    if(value<0 || value > 15){
        ls_err_debug("Ele_judge %d ERR",value);
        str = QString("采集判别值逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_workmode_clicked()
{
    QString str = "";
    int value = 0;
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_workmode;
    value = ui->CB_workmode->currentIndex();
    if(value<0 || value > 3){
        ls_err_debug("Ele_workmode %d ERR",value);
        str = QString("工作模式逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    tGlobalEleArg.iSysValue[index]=value;
    operaEleSysArg(eEleSysDev,SET_OPERA,&tGlobalEleArg,index);
    len = sendOrder(index,sendBuf,value);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_rankse_clicked()
{

}

void QEleSysCtrlDlg::on_PB_pumpai_start_clicked()
{
    QString str = "";
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_pumpai_start;
    len = sendOrder(index,sendBuf);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_pumpai_stop_clicked()
{
    QString str = "";
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_pumpai_stop;
    len = sendOrder(index,sendBuf);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_dev_start_clicked()
{
    QString str = "";
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_dev_start;
    len = sendOrder(index,sendBuf);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_dev_stop_clicked()
{
    QString str = "";
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_dev_stop;
    len = sendOrder(index,sendBuf);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_save_para_clicked()
{
    QString str = "";
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_save_para;
    len = sendOrder(index,sendBuf);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_dev_reset_clicked()
{
    QString str = "";
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_dev_reset;
    len = sendOrder(index,sendBuf);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_request_para_clicked()
{
    QString str = "";
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_request_para;
    len = sendOrder(index,sendBuf);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSysCtrlDlg::on_PB_request_status_clicked()
{
    QString str = "";
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    EleSysPara index = Ele_request_status;
    len = sendOrder(index,sendBuf);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误文件%1 错误行数%2").arg(__FILE__).arg(__LINE__);
        emit this->signalEleSysCtrlMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}
