#include "QEleInfoDlg.h"
#include "ui_QEleInfoDlg.h"

QEleInfoDlg::QEleInfoDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QEleInfoDlg)
{
    ui->setupUi(this);
    initEleInfoDlg();
}

QEleInfoDlg::~QEleInfoDlg()
{
    delete ui;
}

void QEleInfoDlg::destroyDlg()
{
    bDestroy = true;
}

void QEleInfoDlg::initEleInfoDlg()
{
    LS_U8 initBuf[128]={0};
    float fValue[6]={0};
    QPalette palette(this->palette());
    palette.setColor(QPalette::Background,QColor(DLG_BK_COLOR));
    bDestroy = false;
    this->setPalette(palette);
    showStatus(Ele_DevID_0,initBuf);
    showReserve(Ele_DevID_0,fValue,sizeof(fValue)/sizeof(float));
    showPara(Ele_DevID_0,initBuf);
    showStatus(Ele_DevID_1,initBuf);
    showReserve(Ele_DevID_1,fValue,sizeof(fValue)/sizeof(float));
    showPara(Ele_DevID_1,initBuf);
}

void QEleInfoDlg::showStatus(int index,LS_U8 *buf)
{
    QString str = "";
    LS_S8 valueBuf[32]={0};
    LS_S32 value = 0;
    LS_F64 dValue=0.0;
    LS_S32 len = 0;
    LS_U8 *p = buf+8;
    if(bDestroy)return;
    if(index != Ele_DevID_0 && index != Ele_DevID_1)return;
    eleInfoStatuMtx.lock();
    value = Hex2Int(p+len,2);len+=2;
    dValue =(value-16383*0.1)/13106.4*2.00;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.3f",dValue);
    str = QString("通道1压力值：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_ch1_press->setText(str);
    }else {
        ui->LA_ch1_press_2->setText(str);
    }
    value = Hex2Int(p+len,2);len+=2;
    dValue = value/2048*200-50;;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.3f",dValue);
    str = QString("通道1温度值：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_ch1_temp->setText(str);
    }else {
        ui->LA_ch1_temp_2->setText(str);
    }

    value = Hex2Int(p+len,2);len+=2;
    dValue =(value-16383*0.1)/13106.4*2.00;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.3f",dValue);
    str = QString("通道2压力值：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_ch2_press->setText(str);
    }else {
        ui->LA_ch2_press_2->setText(str);
    }

    value = Hex2Int(p+len,2);len+=2;
    dValue = value/2048*200-50;;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.3f",dValue);
    str = QString("通道2温度值：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_ch2_temp->setText(str);
    }else {
        ui->LA_ch2_temp_2->setText(str);
    }

    dValue = 0.0;
    sprintf(valueBuf,"%.3f",dValue);
    str = QString("湿度值：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_humidity->setText(str);
    }else {
        ui->LA_humidity_2->setText(str);
    }
    eleInfoStatuMtx.unlock();
}

void QEleInfoDlg::showPara(int index,LS_U8 *buf)
{
    QString str = "";
    LS_S8 valueBuf[32]={0};
    LS_S32 value = 0;
    LS_S32 len = 0;
    LS_U8 *p = buf+8;
    value = p[0];
    if(bDestroy)return;
    if(index != Ele_DevID_0 && index != Ele_DevID_1)return;
    eleInfoParaMtx.lock();
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("调制电压步长：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_vol_step->setText(str);
    }else {
        ui->LA_vol_step_2->setText(str);
    }

    value = p[1];
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("平均次数：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_aver_count->setText(str);
    }else {
        ui->LA_aver_count_2->setText(str);
    }

    value = p[2];
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("采集次数：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_coll_count->setText(str);
    }else {
        ui->LA_coll_count_2->setText(str);
    }

    value = p[3];
    if(value==1){
        str = "工作模式：封闭采集";
    }else if(value==2){
        str = "工作模式：联通采集";
    }else if(value==3){
        str = "工作模式：循环采集";
    }else{
        str = "工作模式：不工作";
    }
    if(index == Ele_DevID_0){
        ui->LA_workmode->setText(str);
    }else {
        ui->LA_workmode_2->setText(str);
    }

    value = Hex2Int(p+len,2);len+=2;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("激光电流值1：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_laser_current_1->setText(str);
    }else {
        ui->LA_laser_current_1_2->setText(str);
    }

    value = Hex2Int(p+len,2);len+=2;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("激光电流值2：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_laser_current_2->setText(str);
    }else {
        ui->LA_laser_current_2_2->setText(str);
    }

    len+=2;
    value = Hex2Int(p+len,2);len+=2;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("压力低阈值：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_press_ll->setText(str);
    }else {
        ui->LA_press_ll_2->setText(str);
    }

    value = Hex2Int(p+len,2);len+=2;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("压力高阈值：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_press_hh->setText(str);
    }else {
        ui->LA_press_hh_2->setText(str);
    }

    value = Hex2Int(p+len,2);len+=2;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("配置C电压：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_voltage_c->setText(str);
    }else {
        ui->LA_voltage_c_2->setText(str);
    }

    value = Hex2Int(p+len,2);len+=2;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("配置B电压：%1").arg(valueBuf);
    if(index == Ele_DevID_0){
        ui->LA_voltage_b->setText(str);
    }else {
        ui->LA_voltage_b_2->setText(str);
    }

    value = Hex2Int(p+len,2);len+=2;
    memset(valueBuf,0,sizeof(valueBuf));
    sprintf(valueBuf,"%.5d",value);
    str = QString("配置A电压：%1").arg(valueBuf);
    ui->LA_voltage_a->setText(str);
    if(index == Ele_DevID_0){
        ui->LA_voltage_a->setText(str);
    }else {
        ui->LA_voltage_a_2->setText(str);
    }
    eleInfoParaMtx.unlock();
}

void QEleInfoDlg::showReserve(int index,float *fValue,int iCount)
{
    LS_S8 valueBuf[32]={0};
    LS_S32 value = 0;
    QString str = "";
    char flagBuf[6][20]={"预留位1","预留位2","预留位3","预留位4","预留位5","预留位6"};
    if(bDestroy)return;
    if(index != Ele_DevID_0 && index != Ele_DevID_1)return;
    eleInfoReserveMtx.lock();
    for(int i=0;i<iCount;i++){
        memset(valueBuf,0,sizeof(valueBuf));
        sprintf(valueBuf,"%.3f",fValue[0]);
        str = QString("%1：%2").arg(flagBuf[i]).arg(valueBuf);
        if(i==0){
            if(index == Ele_DevID_0){
                ui->LA_reserve_0->setText(str);
            }else{
                ui->LA_reserve_0_2->setText(str);
            }
        }else if(i==1){
            if(index == Ele_DevID_0){
                ui->LA_reserve_1->setText(str);
            }else{
                ui->LA_reserve_1_2->setText(str);
            }
        }else if(i==2){
            if(index == Ele_DevID_0){
                ui->LA_reserve_2->setText(str);
            }else{
                ui->LA_reserve_2_2->setText(str);
            }
        }else if(i==3){
            if(index == Ele_DevID_0){
                ui->LA_reserve_3->setText(str);
            }else{
                ui->LA_reserve_3_2->setText(str);
            }
        }else if(i==4){
            if(index == Ele_DevID_0){
                ui->LA_reserve_4->setText(str);
            }else{
                ui->LA_reserve_4_2->setText(str);
            }
        }else if(i==5){
            if(index == Ele_DevID_0){
                ui->LA_reserve_5->setText(str);
            }else{
                ui->LA_reserve_5_2->setText(str);
            }
        }
    }
    eleInfoReserveMtx.unlock();
}
