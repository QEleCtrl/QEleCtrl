#include "QEleSwitchDlg.h"
#include "ui_QEleSwitchDlg.h"

extern SendoutPtrBuffer eleOutQue;
QEleSwitchDlg::QEleSwitchDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QEleSwitchDlg)
{
    ui->setupUi(this);
    initEleSwitchDlg();
}

QEleSwitchDlg::~QEleSwitchDlg()
{
    delete ui;
}

void QEleSwitchDlg::initEleSwitchDlg()
{
    QPalette palette(this->palette());
    palette.setColor(QPalette::Background,QColor(DLG_BK_COLOR));
    this->setPalette(palette);
    this->eEleSysDev = Ele_DevID_0;
    setCtrlDev(eEleSysDev);

}

void QEleSwitchDlg::setGlobalSysPara(TSysSet *para)
{
    memcpy(&tSysPara,para,sizeof(TSysSet));
}

void QEleSwitchDlg::setCtrlDev(Ele_Ctrl_Dev mDev)
{
    LS_S32 ret = 0;
    eEleSysDev = mDev;
    ret = operaEleSwitch(eEleSysDev,GET_OPERA,&tGlobalEleSwitch);
    if(ret !=LS_SUCC){
        ls_err_debug("operaEleSwitch ERR\n");
        emit this->signalEleSwitchMsg("读取电机电磁阀控制参数失败，显示默认配置");
        tGlobalEleSwitch.bSwitch[Ele_loop_pump]=true;
        tGlobalEleSwitch.bSwitch[Ele_boost_pump]=true;
        tGlobalEleSwitch.bSwitch[Ele_vacuum_pump]=true;
        tGlobalEleSwitch.bSwitch[Ele_dfb_laser]=true;
        tGlobalEleSwitch.bSwitch[Ele_temp_ctrl]=true;
        tGlobalEleSwitch.bSwitch[Ele_gate1]=true;
        tGlobalEleSwitch.bSwitch[Ele_gate2]=true;
        tGlobalEleSwitch.bSwitch[Ele_gate3]=true;
        tGlobalEleSwitch.bSwitch[Ele_solenoid1]=true;
        tGlobalEleSwitch.bSwitch[Ele_solenoid2]=true;
        tGlobalEleSwitch.bSwitch[Ele_solenoid3]=true;
        tGlobalEleSwitch.bSwitch[Ele_solenoid4]=true;
        tGlobalEleSwitch.bSwitch[Ele_data_collect]=true;
    }
    setEleSwitchDlgShow(&tGlobalEleSwitch);
}

void QEleSwitchDlg::setEleSwitchDlgShow(TEleSwitch *para)
{
    if(para->bSwitch[Ele_loop_pump]){
        ui->RB_loop_pump_on->setChecked(true);
        ui->RB_loop_pump_off->setChecked(false);
    }else{
        ui->RB_loop_pump_on->setChecked(false);
        ui->RB_loop_pump_off->setChecked(true);
    }

    if(para->bSwitch[Ele_boost_pump]){
        ui->RB_boost_pump_on->setChecked(true);
        ui->RB_boost_pump_off->setChecked(false);
    }else{
        ui->RB_boost_pump_on->setChecked(false);
        ui->RB_boost_pump_off->setChecked(true);
    }

    if(para->bSwitch[Ele_vacuum_pump]){
        ui->RB_vacuum_pump_on->setChecked(true);
        ui->RB_vacuum_pump_off->setChecked(false);
    }else{
        ui->RB_vacuum_pump_on->setChecked(false);
        ui->RB_vacuum_pump_off->setChecked(true);
    }

    if(para->bSwitch[Ele_dfb_laser]){
        ui->RB_dfb_laser_on->setChecked(true);
        ui->RB_dfb_laser_off->setChecked(false);
    }else{
        ui->RB_dfb_laser_on->setChecked(false);
        ui->RB_dfb_laser_off->setChecked(true);
    }

    if(para->bSwitch[Ele_temp_ctrl]){
        ui->RB_temp_ctrl_on->setChecked(true);
        ui->RB_temp_ctrl_off->setChecked(false);
    }else{
        ui->RB_temp_ctrl_on->setChecked(false);
        ui->RB_temp_ctrl_off->setChecked(true);
    }

    if(para->bSwitch[Ele_gate1]){
        ui->RB_gate1_on->setChecked(true);
        ui->RB_gate1_off->setChecked(false);
    }else{
        ui->RB_gate1_on->setChecked(false);
        ui->RB_gate1_off->setChecked(true);
    }

    if(para->bSwitch[Ele_gate2]){
        ui->RB_gate2_on->setChecked(true);
        ui->RB_gate2_off->setChecked(false);
    }else{
        ui->RB_gate2_on->setChecked(false);
        ui->RB_gate2_off->setChecked(true);
    }

    if(para->bSwitch[Ele_gate3]){
        ui->RB_gate3_on->setChecked(true);
        ui->RB_gate3_off->setChecked(false);
    }else{
        ui->RB_gate3_on->setChecked(false);
        ui->RB_gate3_off->setChecked(true);
    }

    if(para->bSwitch[Ele_solenoid1]){
        ui->RB_solenoid1_on->setChecked(true);
        ui->RB_solenoid1_off->setChecked(false);
    }else{
        ui->RB_solenoid1_on->setChecked(false);
        ui->RB_solenoid1_off->setChecked(true);
    }

    if(para->bSwitch[Ele_solenoid2]){
        ui->RB_solenoid2_on->setChecked(true);
        ui->RB_solenoid2_off->setChecked(false);
    }else{
        ui->RB_solenoid2_on->setChecked(false);
        ui->RB_solenoid2_off->setChecked(true);
    }

    if(para->bSwitch[Ele_solenoid3]){
        ui->RB_solenoid3_on->setChecked(true);
        ui->RB_solenoid3_off->setChecked(false);
    }else{
        ui->RB_solenoid3_on->setChecked(false);
        ui->RB_solenoid3_off->setChecked(true);
    }

    if(para->bSwitch[Ele_solenoid4]){
        ui->RB_solenoid4_on->setChecked(true);
        ui->RB_solenoid4_off->setChecked(false);
    }else{
        ui->RB_solenoid4_on->setChecked(false);
        ui->RB_solenoid4_off->setChecked(true);
    }

    if(para->bSwitch[Ele_data_collect]){
        ui->RB_data_collect_on->setChecked(true);
        ui->RB_data_collect_off->setChecked(false);
    }else{
        ui->RB_data_collect_on->setChecked(false);
        ui->RB_data_collect_off->setChecked(true);
    }
}

int QEleSwitchDlg::sendCtrlCmd(LS_U8 *buf,int len)
{
    LS_S32 ret = 0;
    if(len <= 0){
        ls_err_debug("len %d ERR",len);
        emit this->signalEleSwitchMsg("命令长度错误，无法发送控制指令");
        return LS_FAIL;
    }
    LS_S8_OUT_PACKET *mData=new LS_S8_OUT_PACKET;
    if(!mData){
        ls_info_debug("new LS_U8_OUT_PACKET ERR");
        return LS_FAIL;
    }
    if(eEleSysDev == Ele_DevID_0|| eEleSysDev == Ele_DevID_1){
        mData->data = (LS_S8 *)buf;
        mData->size = len;
        mData->devid = (int)eEleSysDev;
        eleOutQue.PushPacket(mData);
    }else{
        mData->data = (LS_S8 *)buf;
        mData->size = len;
        mData->devid = (int)Ele_DevID_0;
        eleOutQue.PushPacket(mData);
        mData->devid = (int)Ele_DevID_1;
        eleOutQue.PushPacket(mData);
    }
    delete mData;
    mData = NULL;
    return LS_SUCC;
}

void QEleSwitchDlg::setEleDevOnOff(EleSwitch index,bool onOff)
{
    QString str = "";
    LS_U8 sendBuf[64]={0};
    LS_S32 len = 0;
    if(index == Ele_boost_pump){
        ui->RB_boost_pump_on->setChecked(onOff);
        ui->RB_boost_pump_off->setChecked(!onOff);
    }else if(index == Ele_vacuum_pump){
        ui->RB_vacuum_pump_on->setChecked(onOff);
        ui->RB_vacuum_pump_off->setChecked(!onOff);
    }else if(index == Ele_solenoid1){
        ui->RB_solenoid1_on->setChecked(onOff);
        ui->RB_solenoid1_off->setChecked(!onOff);
    }else if(index == Ele_solenoid2){
        ui->RB_solenoid2_on->setChecked(onOff);
        ui->RB_solenoid2_off->setChecked(!onOff);
    }else if(index == Ele_solenoid3){
        ui->RB_solenoid3_on->setChecked(onOff);
        ui->RB_solenoid3_off->setChecked(!onOff);
    }else if(index == Ele_solenoid4){
        ui->RB_solenoid4_on->setChecked(onOff);
        ui->RB_solenoid4_off->setChecked(!onOff);
    }else if(index == Ele_gate1){
        ui->RB_gate1_on->setChecked(onOff);
        ui->RB_gate1_off->setChecked(!onOff);
    }else if(index == Ele_gate2){
        ui->RB_gate2_on->setChecked(onOff);
        ui->RB_gate2_off->setChecked(!onOff);
    }else if(index == Ele_gate3){
        ui->RB_gate2_on->setChecked(onOff);
        ui->RB_gate2_off->setChecked(!onOff);
    }else if(index == Ele_loop_pump){
        ui->RB_loop_pump_on->setChecked(onOff);
        ui->RB_loop_pump_off->setChecked(!onOff);
    }else if(index == Ele_data_collect){
        ui->RB_data_collect_on->setChecked(onOff);
        ui->RB_data_collect_off->setChecked(!onOff);
    }else if(index == Ele_temp_ctrl){
        ui->RB_temp_ctrl_on->setChecked(onOff);
        ui->RB_temp_ctrl_off->setChecked(!onOff);
    }else if(index == Ele_dfb_laser){
        ui->RB_dfb_laser_on->setChecked(onOff);
        ui->RB_dfb_laser_off->setChecked(!onOff);
    }else{
        return;
    }
    if(tGlobalEleSwitch.bSwitch[index] != onOff){
        tGlobalEleSwitch.bSwitch[index] = onOff;
        operaEleSwitch(eEleSysDev,SET_OPERA,&tGlobalEleSwitch,index);
    }
    len = sendDeviceOnOff(index,sendBuf,onOff);
    if(len <= 0){
        ls_err_debug("ERR");
        str = QString("逻辑错误，不应当发生,错误设备%1 错误文件%2 错误行数%3").arg(index).arg(__FILE__).arg(__LINE__);
        emit this->signalEleSwitchMsg(str);
        return;
    }
    sendCtrlCmd(sendBuf,len);
}

void QEleSwitchDlg::on_RB_loop_pump_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_loop_pump;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_loop_pump_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_loop_pump;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_boost_pump_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_boost_pump;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_boost_pump_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_boost_pump;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_vacuum_pump_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_vacuum_pump;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_vacuum_pump_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_vacuum_pump;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_dfb_laser_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_dfb_laser;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_dfb_laser_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_dfb_laser;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_temp_ctrl_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_temp_ctrl;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_temp_ctrl_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_temp_ctrl;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_gate1_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_gate1;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_gate1_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_gate1;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_gate2_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_gate2;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_gate2_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_gate2;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_gate3_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_gate3;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_gate3_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_gate3;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_solenoid1_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_solenoid1;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_solenoid1_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_solenoid1;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_solenoid2_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_solenoid2;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_solenoid2_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_solenoid2;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_solenoid3_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_solenoid3;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_solenoid3_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_solenoid3;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_solenoid4_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_solenoid4;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_solenoid4_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_solenoid4;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_data_collect_on_clicked()
{
    bool onOff = true;
    EleSwitch index = Ele_data_collect;
    setEleDevOnOff(index,onOff);
}

void QEleSwitchDlg::on_RB_data_collect_off_clicked()
{
    bool onOff = false;
    EleSwitch index = Ele_data_collect;
    setEleDevOnOff(index,onOff);
}
