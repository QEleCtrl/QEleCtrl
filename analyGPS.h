#ifndef _ANALYGPS_H_
#define _ANALYGPS_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

typedef struct{
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;
}date_time;/*时间结构体结束*/

typedef struct {
	double gpsData;
    double gpsDeg;
	int degree;
	int minute;
	int second;
}location_info;

typedef struct{
	date_time		D;			/*时间*/
	location_info	latitude;	/*纬度*/
	location_info	longitude;	/*经度*/
	char			NS;			/*南北极*/
    char			EW;			/*东西半球*/
	int Valid;
}GPS_INFO;

int getTimeFromGPS(char *inBuf,int len,GPS_INFO *mGPS);
int getPlaceFromGPS(char *inBuf,int len,GPS_INFO *mGPS);
void show_gps(GPS_INFO *GPS);

#endif
