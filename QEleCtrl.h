#ifndef QELECTRL_H
#define QELECTRL_H

#include <QWidget>
#include <QtCharts>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QContextMenuEvent>
#include <QtCharts/QSplineSeries>
#include <QDateTime>
#include <QTimer>
#include <QQueue>
#include <pthread.h>
#include <QCloseEvent>
#include <QMutex>
#include <QDesktopWidget>
#include "types.h"
#include "config.h"
#include "util.h"
#include "mathutil.h"
#include "saveData.h"
#include "analyGPS.h"
#include "dataBuffer.h"
#include "sendoutBuffer.h"
#include "packSocket.h"
#include "QSysSetDlg.h"
#include "QEleInfoDlg.h"
#include "QEleChartDlg.h"
#include "QTempCtrlDlg.h"
#include "QEleSwitchDlg.h"
#include "QEleSysCtrlDlg.h"
#include "temProto.h"

QT_CHARTS_USE_NAMESPACE

namespace Ui {
class QEleCtrl;
}

class QEleCtrl : public QWidget
{
    Q_OBJECT

public:
    explicit QEleCtrl(QWidget *parent = 0);
    ~QEleCtrl();

protected:
    void contextMenuEvent(QContextMenuEvent *event);
    void closeEvent(QCloseEvent *e);
private:
    Ui::QEleCtrl *ui;

private:
    QSysSetDlg      *dSysSet;
    QEleInfoDlg     *dEleInfo;
    QEleSysCtrlDlg  *dEleSysCtrl;
    QEleSwitchDlg   *dEleSwitch;
    QEleChartDlg    *dEleChart;
    QTempCtrlDlg    *dTempCtrl;
    QChart          *chart[ELE_MAX_NUM];
    QSplineSeries   *series[ELE_MAX_NUM];
    QTimer          *countTimer;
    QTimer          *checkTimer;
    QTimer          *gpsAskTimer;
    QTimer          *eleHeartTimer;
    QTimer          *tempHeartTimer;
    QMenu           *pRightMenu;
    QRect           QDlgRect[6];
    TSoftRect       tDlgRect;
    saveData        *mSave[ELE_MAX_NUM];
    QMutex          handlComMtx;
    QMutex          handlDataMtx;
    QMutex          handlTempMtx;
    QMutex          showMsgMtx;
    double          chartMaxValue[ELE_MAX_NUM];
    double          chartMinValue[ELE_MAX_NUM];
    LS_S8           cGlobalSaveDir[MAX_PATH];

public:
    QSerialPort     *QSGpsUart;
    QSerialPort     *QSEleDevUart[ELE_MAX_NUM];
    QSerialPort     *QSEleTempUart[ELE_MAX_NUM];
    DataPtrBuffer   *commQue[ELE_MAX_NUM];
    DataPtrBuffer   *dataQue[ELE_MAX_NUM];
    DataPtrBuffer   *tempQue[ELE_MAX_NUM];
    DataPtrBuffer   *eleQue[ELE_MAX_NUM];
    DataPtrBuffer   *gpsQue;
    LS_U32          iGlobalDevRecvDataCount[ELE_MAX_NUM];
private:
    LS_U32          iGlobalSoftRunTime;
    LS_U32          iGlobalSaveCount[ELE_MAX_NUM];
    LS_U32          iGlobalCheckCount[ELE_MAX_NUM];
public:
    QQueue<float>    chartQue[ELE_MAX_NUM];
    TSysSet         tGlobalSysSet;
    bool            bAllThreadExit;

    pthread_t       pRecvEleDevTid[2];
    pthread_t       pRecvTempTid[2];
    pthread_t       pAnaComRecvTid[2];
    pthread_t       pAnaDataRecvTid[2];
    pthread_t       pRecvGpsTid;
    pthread_t       pSendoutTid;
    pthread_t       pSendTempTid;
private:
    void drawLine(int index,float value);
    void initChart();
    void initQSerials();
    void initCtrlShow();
    void setWidgetTheme();
    void initThreads();
    void createRightMenu();
    void initChildWidgets();
    void initMyDlg();
    void initVariables();
    void updateRunTimerShow();
    void stopGpsRecvThread();
    void stopEleRecvThread(int index);
    void stopAllThreads();
    void openEleUart(Ele_Ctrl_Dev,int port);
    void openTempUart(Ele_Ctrl_Dev,int port);
public:
    void sendTempCmd(Ele_Ctrl_Dev dev,void *tmp,int len);
    void handleRecvGpsBuf(void *buf,LS_S32 len);
    void handleRecvDataBuf(LS_S32 index,void *buf,LS_S32 len);
    void handleRecvCommBuf(LS_S32 index,void *buf,LS_S32 len);
    void handleRecvTempBuf(LS_S32 index,void *buf,LS_S32 len);
signals:
    void mainShowMsg(QString str,MsgType type=MSG_ERR);
    void mainShowStatus(int index,LS_U8 *buf);
    void mainShowPara(int index,LS_U8 *buf);
    void mainShowReserve(int index,float *,int);
    void mainSendEleUart(int index,LS_S8 *p,int len);
    void mainSendTempUart(int index,LS_S8 *p,int len);
public slots:
    void countTimerDeal();
    void slotSysSet();
    void slotEleChartShow();
    void slotEleCtrl();
    void slotSwithCtrl();
    void slotTempCtrl();
    void slotEleArgStatusShow();
    void slotChildUnfold();
    void slotChildFold();
    void slotCountReset();
    void slotMsgReset();
    void slotRecvSysSetRet(TSysSet *tSysArg,QString);
    void showMsg(QString str,MsgType type=MSG_ERR);
    void slotSendEleUart(int index,LS_S8 *p,int len);
    void slotSendTempUart(int index,LS_S8 *p,int len);
private slots:
    void readGpsUart();
    void readEleUart_0();
    void readEleUart_1();
    void readTempUart_0();
    void readTempUart_1();
    void sendEleHeart();
    void sendTempHeart();
    void sendGpsAskCmd();
    void checkDevOnline();
    void on_RB_ELE_0_clicked();
    void on_RB_ELE_1_clicked();
    void on_RB_ELE_BOTH_clicked();
};

#endif // QELECTRL_H
