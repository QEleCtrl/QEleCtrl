#ifndef _PACK_SOCKET_H_
#define _PACK_SOCKET_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <qglobal.h>
#ifdef Q_OS_LINUX
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>
#elif defined (Q_OS_WIN)
#include <WinSock2.h>
#include <Windows.h>
#include <Ws2tcpip.h>
#endif
#define SOCK_TRUE 			1
#define SOCK_FALSE 			0
#define SOCK_TIMEOUT		0

enum SelectType{READ_SELECT=0,WRITE_SELECT=1};

enum SocketType{UDP_SOCK=0,TCP_SOCK=1};

int createSocket(enum SocketType mtype);

int myselect(int sockfd, int usec, enum SelectType mtype);

void setSocketNonblock(int sock);

int joinGroupSock(int sockfd, char *groupip);

int writeUdpSocket(int sockfd, char* dstIPAddr, int port, void *sendBuf, int bufLen);

int bindSocket (int sockfd,int port);

int listenTcpSocket(int sockfd);

int acceptTcpSocket(int sockfd,char *ipAddr);

int connectTcpSocket(int sockfd, char *dstIPAddr, int port);

int writeTcpSocket(int sockfd, void *sendBuf, int bufLen);

int readTCPSocket(int sockfd, char* recvBuf,int bufLen, int *recvLen);

void closePackSocket(int *sockfd);

LS_S8 getIPType(char *ipAddr);

#endif
