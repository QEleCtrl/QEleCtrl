#include "eleProto.h"
#include "util.h"
#include "packSocket.h"

int setLDPara(LS_U8 *buf,int value)
{
	int len =0;
	if(value <0 || value>255)return -1;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'L';buf[len++]=(LS_U8)'D';buf[len++]=(LS_U8)'P';
    buf[len++]=(LS_U8)'A';buf[len++]=(LS_U8)'R';buf[len++]=(LS_U8)'A';
	buf[len++]=0x0D;
	Int2Hex(value,buf+len,2);
	len+=2;
	buf[len++]=getBufXor(buf,len);
	buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setLDPTem(LS_U8 *buf,int value)
{
	int len =0;
	if(value <0 || value>255)return -1;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'L';buf[len++]=(LS_U8)'D';buf[len++]=(LS_U8)'P';
    buf[len++]=(LS_U8)'T';buf[len++]=(LS_U8)'E';buf[len++]=(LS_U8)'M';
	buf[len++]=0x0D;
	Int2Hex(value,buf+len,2);
	len+=2;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setLaserTem(LS_U8 *buf,int value)
{
	int len =0;
	if(value <0 || value>255)return -1;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'S';buf[len++]=(LS_U8)'E';buf[len++]=(LS_U8)'P';
    buf[len++]=(LS_U8)'A';buf[len++]=(LS_U8)'R';buf[len++]=(LS_U8)'A';
	buf[len++]=0x0D;
	Int2Hex(value,buf+len,2);
	len+=2;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setRankse(LS_U8 *buf,int value_voltage,int value_num)
{
	int len =0;
	buf[len++]=0x24;
	if(value_voltage <0 || value_voltage>256)return -1;
	if(value_num <0 || value_num>255)return -2;
    buf[len++]=(LS_U8)'R';buf[len++]=(LS_U8)'A';buf[len++]=(LS_U8)'N';
    buf[len++]=(LS_U8)'K';buf[len++]=(LS_U8)'S';buf[len++]=(LS_U8)'E';
	buf[len++]=0x0D;
	buf[len++] = value_num;
	buf[len++] = value_voltage;
	buf[len] &= 0x80;
	//len+=2;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setThrold(LS_U8 *buf,int value)
{
	int len =0;
	if(value <0 || value>65535)return -1;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'T';buf[len++]=(LS_U8)'H';buf[len-1]++;buf[len++]=(LS_U8)'R';
    buf[len++]=(LS_U8)'O';buf[len++]=(LS_U8)'L';buf[len++]=(LS_U8)'D';
	buf[len++]=0x0D;
	Int2Hex(value,buf+len,2);
	len+=2;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setPreshh(LS_U8 *buf,int value)
{
	int len =0;
	if(value <0 || value>65535)return -1;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'P';buf[len++]=(LS_U8)'R';buf[len++]=(LS_U8)'E';
    buf[len++]=(LS_U8)'S';buf[len++]=(LS_U8)'H';buf[len++]=(LS_U8)'H';
	buf[len++]=0x0D;
	Int2Hex(value,buf+len,2);
	len+=2;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setPresll(LS_U8 *buf,int value)
{
	int len =0;
	if(value <0 || value>65535)return -1;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'P';buf[len++]=(LS_U8)'R';buf[len++]=(LS_U8)'E';
    buf[len++]=(LS_U8)'S';buf[len++]=(LS_U8)'L';buf[len++]=(LS_U8)'L';
	buf[len++]=0x0D;
	Int2Hex(value,buf+len,2);
	len+=2;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setVolta(LS_U8 *buf,int index,int value)
{
	int len =0;
	buf[len++]=0x24;
	if(value <0 || value>65535)return -1;
    buf[len++]=(LS_U8)'V';buf[len++]=(LS_U8)'O';buf[len++]=(LS_U8)'L';
    buf[len++]=(LS_U8)'T';buf[len++]=(LS_U8)'A';
	if(index==0){
		buf[len++]=0x01;
	}else if(index==1) {
		buf[len++]=0x02;
	}else if(index == 2){
		buf[len++]=0x03;
	}
	buf[len++]=0x0D;
	Int2Hex(value,buf+len,2);
	len+=2;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setPumpai(LS_U8 *buf,int index)
{
	int len =0;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'P';buf[len++]=(LS_U8)'U';buf[len++]=(LS_U8)'M';
    buf[len++]=(LS_U8)'P';buf[len++]=(LS_U8)'A';buf[len++]=(LS_U8)'I';
	buf[len++]=0x0D;
	buf[len++]=0x00;
	if(index==0){buf[len++]=0x01;}
	else {buf[len++]=0x00;}
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setReset(LS_U8 *buf)
{
	int len =0;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'R';buf[len++]=(LS_U8)'E';buf[len++]=(LS_U8)'S';
    buf[len++]=(LS_U8)'E';buf[len++]=(LS_U8)'T';buf[len++]=(LS_U8)'T';
	buf[len++]=0x0D;
	buf[len++]=0x00;buf[len++]=0x00;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setRunTop(LS_U8 *buf,int index)
{
	int len =0;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'R';buf[len++]=(LS_U8)'U';buf[len++]=(LS_U8)'N';
    buf[len++]=(LS_U8)'T';buf[len++]=(LS_U8)'O';buf[len++]=(LS_U8)'P';
	buf[len++]=0x0D;
	buf[len++]=0x00;
	if(index==0){buf[len++]=0x01;}
	else {buf[len++]=0x00;}
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setSavepa(LS_U8 *buf)
{
	int len =0;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'S';buf[len++]=(LS_U8)'A';buf[len++]=(LS_U8)'V';
    buf[len++]=(LS_U8)'E';buf[len++]=(LS_U8)'P';buf[len++]=(LS_U8)'A';
	buf[len++]=0x0D;
	buf[len++]=0x00;buf[len++]=0x00;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setRequet(LS_U8 *buf,int index)
{
	int len =0;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'R';buf[len++]=(LS_U8)'E';buf[len++]=(LS_U8)'Q';
    buf[len++]=(LS_U8)'U';buf[len++]=(LS_U8)'E';buf[len++]=(LS_U8)'T';
	buf[len++]=0x0D;
	buf[len++]=0x00;
	if(index==0){buf[len++]=0x00;}
	else {buf[len++]=0x01;}
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setWorkmode(LS_U8 *buf,int value)
{
	int len =0;
	if(value <0 || value>3)return -1;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'W';buf[len++]=(LS_U8)'O';buf[len++]=(LS_U8)'M';
    buf[len++]=(LS_U8)'O';buf[len++]=(LS_U8)'D';buf[len++]=(LS_U8)'E';
	buf[len++]=0x0D;
	Int2Hex(value,buf+len,2);
	len+=2;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int setCollectTime(LS_U8 *buf,int value)
{
	int len =0;
	if(value <0 || value>255)return -1;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'S';buf[len++]=(LS_U8)'Q';buf[len++]=(LS_U8)'U';
    buf[len++]=(LS_U8)'A';buf[len++]=(LS_U8)'N';buf[len++]=(LS_U8)'T';
	buf[len++]=0x0D;
	Int2Hex(value,buf+len,2);
	len+=2;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int sendOrder(EleSysPara orderType,LS_U8 *sendBuf,int value)
{
	int ret = 0;
    if(orderType<Ele_ld_para||orderType>Ele_request_status)return -1;
    switch(orderType){
        case Ele_ld_para:
            ret=setLDPara(sendBuf,value);
            break;
        case Ele_las_tem:
            ret=setLaserTem(sendBuf,value);
            break;
        case Ele_throld:
            ret=setThrold(sendBuf,value);
            break;
        case Ele_preshh:
            ret=setPreshh(sendBuf,value);
            break;
        case Ele_presll:
            ret=setPresll(sendBuf,value);
            break;
        case Ele_volta_a:
            ret=setVolta(sendBuf,0,value);
            break;
        case Ele_volta_b:
            ret=setVolta(sendBuf,1,value);
            break;
        case Ele_volta_c:
            ret=setVolta(sendBuf,2,value);
            break;
        case Ele_coll_count:
            ret = setCollectTime(sendBuf,value);
            break;
        case Ele_judge:
            ret=setLDPTem(sendBuf,value);
            break;
        case Ele_workmode:
            ret = setWorkmode(sendBuf,value);
            break;
        case Ele_rankse_vol:
        case Ele_rankse_value:
            //setRankse(sendBuf,value);
            break;
        case Ele_pumpai_start:
            ret=setPumpai(sendBuf,0);
            break;
        case Ele_pumpai_stop:
            ret=setPumpai(sendBuf,1);
            break;
        case Ele_dev_start:
            ret=setRunTop(sendBuf,0);
            break;
        case Ele_dev_stop:
            ret=setRunTop(sendBuf,1);
            break;
        case Ele_save_para:
            ret=setSavepa(sendBuf);
            break;
        case Ele_dev_reset:
            ret=setReset(sendBuf);
            break;
        case Ele_request_para:
            ret=setRequet(sendBuf,0);
            break;
        case Ele_request_status:
            ret=setRequet(sendBuf,1);
            break;
			default:
				break;
    }
	return ret;
}

int sendDeviceOnOff(EleSwitch devid,LS_U8 *buf,bool onOff)
{
	int len = 0;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'S';buf[len++]=(LS_U8)'W';buf[len++]=(LS_U8)'I';
    buf[len++]=(LS_U8)'T';buf[len++]=(LS_U8)'C';buf[len++]=(LS_U8)'H';
	buf[len++]=0x0D;
    buf[len++]=devid+1;
	if(onOff){
		buf[len++]=0x01;
	}else{
		buf[len++]=0x00;
	}
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int sendDataBoardOnOff(bool onOff,LS_U8 *buf)
{
	int len = 0;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'S';buf[len++]=(LS_U8)'W';buf[len++]=(LS_U8)'I';
    buf[len++]=(LS_U8)'T';buf[len++]=(LS_U8)'C';buf[len++]=(LS_U8)'H';
	buf[len++]=0x0D;
	if(onOff){
		buf[len++]=0x11;
		buf[len++]=0x01;
	}else{
		buf[len++]=0x12;
		buf[len++]=0x00;
	}
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}

int sendCollectJudge(LS_U8 *buf,int judgeType)
{
	int len =0;
	if(judgeType <0 || judgeType>0x1f)return -1;
	buf[len++]=0x24;
    buf[len++]=(LS_U8)'L';buf[len++]=(LS_U8)'D';buf[len++]=(LS_U8)'P';
    buf[len++]=(LS_U8)'T';buf[len++]=(LS_U8)'E';buf[len++]=(LS_U8)'M';
	buf[len++]=0x0D;
	buf[len++]=0x00;
	buf[len++]=judgeType;
	buf[len++]=getBufXor(buf,len);buf[len++]=0x0D;buf[len++]=0x0A;
	return len;
}
