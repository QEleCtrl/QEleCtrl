#include "util.h"

void Int2Hex(int num,void *p,int n){
#if 1
	int i = 0;
	int m = n-1;
	unsigned char *tmp = (unsigned char *)p;
	tmp[n-1] = num & 0xff;
	for(i =1;i<n;i++){
		tmp[m-i] = num >> (i<<3);
	}
#else
	p[0] = num & 0xff;
	for(int i =1;i<n;i++){
		p[i] = num >> (i<<3);
	}
#endif
}

int Hex2Int(void *p,int n){
#if 1
	int num = 0;
	int i=0;
	unsigned char *tmp = (unsigned char *)p;
	while(i<n){
		num |= tmp[i]<<((n-i-1)<<3);
		i++;
	}
#else
	int num = 0;
	while(n--){
		num |= p[n]<<(n<<3);
	}
#endif
	return num;
}

int getBufSum(void *p,int n)
{
	unsigned char *tmp = (unsigned char *)p;
	int i=0,num=0;
	int dSum = 0;
	for(i=0;i<n;i++){
		dSum += tmp[i];
	}
	num = dSum&0xff;
	return num;
}

void printBufByHex(LS_S8 *file,LS_S32 line,void *tmp,int len)
{
    unsigned char *buf = (unsigned char*)tmp;
    printf("%s %d ",file,line);
    for(int i=0;i<len;i++){
        printf("%.2X ",buf[i]);
        fflush(stdout);
    }
    printf("\n");
    fflush(stdout);
}

void QString2Char(QString str,char *buf)
{
    if(str.length()<=0)return;
    QByteArray ba = str.toLatin1(); // must
    char *ch=ba.data();
    memcpy(buf,ch,str.length());
}

int createNewDir(char *dirPath)
{
    QDir dir;
    QString dirStr = QString("%1").arg(dirPath);
    if (!dir.exists(dirStr))
    {
        if(!dir.mkpath(dirStr))return LS_FAIL;
    }
    return LS_SUCC;
}


int checkIfHaveHead(unsigned char *buf,int len,int *mCount,int *mLocation)
{	
	int i = 0;	
	*mCount = 0;
	int num = 0;
	for(i=0;i<len;i++){		
		if(buf[i]==0x7E && buf[i+1]==0x81 && buf[i+2]==0x03){
			mLocation[num]=i;
			num ++;	
		}	
	}
	*mCount = num;
	return num;
}

int splitBuf(char **arr, char *str, const char *del)
{
	char *s =NULL; 
	int num = 0;
	s=strtok(str,del);
	while(s != NULL)
	{
		*arr++ = s;
		s = strtok(NULL,del);
		num ++;
	}
	return num;
}

int checkEleHeadCount(void *tmp,int len,int *mCount,int *mLocation)
{	
	int i = 0;	
    LS_U8 *buf = (LS_U8 *)tmp;
	*mCount = 0;
	int num = 0;
	for(i=0;i<len-1;i++){		
		if(buf[i]==0x24){
			if(buf[i+1]==0x4C||buf[i+1]==0x53||buf[i+1]==0x52||
				buf[i+1]==0x54||buf[i+1]==0x50||buf[i+1]==0x56||buf[i+1]==0xAA){
					if(buf[i+2]!=0x23&&buf[i+2]!=0x24&&buf[i+2]!=0x25){
						mLocation[num]=i;
						num ++;	
					}
			}
		}	
	}
	*mCount = num;
	return num;
}

int checkGpsHeadCount(void *tmp,int len,int *mCount,int *mLocation)
{
    int i = 0;
    *mCount = 0;
    int num = 0;
    LS_U8 *buf = (LS_U8 *)tmp;
    for(i=0;i<len-1;i++){
        if(buf[i]==0x01&&buf[i+1]==0x03&&buf[i+2]==0x46&&buf[i+3]==0x24){
            mLocation[num]=i;
            num ++;
        }
    }
    *mCount = num;
    return num;
}

int checkTempHeadCount(void *tmp,int len,int *mCount,int *mLocation)
{
    int i = 0;
    *mCount = 0;
    int num = 0;
    LS_U8 *buf = (LS_U8 *)tmp;
    for(i=0;i<len-1;i++){
        if(buf[i]==0x54&&buf[i+1]==0x43&&buf[i+2]==0x31&&buf[i+3]==0x3A){
            mLocation[num]=i;
            num ++;
        }
    }
    *mCount = num;
    return num;
}

void getTimeFromNum(LS_U32 mcount,int *h,int *m,int *s)
{
	int mHour=mcount/3600;
	int mMin=(mcount-mHour*3600)/60;
	int mSec = mcount%60;
	*h=mHour;
	*m=mMin;
	*s = mSec;
}

bool isIpAddr(const QString &ip)
{
    QRegExp rx2("((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)");
    if(rx2.exactMatch(ip))
    {
        return true;
    }
    return false;
}

bool isSysArgChanged(TSysSet *arg1,TSysSet *arg2)
{
    for(int i=0;i<ELE_MAX_NUM;i++){
        if(arg1->eleDevPort[i] != arg2->eleDevPort[i])return true;
        if(arg1->eleTempPort[i] != arg2->eleTempPort[i])return true;
    }
    if(arg1->gpsPort != arg2->gpsPort)return true;
    return false;
}

int getBufXor(LS_U8 *buf,int len)
{
    unsigned int num = 0;
    num = buf[0]^buf[1];
    for(int i=2;i<len;i++){
        num = (num^buf[i]);
    }
    return num;
}

void myMSleep(LS_U64 msec)
{
#ifdef Q_OS_LINUX
    usleep(1000*msec);
#elif defined (Q_OS_WIN)
    Sleep(msec);
#endif
}

int str2HexStr(char *srcBuf,int len,char *dstStr)
{
    int ret = 0,bufLen=0;
    for(int i=0;i<len;i++){
        ret = sprintf(dstStr,"%.2X ",(unsigned char)srcBuf[i]);
        dstStr+=ret;
        bufLen+=ret;
    }
    return bufLen;
}

void getCurrentDateTime(int *year,int *month,int *day,int *hour,int *min,int *sec)
{
    QDateTime current_date_time =QDateTime::currentDateTime();
    *year = current_date_time.date().year();
    *month = current_date_time.date().month();
    *day = current_date_time.date().day();

    *hour = current_date_time.time().hour();
    *min = current_date_time.time().minute();
    *sec = current_date_time.time().second();
}
