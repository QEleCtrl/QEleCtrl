#ifndef _ELE_PROTO_H_
#define _ELE_PROTO_H_
#include <QString>
#include "types.h"

static QString orderIm[] = {"ld电流设定","采集判别设定","laser温度设定","采样平均数设定","采集阈值设定", \
							"laser压力高阈值设定","laser压力低阈值设定","da a通道设定","da b通道设定","激光器换气到存储室开始", \
							"激光器换气到存储室停止","复位","启动","停止","存储数据","参数读取","状态读取", \
							"信令应答","参数应答","状态应答","采集次数设置","工作模式设置","da C通道设定",\
							"深海水循环泵","隔膜真空泵","隔膜增压泵","DFB激光驱动","温度控制模块","电磁阀1",\
							"电磁阀2","电磁阀3","电磁阀4",\
							"数据采集板","选通电磁阀1","选通电磁阀2","选通电磁阀3", \
							"温控输出状态查询","温控输出控制","温控实际输出电压查询","温控实际输出电流查询", \
							"温控过压阈值设定","温控过压阈值查询","温控过流保护阈值设定","温控过流保护阈值查询", \
							"温控实际温度查询","温控设定温度","温控过温保护错误", \
							"温控过低保护阈值设定","温控过温保护使能查询"};

int sendOrder(EleSysPara orderType,LS_U8 *sendBuf,int value=-1);
int setRankse(LS_U8 *buf,int value_voltage,int value_num);
int sendCollectJudge(LS_U8 *buf,int judgeType);
int sendDeviceOnOff(EleSwitch devid,LS_U8 *buf,bool onOff);
int sendDataBoardOnOff(bool onOff,LS_U8 *buf);

#endif
