/********************************************************************************
** Form generated from reading UI file 'QEleSysCtrlDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QELESYSCTRLDLG_H
#define UI_QELESYSCTRLDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_QEleSysCtrlDlg
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLineEdit *LE_ld_para;
    QPushButton *PB_ld_para;
    QHBoxLayout *horizontalLayout_9;
    QLineEdit *LE_las_tem;
    QPushButton *PB_las_tem;
    QHBoxLayout *horizontalLayout_10;
    QLineEdit *LE_throld;
    QPushButton *PB_throld;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *LE_preshh;
    QPushButton *PB_preshh;
    QHBoxLayout *horizontalLayout_6;
    QLineEdit *LE_presll;
    QPushButton *PB_presll;
    QHBoxLayout *horizontalLayout_5;
    QLineEdit *LE_coll_count;
    QPushButton *PB_coll_count;
    QHBoxLayout *horizontalLayout_7;
    QLineEdit *LE_volta_a;
    QPushButton *PB_volta_a;
    QHBoxLayout *horizontalLayout_11;
    QLineEdit *LE_volta_b;
    QPushButton *PB_volta_b;
    QHBoxLayout *horizontalLayout_12;
    QLineEdit *LE_volta_c;
    QPushButton *PB_volta_c;
    QHBoxLayout *horizontalLayout_2;
    QComboBox *CB_judge;
    QPushButton *PB_judge;
    QHBoxLayout *horizontalLayout_3;
    QComboBox *CB_workmode;
    QPushButton *PB_workmode;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_8;
    QLineEdit *LE_rankse_vol;
    QLineEdit *LE_rankse_value;
    QPushButton *PB_rankse;
    QHBoxLayout *horizontalLayout_13;
    QPushButton *PB_pumpai_start;
    QPushButton *PB_pumpai_stop;
    QHBoxLayout *horizontalLayout_14;
    QPushButton *PB_dev_start;
    QPushButton *PB_dev_stop;
    QHBoxLayout *horizontalLayout_15;
    QPushButton *PB_save_para;
    QPushButton *PB_dev_reset;
    QHBoxLayout *horizontalLayout_16;
    QPushButton *PB_request_para;
    QPushButton *PB_request_status;

    void setupUi(QDialog *QEleSysCtrlDlg)
    {
        if (QEleSysCtrlDlg->objectName().isEmpty())
            QEleSysCtrlDlg->setObjectName(QString::fromUtf8("QEleSysCtrlDlg"));
        QEleSysCtrlDlg->resize(650, 440);
        QEleSysCtrlDlg->setMinimumSize(QSize(0, 0));
        QEleSysCtrlDlg->setMaximumSize(QSize(7500, 7000));
        gridLayout = new QGridLayout(QEleSysCtrlDlg);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        LE_ld_para = new QLineEdit(QEleSysCtrlDlg);
        LE_ld_para->setObjectName(QString::fromUtf8("LE_ld_para"));
        LE_ld_para->setMinimumSize(QSize(100, 25));
        LE_ld_para->setMaximumSize(QSize(200, 45));
        QFont font;
        font.setPointSize(12);
        LE_ld_para->setFont(font);
        LE_ld_para->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(LE_ld_para);

        PB_ld_para = new QPushButton(QEleSysCtrlDlg);
        PB_ld_para->setObjectName(QString::fromUtf8("PB_ld_para"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PB_ld_para->sizePolicy().hasHeightForWidth());
        PB_ld_para->setSizePolicy(sizePolicy);
        PB_ld_para->setMinimumSize(QSize(100, 0));
        PB_ld_para->setMaximumSize(QSize(200, 16777215));

        horizontalLayout->addWidget(PB_ld_para);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        LE_las_tem = new QLineEdit(QEleSysCtrlDlg);
        LE_las_tem->setObjectName(QString::fromUtf8("LE_las_tem"));
        LE_las_tem->setMinimumSize(QSize(100, 25));
        LE_las_tem->setMaximumSize(QSize(200, 45));
        LE_las_tem->setFont(font);
        LE_las_tem->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(LE_las_tem);

        PB_las_tem = new QPushButton(QEleSysCtrlDlg);
        PB_las_tem->setObjectName(QString::fromUtf8("PB_las_tem"));
        sizePolicy.setHeightForWidth(PB_las_tem->sizePolicy().hasHeightForWidth());
        PB_las_tem->setSizePolicy(sizePolicy);
        PB_las_tem->setMinimumSize(QSize(100, 0));
        PB_las_tem->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_9->addWidget(PB_las_tem);


        verticalLayout_2->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        LE_throld = new QLineEdit(QEleSysCtrlDlg);
        LE_throld->setObjectName(QString::fromUtf8("LE_throld"));
        LE_throld->setMinimumSize(QSize(100, 25));
        LE_throld->setMaximumSize(QSize(200, 45));
        LE_throld->setFont(font);
        LE_throld->setAlignment(Qt::AlignCenter);

        horizontalLayout_10->addWidget(LE_throld);

        PB_throld = new QPushButton(QEleSysCtrlDlg);
        PB_throld->setObjectName(QString::fromUtf8("PB_throld"));
        sizePolicy.setHeightForWidth(PB_throld->sizePolicy().hasHeightForWidth());
        PB_throld->setSizePolicy(sizePolicy);
        PB_throld->setMinimumSize(QSize(100, 0));
        PB_throld->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_10->addWidget(PB_throld);


        verticalLayout_2->addLayout(horizontalLayout_10);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        LE_preshh = new QLineEdit(QEleSysCtrlDlg);
        LE_preshh->setObjectName(QString::fromUtf8("LE_preshh"));
        LE_preshh->setMinimumSize(QSize(100, 25));
        LE_preshh->setMaximumSize(QSize(200, 45));
        LE_preshh->setFont(font);
        LE_preshh->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(LE_preshh);

        PB_preshh = new QPushButton(QEleSysCtrlDlg);
        PB_preshh->setObjectName(QString::fromUtf8("PB_preshh"));
        sizePolicy.setHeightForWidth(PB_preshh->sizePolicy().hasHeightForWidth());
        PB_preshh->setSizePolicy(sizePolicy);
        PB_preshh->setMinimumSize(QSize(100, 0));
        PB_preshh->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_4->addWidget(PB_preshh);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        LE_presll = new QLineEdit(QEleSysCtrlDlg);
        LE_presll->setObjectName(QString::fromUtf8("LE_presll"));
        LE_presll->setMinimumSize(QSize(100, 25));
        LE_presll->setMaximumSize(QSize(200, 45));
        LE_presll->setFont(font);
        LE_presll->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(LE_presll);

        PB_presll = new QPushButton(QEleSysCtrlDlg);
        PB_presll->setObjectName(QString::fromUtf8("PB_presll"));
        sizePolicy.setHeightForWidth(PB_presll->sizePolicy().hasHeightForWidth());
        PB_presll->setSizePolicy(sizePolicy);
        PB_presll->setMinimumSize(QSize(100, 0));
        PB_presll->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_6->addWidget(PB_presll);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        LE_coll_count = new QLineEdit(QEleSysCtrlDlg);
        LE_coll_count->setObjectName(QString::fromUtf8("LE_coll_count"));
        LE_coll_count->setMinimumSize(QSize(100, 25));
        LE_coll_count->setMaximumSize(QSize(200, 45));
        LE_coll_count->setFont(font);
        LE_coll_count->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(LE_coll_count);

        PB_coll_count = new QPushButton(QEleSysCtrlDlg);
        PB_coll_count->setObjectName(QString::fromUtf8("PB_coll_count"));
        sizePolicy.setHeightForWidth(PB_coll_count->sizePolicy().hasHeightForWidth());
        PB_coll_count->setSizePolicy(sizePolicy);
        PB_coll_count->setMinimumSize(QSize(100, 0));
        PB_coll_count->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_5->addWidget(PB_coll_count);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        LE_volta_a = new QLineEdit(QEleSysCtrlDlg);
        LE_volta_a->setObjectName(QString::fromUtf8("LE_volta_a"));
        LE_volta_a->setMinimumSize(QSize(100, 25));
        LE_volta_a->setMaximumSize(QSize(200, 45));
        LE_volta_a->setFont(font);
        LE_volta_a->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(LE_volta_a);

        PB_volta_a = new QPushButton(QEleSysCtrlDlg);
        PB_volta_a->setObjectName(QString::fromUtf8("PB_volta_a"));
        sizePolicy.setHeightForWidth(PB_volta_a->sizePolicy().hasHeightForWidth());
        PB_volta_a->setSizePolicy(sizePolicy);
        PB_volta_a->setMinimumSize(QSize(100, 0));
        PB_volta_a->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_7->addWidget(PB_volta_a);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        LE_volta_b = new QLineEdit(QEleSysCtrlDlg);
        LE_volta_b->setObjectName(QString::fromUtf8("LE_volta_b"));
        LE_volta_b->setMinimumSize(QSize(100, 25));
        LE_volta_b->setMaximumSize(QSize(200, 45));
        LE_volta_b->setFont(font);
        LE_volta_b->setAlignment(Qt::AlignCenter);

        horizontalLayout_11->addWidget(LE_volta_b);

        PB_volta_b = new QPushButton(QEleSysCtrlDlg);
        PB_volta_b->setObjectName(QString::fromUtf8("PB_volta_b"));
        sizePolicy.setHeightForWidth(PB_volta_b->sizePolicy().hasHeightForWidth());
        PB_volta_b->setSizePolicy(sizePolicy);
        PB_volta_b->setMinimumSize(QSize(100, 0));
        PB_volta_b->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_11->addWidget(PB_volta_b);


        verticalLayout_2->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        LE_volta_c = new QLineEdit(QEleSysCtrlDlg);
        LE_volta_c->setObjectName(QString::fromUtf8("LE_volta_c"));
        LE_volta_c->setMinimumSize(QSize(100, 25));
        LE_volta_c->setMaximumSize(QSize(200, 45));
        LE_volta_c->setFont(font);
        LE_volta_c->setAlignment(Qt::AlignCenter);

        horizontalLayout_12->addWidget(LE_volta_c);

        PB_volta_c = new QPushButton(QEleSysCtrlDlg);
        PB_volta_c->setObjectName(QString::fromUtf8("PB_volta_c"));
        sizePolicy.setHeightForWidth(PB_volta_c->sizePolicy().hasHeightForWidth());
        PB_volta_c->setSizePolicy(sizePolicy);
        PB_volta_c->setMinimumSize(QSize(100, 0));
        PB_volta_c->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_12->addWidget(PB_volta_c);


        verticalLayout_2->addLayout(horizontalLayout_12);


        gridLayout->addLayout(verticalLayout_2, 0, 0, 7, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        CB_judge = new QComboBox(QEleSysCtrlDlg);
        CB_judge->setObjectName(QString::fromUtf8("CB_judge"));
        CB_judge->setMinimumSize(QSize(0, 45));

        horizontalLayout_2->addWidget(CB_judge);

        PB_judge = new QPushButton(QEleSysCtrlDlg);
        PB_judge->setObjectName(QString::fromUtf8("PB_judge"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(PB_judge->sizePolicy().hasHeightForWidth());
        PB_judge->setSizePolicy(sizePolicy1);
        PB_judge->setMinimumSize(QSize(0, 45));

        horizontalLayout_2->addWidget(PB_judge);


        gridLayout->addLayout(horizontalLayout_2, 0, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        CB_workmode = new QComboBox(QEleSysCtrlDlg);
        CB_workmode->setObjectName(QString::fromUtf8("CB_workmode"));
        CB_workmode->setMinimumSize(QSize(0, 45));

        horizontalLayout_3->addWidget(CB_workmode);

        PB_workmode = new QPushButton(QEleSysCtrlDlg);
        PB_workmode->setObjectName(QString::fromUtf8("PB_workmode"));
        sizePolicy1.setHeightForWidth(PB_workmode->sizePolicy().hasHeightForWidth());
        PB_workmode->setSizePolicy(sizePolicy1);
        PB_workmode->setMinimumSize(QSize(0, 45));
        PB_workmode->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_3->addWidget(PB_workmode);


        gridLayout->addLayout(horizontalLayout_3, 1, 1, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        LE_rankse_vol = new QLineEdit(QEleSysCtrlDlg);
        LE_rankse_vol->setObjectName(QString::fromUtf8("LE_rankse_vol"));
        LE_rankse_vol->setMinimumSize(QSize(60, 25));
        LE_rankse_vol->setMaximumSize(QSize(100, 45));
        LE_rankse_vol->setFont(font);
        LE_rankse_vol->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(LE_rankse_vol);

        LE_rankse_value = new QLineEdit(QEleSysCtrlDlg);
        LE_rankse_value->setObjectName(QString::fromUtf8("LE_rankse_value"));
        LE_rankse_value->setMinimumSize(QSize(60, 25));
        LE_rankse_value->setMaximumSize(QSize(100, 45));
        LE_rankse_value->setFont(font);
        LE_rankse_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(LE_rankse_value);


        verticalLayout->addLayout(horizontalLayout_8);

        PB_rankse = new QPushButton(QEleSysCtrlDlg);
        PB_rankse->setObjectName(QString::fromUtf8("PB_rankse"));
        sizePolicy1.setHeightForWidth(PB_rankse->sizePolicy().hasHeightForWidth());
        PB_rankse->setSizePolicy(sizePolicy1);
        PB_rankse->setMinimumSize(QSize(0, 45));

        verticalLayout->addWidget(PB_rankse);

        verticalLayout->setStretch(1, 1);

        gridLayout->addLayout(verticalLayout, 2, 1, 1, 1);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        PB_pumpai_start = new QPushButton(QEleSysCtrlDlg);
        PB_pumpai_start->setObjectName(QString::fromUtf8("PB_pumpai_start"));
        sizePolicy1.setHeightForWidth(PB_pumpai_start->sizePolicy().hasHeightForWidth());
        PB_pumpai_start->setSizePolicy(sizePolicy1);
        PB_pumpai_start->setMinimumSize(QSize(120, 50));

        horizontalLayout_13->addWidget(PB_pumpai_start);

        PB_pumpai_stop = new QPushButton(QEleSysCtrlDlg);
        PB_pumpai_stop->setObjectName(QString::fromUtf8("PB_pumpai_stop"));
        sizePolicy1.setHeightForWidth(PB_pumpai_stop->sizePolicy().hasHeightForWidth());
        PB_pumpai_stop->setSizePolicy(sizePolicy1);
        PB_pumpai_stop->setMinimumSize(QSize(120, 50));

        horizontalLayout_13->addWidget(PB_pumpai_stop);


        gridLayout->addLayout(horizontalLayout_13, 3, 1, 1, 1);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        PB_dev_start = new QPushButton(QEleSysCtrlDlg);
        PB_dev_start->setObjectName(QString::fromUtf8("PB_dev_start"));
        sizePolicy1.setHeightForWidth(PB_dev_start->sizePolicy().hasHeightForWidth());
        PB_dev_start->setSizePolicy(sizePolicy1);
        PB_dev_start->setMinimumSize(QSize(120, 50));

        horizontalLayout_14->addWidget(PB_dev_start);

        PB_dev_stop = new QPushButton(QEleSysCtrlDlg);
        PB_dev_stop->setObjectName(QString::fromUtf8("PB_dev_stop"));
        sizePolicy1.setHeightForWidth(PB_dev_stop->sizePolicy().hasHeightForWidth());
        PB_dev_stop->setSizePolicy(sizePolicy1);
        PB_dev_stop->setMinimumSize(QSize(120, 50));

        horizontalLayout_14->addWidget(PB_dev_stop);


        gridLayout->addLayout(horizontalLayout_14, 4, 1, 1, 1);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        PB_save_para = new QPushButton(QEleSysCtrlDlg);
        PB_save_para->setObjectName(QString::fromUtf8("PB_save_para"));
        sizePolicy1.setHeightForWidth(PB_save_para->sizePolicy().hasHeightForWidth());
        PB_save_para->setSizePolicy(sizePolicy1);
        PB_save_para->setMinimumSize(QSize(120, 50));

        horizontalLayout_15->addWidget(PB_save_para);

        PB_dev_reset = new QPushButton(QEleSysCtrlDlg);
        PB_dev_reset->setObjectName(QString::fromUtf8("PB_dev_reset"));
        sizePolicy1.setHeightForWidth(PB_dev_reset->sizePolicy().hasHeightForWidth());
        PB_dev_reset->setSizePolicy(sizePolicy1);
        PB_dev_reset->setMinimumSize(QSize(120, 50));

        horizontalLayout_15->addWidget(PB_dev_reset);


        gridLayout->addLayout(horizontalLayout_15, 5, 1, 1, 1);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        PB_request_para = new QPushButton(QEleSysCtrlDlg);
        PB_request_para->setObjectName(QString::fromUtf8("PB_request_para"));
        sizePolicy1.setHeightForWidth(PB_request_para->sizePolicy().hasHeightForWidth());
        PB_request_para->setSizePolicy(sizePolicy1);
        PB_request_para->setMinimumSize(QSize(120, 50));

        horizontalLayout_16->addWidget(PB_request_para);

        PB_request_status = new QPushButton(QEleSysCtrlDlg);
        PB_request_status->setObjectName(QString::fromUtf8("PB_request_status"));
        sizePolicy1.setHeightForWidth(PB_request_status->sizePolicy().hasHeightForWidth());
        PB_request_status->setSizePolicy(sizePolicy1);
        PB_request_status->setMinimumSize(QSize(120, 50));

        horizontalLayout_16->addWidget(PB_request_status);


        gridLayout->addLayout(horizontalLayout_16, 6, 1, 1, 1);


        retranslateUi(QEleSysCtrlDlg);

        QMetaObject::connectSlotsByName(QEleSysCtrlDlg);
    } // setupUi

    void retranslateUi(QDialog *QEleSysCtrlDlg)
    {
        QEleSysCtrlDlg->setWindowTitle(QApplication::translate("QEleSysCtrlDlg", "\347\224\265\346\216\247\347\263\273\347\273\237\346\216\247\345\210\266", nullptr));
        PB_ld_para->setText(QApplication::translate("QEleSysCtrlDlg", "LD\347\224\265\346\265\201\350\256\276\345\256\232\n"
"(0~255)", nullptr));
        PB_las_tem->setText(QApplication::translate("QEleSysCtrlDlg", "Laser\346\270\251\345\272\246\350\256\276\345\256\232\n"
"(0~255)", nullptr));
        PB_throld->setText(QApplication::translate("QEleSysCtrlDlg", "\351\207\207\351\233\206\351\230\210\345\200\274\350\256\276\345\256\232\n"
"(0~65535)", nullptr));
        PB_preshh->setText(QApplication::translate("QEleSysCtrlDlg", "Laser\345\216\213\345\212\233\351\253\230\351\230\210\345\200\274\n"
"(0~65535)", nullptr));
        PB_presll->setText(QApplication::translate("QEleSysCtrlDlg", "Laser\345\216\213\345\212\233\344\275\216\351\230\210\345\200\274\n"
"(0~65535)", nullptr));
        PB_coll_count->setText(QApplication::translate("QEleSysCtrlDlg", "\351\207\207\351\233\206\346\254\241\346\225\260\350\256\276\347\275\256\n"
"(0~10)", nullptr));
        PB_volta_a->setText(QApplication::translate("QEleSysCtrlDlg", "DA A\351\200\232\351\201\223\350\256\276\345\256\232\n"
"(0~65535)", nullptr));
        PB_volta_b->setText(QApplication::translate("QEleSysCtrlDlg", "DA B\351\200\232\351\201\223\350\256\276\345\256\232\n"
"(0~65535)", nullptr));
        PB_volta_c->setText(QApplication::translate("QEleSysCtrlDlg", "DA C\351\200\232\351\201\223\350\256\276\345\256\232\n"
"(0~65535)", nullptr));
        PB_judge->setText(QApplication::translate("QEleSysCtrlDlg", "\351\207\207\351\233\206\345\210\244\345\210\253\350\256\276\345\256\232", nullptr));
        PB_workmode->setText(QApplication::translate("QEleSysCtrlDlg", "\345\267\245\344\275\234\346\250\241\345\274\217\350\256\276\345\256\232", nullptr));
        PB_rankse->setText(QApplication::translate("QEleSysCtrlDlg", "\351\207\207\346\240\267\345\271\263\345\235\207\346\225\260\n"
"\350\256\276\345\256\232", nullptr));
        PB_pumpai_start->setText(QApplication::translate("QEleSysCtrlDlg", "\346\277\200\345\205\211\345\231\250\346\215\242\346\260\224\345\274\200\345\247\213", nullptr));
        PB_pumpai_stop->setText(QApplication::translate("QEleSysCtrlDlg", "\346\277\200\345\205\211\345\231\250\346\215\242\346\260\224\345\201\234\346\255\242", nullptr));
        PB_dev_start->setText(QApplication::translate("QEleSysCtrlDlg", "\345\220\257\345\212\250", nullptr));
        PB_dev_stop->setText(QApplication::translate("QEleSysCtrlDlg", "\345\201\234\346\255\242", nullptr));
        PB_save_para->setText(QApplication::translate("QEleSysCtrlDlg", "\345\255\230\345\202\250\346\225\260\346\215\256", nullptr));
        PB_dev_reset->setText(QApplication::translate("QEleSysCtrlDlg", "\345\244\215\344\275\215", nullptr));
        PB_request_para->setText(QApplication::translate("QEleSysCtrlDlg", "\345\217\202\346\225\260\350\257\273\345\217\226", nullptr));
        PB_request_status->setText(QApplication::translate("QEleSysCtrlDlg", "\347\212\266\346\200\201\350\257\273\345\217\226", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QEleSysCtrlDlg: public Ui_QEleSysCtrlDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QELESYSCTRLDLG_H
