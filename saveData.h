#ifndef SAVEDATA_H
#define SAVEDATA_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <QDebug>
#include <QMutex>
#include "QApplication"
#include "types.h"
#include "util.h"

using namespace std;

class saveData
{
public:
    saveData(int index);
    virtual ~saveData();

public:
    void setSavePath(LS_S8 *path);
    int saveRecvPressData(LS_U8 *buf,int len);
    int saveRecvPressValue(double *dValue,int valueCount);
    int saveConcentValue(int saveCount,float fTempValue,float *fConcentValue,int concentCount=3);

private:
    FILE *concentFp;
    char saveDirPath[MAX_PATH];
    QMutex  saveMtx;
    int   saveIndex;
};

#endif // SAVEDATA_H
