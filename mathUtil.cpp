#include "mathUtil.h"

const int N = 8192;
const float PI = 3.1416926;
const double C = 3e10;
const double Na = 6.022e23;
//static  float MeasureNoise_R=0.0396;
static  float MeasureNoise_R=2.0;
//static  float ProcessNiose_Q = 0.25;
static  float ProcessNiose_Q = 0.5;
static  float x_last;
static  float p_last;
#define M  35
static float value_buf[M] ;
static  int Count_Number=0;
float Newnumber = 0;
float Output=0;
static double  TaoMean[20]={0};
static double CH4Mean[20]={0};
static int FlagCH4=0;
static int MeanNumber=0;
static int FlagMean=0;
static int _have_paint = 0;

inline void swap (float &a, float &b)
{
    float t;
    t = a;
    a = b;
    b = t;
}

void bitrp (float xreal [], float ximag [], int n)
{
    // 位反转置换 Bit-reversal Permutation
    int i, j, a, b, p;

    for (i = 1, p = 0; i < n; i *= 2)
    {
        p ++;
    }
    for (i = 0; i < n; i ++)
    {
        a = i;
        b = 0;
        for (j = 0; j < p; j ++)
        {
            b = (b << 1) + (a & 1);    // b = b * 2 + a % 2;
            a >>= 1;        // a = a / 2;
        }
        if ( b > i)
        {
            swap (xreal [i], xreal [b]);
            swap (ximag [i], ximag [b]);
        }
    }
}


void FFT(float xreal [], float ximag [], int n)
{
    // 快速傅立叶变换，将复数 x 变换后仍保存在 x 中，xreal, ximag 分别是 x 的实部和虚部
    float wreal [N / 2], wimag [N /2], treal, timag, ureal, uimag, arg;
    int m, k, j, t, index1, index2;

    bitrp (xreal, ximag, n);

    // 计算 1 的前 n / 2 个 n 次方根的共轭复数 W'j = wreal [j] + i * wimag [j] , j = 0, 1, ... , n / 2 - 1
    arg = - 2 * PI / n;
    treal = cos (arg);
    timag = sin (arg);
    wreal [0] = 1.0;
    wimag [0] = 0.0;
    for (j = 1; j < n / 2; j ++)
    {
        wreal [j] = wreal [j - 1] * treal - wimag [j - 1] * timag;
        wimag [j] = wreal [j - 1] * timag + wimag [j - 1] * treal;
    }

    for (m = 2; m <= n; m *= 2)
    {
        for (k = 0; k < n; k += m)
        {
            for (j = 0; j < m / 2; j ++)
            {
                index1 = k + j;
                index2 = index1 + m / 2;
                t = n * j / m;    // 旋转因子 w 的实部在 wreal [] 中的下标为 t
                treal = wreal [t] * xreal [index2] - wimag [t] * ximag [index2];
                timag = wreal [t] * ximag [index2] + wimag [t] * xreal [index2];
                ureal = xreal [index1];
                uimag = ximag [index1];
                xreal [index1] = ureal + treal;
                ximag [index1] = uimag + timag;
                xreal [index2] = ureal - treal;
                ximag [index2] = uimag - timag;
            }
        }
    }
}


float KalmanFilter( float ResrcData)
{
    float x_mid;
    float x_now;
    float p_mid ;
    float p_now;
    float kg;

    x_mid=x_last; 		                    //x_last=x(k-1|k-1),x_mid=x(k|k-1)
    p_mid=p_last+ProcessNiose_Q; 		    //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=噪声
    kg=p_mid/(p_mid+MeasureNoise_R); 		//kg为kalman filter，R为噪声
    x_now=x_mid+kg*(ResrcData-x_mid);		//估计出的最优值

    p_now=(1-kg)*p_mid;		                //最优值对应的covariance

    p_last = p_now; 		                //更新covariance值
    x_last = x_now;
    return x_now;
}

void countCouncent(double *mValue,float *outputValue)
{
    double dYValue[DATA_COUNT]={0};
    double x[DATA_COUNT]={0};
    float y[DATA_COUNT]={0};
    float z[8192]={0};
    float yy[8192] = {0};
    float yyy[8192]={0};
    static double dValue = 123.0;
    for(int i=0;i<DATA_COUNT;i++){
        x[i]=(double)i;
        y[i]=(float)mValue[i];
        if(mValue[i]<0)return;
        dYValue[i]=mValue[i];
    }
    //dValue ++;
    float tempvalue=0;
    int tempnumber=0;
    for(int j=0;j<DATA_COUNT;j++)
    {
        if(y[j]>=tempvalue)
        {
            tempvalue = y[j];
            tempnumber = j ;
        }
    }

    int loopnumber = 0;
    for(int j=0;j<DATA_COUNT;j++)
    {
        int k=0;
        k=j+tempnumber;
        yy[j]=y[k];
        loopnumber=j;
    }

    float averagesum=0;
    float tailnumber=0;
    for (int i=7900;i<8000;i++)
    {
        averagesum = averagesum+y[i];
    }

    tailnumber = averagesum/100;

    for (int i=0;i<8192;i++)
    {
        if (i <= loopnumber)
        {
            yyy[i] = y[i];
        }
        else
        {
            yyy[i]=tailnumber;
        }

    }
    // 对数浓度结算

    double sum_a =0;
    double sum_b =0;
    double sum_c =0;

    for (int i= 100;i<500;i++)
    {
        sum_a=yyy[i]+sum_a;
    }

    for (int i = 900;i<1300;i++)
    {
        sum_b=yyy[i]+sum_b;
    }

    for (int i = 7500;i<7900;i++)
    {
        sum_c=yyy[i]+sum_c;
    }
    double interval = 800*10*1e-9;

    double beta_1 =1/interval*log((sum_a-sum_c)/(sum_b-sum_c));

    beta_1 = 1/beta_1*1e6;

    //115121

    // 浓度处理

    //	FFT (yy, z,512);
    FFT(yyy,z,4096);
    //free(yy);
    //free(yyy);
    //free(y);

    int  Nx=4096;
    double  T = 10e-9;
    //double  beta = 1/T* log(-yy[1]/z[1]*sin(2*PI/Nx)+cos(2*PI/Nx));
    double  beta = 1/T* log(-yyy[1]/z[1]*sin(2*PI/Nx)+cos(2*PI/Nx));
    //double  beta = 1/T* log(-Y[1].real/Y[1].imag*sin(2*PI/Nx)+cos(2*PI/Nx));
    //double  beta = 1/T* log(-xreal[1]/ximag[1]*sin(2*PI/512)+cos(2*PI/512));



    double  TaoF=1.0/beta;
    double  Tao= TaoF*1e6;


    double TaoTemp=0;

    TaoTemp = TaoMean[0];




    if(FlagMean >10)
    {
        if((abs(TaoMean[0]-Tao)<(TaoMean[0]*0.2))&&(Tao>0))
        {
            for(int n=19;n>=1;n--)
            {
                TaoMean[n] = TaoMean[n-1];
            }

            TaoMean[0]=Tao;
        }
    }
    else
    {
        TaoMean[0]=Tao;
    }

    TaoMean[0]=Tao;


    double TempMean_1=0;
    FlagMean=0;
    for(int n=0;n<20;n++)
    {
        if(TaoMean[n] > 0)
        {
            TempMean_1  += TaoMean[n];
            FlagMean++;
        }
    }

    //不对Tao进行率平均滤波处理

    //Tao=TempMean_1/FlagMean;

    TaoMean[0] = Tao;

    //对Tao进行卡曼滤波处理；

    Newnumber = Tao ;
    if(Count_Number > (M-1))
    {
        for (int i=M-1;i>0;i--)
        {
            value_buf[i]=value_buf[i-1];
        }
        value_buf[0]  = Newnumber;

        float value_temp[M] ={0};

        for (int i=0;i<M;i++)
        {
            value_temp[i] = value_buf[i];
        }


        float median_filtering;
        float temp;
        for (int j=0;j<M-1;j++)
        {
            for (int i=0;i<M-1-j;i++)
            {
                if ( value_temp[i]>value_temp[i+1] )
                {
                    temp = value_temp[i];
                    value_temp[i] = value_temp[i+1];
                    value_temp[i+1] = temp;
                }
            }
        }
        Output=value_temp[(M-1)/2];

    }
    else
    {
        value_buf[Count_Number] = Newnumber;
        Output = Newnumber;
        if (Count_Number == 0 )
        {
            x_last = Newnumber;
            p_last = 0;
        }

    }
    Count_Number++;

    //Tao=Output;

    //
    float Kalman_output=0;
    Kalman_output=KalmanFilter(Tao);

    //定义甲烷吸收基线
    //double  Time_Base_Ch4=0.7635;
    double  Time_Base_Ch4=11.07;
    //double  Time_Base_Ch4=1.298;
    //定义甲烷吸收峰出
    double  Time_Ch4;
    //Time_Ch4=Tao-4;
    //Time_Ch4 = Output;
    Time_Ch4=Kalman_output;

    //Time_Ch4=1.18259;
    //定义吸收系数

    double absorp_coefficient;

    Time_Ch4=Time_Ch4;
    Time_Base_Ch4 = Time_Base_Ch4;

    absorp_coefficient=1/C*(1/Time_Ch4-1/Time_Base_Ch4)*1e6;

    //定义吸收截面
    //double absorp_cross_section = 1.49e-20;
    double absorp_cross_section = 1.8e-20;
    //定义甲烷浓度
    double N_CH4  ,V_CH4 ;
    N_CH4 = absorp_coefficient/absorp_cross_section/Na*1e3*1e9;
    int Vr = 22.4;
    V_CH4 = N_CH4/1e9*Vr*1e6;

    //定标曲线输入值

    double slope = 0.36;
    double offset= 1.2;

    //定标曲线
    //dValue=0.5+1.8*V_CH4+V_CH4*V_CH4;
    //dValue=-0.329+1.536*V_CH4+0.122*V_CH4*V_CH4;
    dValue =  V_CH4;
    //调试中 ，目前先显示Tao
    //dValue = V_CH4;
    //dValue = Tao;
    //Tao = Tao  + 0.02;
    //Output  = Output + 0.02;

    float zero11=rand()%10/100;
    if (dValue <0)
    {
        dValue = -dValue;
    }

#if 0
    str1.Format("衰荡时间：%.4f",Time_Ch4);
    str2.Format("浓度值：%.4f",dValue);
    //str2.Format("卡曼滤波：%.4f",Kalman_output);
    str3.Format("中值滤波:%.4f",Output);
#endif
    outputValue[0]=Time_Ch4;
    //fConValue[1]=Kalman_output;
    //fConValue[1] = dValue;
    outputValue[1] = beta_1;
    outputValue[2]=Output;
}
