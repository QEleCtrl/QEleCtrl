#include "QSysSetDlg.h"
#include "ui_QSysSetDlg.h"

QSysSetDlg::QSysSetDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QSysSetDlg)
{
    ui->setupUi(this);

    QString str = "";
    this->setWindowTitle("软件设置");
    ui->CB_ele_dev1_com->setView(new QListView());
    ui->CB_ele_dev0_com->setView(new QListView());
    ui->CB_ele_temp1_com->setView(new QListView());
    ui->CB_ele_temp0_com->setView(new QListView());
    for(int i=0;i<20;i++){
        str = QString("COM%1").arg(i+1);
        ui->CB_GPS_COM->insertItem(i,str);
        ui->CB_ele_dev0_com->insertItem(i,str);
        ui->CB_ele_dev1_com->insertItem(i,str);
        ui->CB_ele_temp0_com->insertItem(i,str);
        ui->CB_ele_temp1_com->insertItem(i,str);
    }
    QPalette palette(this->palette());
    palette.setColor(QPalette::Background,QColor(DLG_BK_COLOR));
    this->setPalette(palette);
}

QSysSetDlg::~QSysSetDlg()
{
    delete ui;
}
void QSysSetDlg::setSysSetSavePath(LS_S8 *path)
{
    QString str = "";
    str=QString("%1").arg(path);
    ui->LE_save_path->setText(str);
}

void QSysSetDlg::setSysSetDlg(TSysSet arg)
{
    //ls_info_debug("%d %d %d %d %d",arg.eleDevPort[0],arg.eleDevPort[1],arg.eleTempPort[0],arg.eleTempPort[1],arg.gpsPort);
    int index = arg.eleDevPort[Ele_DevID_0];
    ui->CB_ele_dev0_com->setCurrentIndex(index-1);

    index = arg.eleDevPort[Ele_DevID_1];
    ui->CB_ele_dev1_com->setCurrentIndex(index-1);

    index = arg.eleTempPort[Ele_DevID_0];
    ui->CB_ele_temp0_com->setCurrentIndex(index-1);

    index = arg.eleTempPort[Ele_DevID_1];
    ui->CB_ele_temp1_com->setCurrentIndex(index-1);

    if(arg.gpsPort>0 && arg.gpsPort<20){
        ui->CB_GPS_COM->setCurrentIndex(arg.gpsPort-1);
    }
}

void QSysSetDlg::on_PB_save_path_clicked()
{
    QString str = QFileDialog::getExistingDirectory();
    if(str.length()>=0);
    ui->LE_save_path->setText(str);
}

void QSysSetDlg::on_PB_SYSSET_clicked()
{
    TSysSet arg;
    int value = 0;
    QString str="";

    value = ui->CB_ele_dev0_com->currentIndex();
    if(value<0 || value>50){
        emit this->signalSysSetMsg("端口应 0~50",MSG_ERR);
        ls_err_debug("sys set gps port ERR");
        return;
    }
    arg.eleDevPort[Ele_DevID_0]=value+1;

    value = ui->CB_ele_dev1_com->currentIndex();
    if(value<0 || value>50){
        emit this->signalSysSetMsg("端口应 0~50",MSG_ERR);
        ls_err_debug("sys set gps port ERR");
        return;
    }
    arg.eleDevPort[Ele_DevID_1]=value+1;

    value = ui->CB_ele_temp0_com->currentIndex();
    if(value<0 || value>50){
        emit this->signalSysSetMsg("端口应 0~50",MSG_ERR);
        ls_err_debug("sys set gps port ERR");
        return;
    }
    arg.eleTempPort[Ele_DevID_0]=value+1;

    value = ui->CB_ele_temp1_com->currentIndex();
    if(value<0 || value>50){
        emit this->signalSysSetMsg("端口应 0~50",MSG_ERR);
        ls_err_debug("sys set gps port ERR");
        return;
    }
    arg.eleTempPort[Ele_DevID_1]=value+1;

    value = ui->CB_GPS_COM->currentIndex();
    if(value<0 || value>50){
        emit this->signalSysSetMsg("端口应 0~50",MSG_ERR);
        ls_err_debug("sys set gps port ERR");
        return;
    }
    arg.gpsPort = (value+1);

    bool bRepeat=false;
    for(int i=0;i<ELE_MAX_NUM;i++){
        for(int j=0;j<ELE_MAX_NUM;j++){
            if(arg.eleDevPort[i]==arg.eleTempPort[j]){bRepeat=true;break;}
        }
        if(arg.eleDevPort[i]==arg.gpsPort){bRepeat=true;break;}
        if(arg.eleTempPort[i]==arg.gpsPort){bRepeat=true;break;}
    }
    if(arg.eleDevPort[0]==arg.eleDevPort[1])bRepeat=true;
    if(arg.eleTempPort[0]==arg.eleTempPort[1])bRepeat=true;
    if(bRepeat){
        emit this->signalSysSetMsg("端口号不能相同",MSG_ERR);
        ls_err_debug("端口号不能相同");
        return;
    }
    str = ui->LE_save_path->text();

    if(str.length()<=0){
        emit this->signalSysSetMsg("存储路径为空",MSG_ERR);
        ls_err_debug("存储路径为空");
        return;
    }
   QDir dir(str);
   if(!dir.exists()){
       emit this->signalSysSetMsg("存储文件夹不存在",MSG_ERR);
       ls_err_debug("存储文件夹不存在");
   }
    emit this->signalSysSetRet(&arg,str);
}

