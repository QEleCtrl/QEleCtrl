#ifndef QELESYSCTRLDLG_H
#define QELESYSCTRLDLG_H

#include <QDialog>
#include <QListView>
#include "types.h"
#include "config.h"
#include "util.h"
#include "sendoutBuffer.h"
#include "eleProto.h"

namespace Ui {
class QEleSysCtrlDlg;
}

class QEleSysCtrlDlg : public QDialog
{
    Q_OBJECT

public:
    explicit QEleSysCtrlDlg(QWidget *parent = 0);
    ~QEleSysCtrlDlg();

private:
    Ui::QEleSysCtrlDlg *ui;

private:
    Ele_Ctrl_Dev    eEleSysDev;
    TEleSysArg      tGlobalEleArg;
    TSysSet         tSysPara;

public:
    void setCtrlDev(Ele_Ctrl_Dev mDev);
    void setGlobalSysPara(TSysSet *para);
private:
    void initEleSysDlg();
    int sendCtrlCmd(LS_U8 *buf,int len);
    void setEleSysDlgShow(TEleSysArg *arg);
signals:
    void signalEleSysCtrlMsg(QString str,MsgType type=MSG_ERR);
private slots:
    void on_PB_ld_para_clicked();
    void on_PB_las_tem_clicked();
    void on_PB_throld_clicked();
    void on_PB_preshh_clicked();
    void on_PB_presll_clicked();
    void on_PB_coll_count_clicked();
    void on_PB_volta_a_clicked();
    void on_PB_volta_b_clicked();
    void on_PB_volta_c_clicked();
    void on_PB_judge_clicked();
    void on_PB_workmode_clicked();
    void on_PB_rankse_clicked();
    void on_PB_pumpai_start_clicked();
    void on_PB_pumpai_stop_clicked();
    void on_PB_dev_start_clicked();
    void on_PB_dev_stop_clicked();
    void on_PB_save_para_clicked();
    void on_PB_dev_reset_clicked();
    void on_PB_request_para_clicked();
    void on_PB_request_status_clicked();
};

#endif // QELESYSCTRLDLG_H
