/********************************************************************************
** Form generated from reading UI file 'QArgStatusDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QARGSTATUSDLG_H
#define UI_QARGSTATUSDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>

QT_BEGIN_NAMESPACE

class Ui_QArgStatusDlg
{
public:

    void setupUi(QDialog *QArgStatusDlg)
    {
        if (QArgStatusDlg->objectName().isEmpty())
            QArgStatusDlg->setObjectName(QString::fromUtf8("QArgStatusDlg"));
        QArgStatusDlg->resize(556, 380);

        retranslateUi(QArgStatusDlg);

        QMetaObject::connectSlotsByName(QArgStatusDlg);
    } // setupUi

    void retranslateUi(QDialog *QArgStatusDlg)
    {
        QArgStatusDlg->setWindowTitle(QApplication::translate("QArgStatusDlg", "\345\217\202\346\225\260\345\222\214\347\212\266\346\200\201\346\230\276\347\244\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QArgStatusDlg: public Ui_QArgStatusDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QARGSTATUSDLG_H
