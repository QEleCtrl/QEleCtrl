#include "QEleCtrl.h"
#include "QTextCodec"
#include <QApplication>
#include <QMetaType>

int main(int argc, char *argv[])
{
    qRegisterMetaType<QTextCharFormat>("QTextCharFormat");
    qRegisterMetaType<QTextCursor>("QTextCursor");
    QApplication a(argc, argv);

    QString path = QDir::temp().absoluteFilePath("SingleAppTest.lock");
    QLockFile lockFile(path);

    bool isLock = lockFile.isLocked();
    if (!lockFile.tryLock(100))
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("应用软件已在运行");
        msgBox.exec();
        return 1;
    }
    //QTextCodec::setCodecForLocale(QTextCodec::codecForName("GBK"));
    QEleCtrl w;
    w.show();

    return a.exec();
}
