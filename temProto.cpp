#include "temProto.h"

int getTempActualValue(LS_S8 *buf)
{
    int ret = sprintf(buf,"%s","TC1:TCACTUALTEMP?");
    buf[ret++]=0x0D;
    return ret;
}

int getTempOutVolValue(LS_S8 *buf)
{
    int ret = sprintf(buf,"%s","TC1:TCACTUALVOLTAGE?");
    buf[ret++]=0x0D;
    return ret;
}

int getTempOutCurValue(LS_S8 *buf)
{
    int ret = sprintf(buf,"%s","TC1:TCACTUALCURRENT?");
    buf[ret++]=0x0D;
    return ret;
}

int getTemProRet(LS_S8 *buf)
{
    int ret = sprintf(buf,"%s","TC1:TCOTEF?");
    buf[ret++]=0x0D;
    return ret;
}

int getTempOutputModel(LS_S8 *buf)
{
    int ret = sprintf(buf,"%s","TC1:TCOE?");
    buf[ret++]=0x0D;
    return ret;
}

int setTempAdjustValue(LS_S8 *buf,float value)
{
    int ret = sprintf(buf,"TC1:TCADJUSTTEMP=%.2f",value);
    buf[ret++]=0x0D;
    return ret;
}

int setTempMaxValue(LS_S8 *buf,float value)
{
    int ret = sprintf(buf,"TC1:TCOTPHT=%.2f",value);
    buf[ret++]=0x0D;
    return ret;
}

int setTempMinValue(LS_S8 *buf,float value)
{
    int ret = sprintf(buf,"TC1:TCOTPLT=%.2f",value);
    buf[ret++]=0x0D;
    return ret;
}

int setTempOtpLT(LS_S8 *buf,float value)
{
    int ret = sprintf(buf,"TC1:TCOTPLT=%.2f",value);
    buf[ret++]=0x0D;
    return ret;
}

int setTempOtpHT(LS_S8 *buf,float value)
{
    int ret = sprintf(buf,"TC1:TCOTPHT=%.2f",value);
    buf[ret++]=0x0D;
    return ret;
}

int setTemKeyOnff(LS_S8 *buf,int onoff)
{
    int ret = sprintf(buf,"TC1:TCSW=%d",onoff);
    buf[ret++]=0x0D;
    return ret;
}

int setTempProVolValue(LS_S8 *buf,float fValue)
{
    int ret = sprintf(buf,"TC1:TCADJUSTOVPV=%.2f",fValue);
    buf[ret++]=0x0D;
    return ret;
}

int getTempProVolValue(LS_S8 *buf)
{
    int ret = sprintf(buf,"TC1:TCOVPV?");
    buf[ret++]=0x0D;
    return ret;
}

int setTempProCurValue(LS_S8 *buf,float fValue)
{
    int ret = sprintf(buf,"TC1:TCADJUSTOCPC=%.2f",fValue);
    buf[ret++]=0x0D;
    return ret;
}

int getTempProCurValue(LS_S8 *buf)
{
    int ret = sprintf(buf,"TC1:TCOCPC?");
    buf[ret++]=0x0D;
    return ret;
}
