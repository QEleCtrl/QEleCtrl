#-------------------------------------------------
#
# Project created by QtCreator 2019-05-03T10:51:40
#
#-------------------------------------------------

QT       += core gui charts serialport
DEFINES -= UNICODE
win32 {
    #RC_FILE += $$PWD/res/img/fill.rc
    LIBS += -lwsock32
    RC_FILE=logo.rc
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QEleCtrl
TEMPLATE = app

MOC_DIR = $$PWD/tmp/moc
RCC_DIR = $$PWD/tmp/rcc
OBJECTS_DIR = $$PWD/tmp/obj

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        QEleCtrl.cpp \
    config.cpp \
    etc.cpp \
    packSocket.cpp \
    QSysSetDlg.cpp \
    QEleSysCtrlDlg.cpp \
    eleProto.cpp \
    QEleSwitchDlg.cpp \
    QEleChartDlg.cpp \
    QEleInfoDlg.cpp \
    util.cpp \
    mathUtil.cpp \
    saveData.cpp \
    QcwIndicatorLamp.cpp \
    analyGPS.cpp \
    QTempCtrlDlg.cpp \
    temProto.cpp

HEADERS  += QEleCtrl.h \
    config.h \
    etc.h \
    packSocket.h \
    types.h \
    QSysSetDlg.h \
    QEleSysCtrlDlg.h \
    eleProto.h \
    QEleSwitchDlg.h \
    QEleChartDlg.h \
    QEleInfoDlg.h \
    util.h \
    dataBuffer.h \
    mathUtil.h \
    saveData.h \
    QcwIndicatorLamp.h \
    analyGPS.h \
    QTempCtrlDlg.h \
    temProto.h \
    sendoutBuffer.h

FORMS    += QEleCtrl.ui \
    QSysSetDlg.ui \
    QEleSysCtrlDlg.ui \
    QEleSwitchDlg.ui \
    QEleChartDlg.ui \
    QEleInfoDlg.ui \
    QTempCtrlDlg.ui \
    QTempCtrlDlg.ui

RESOURCES += \
    qelectrl.qrc
