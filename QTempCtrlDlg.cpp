#include "QTempCtrlDlg.h"
#include "ui_QTempCtrlDlg.h"

extern SendoutPtrBuffer        tempOutQue;

QTempCtrlDlg::QTempCtrlDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QTempCtrlDlg)
{
    ui->setupUi(this);
    QPalette palette(this->palette());
    palette.setColor(QPalette::Background,QColor(DLG_BK_COLOR));
    this->setPalette(palette);
    tTempDev = Ele_DevID_0;
    setTempDev(tTempDev);
    initTempDlg();
}

QTempCtrlDlg::~QTempCtrlDlg()
{
    delete ui;
}

void QTempCtrlDlg::initTempDlg()
{
    LS_S32 ret = 0;
    QString str = "";
    ret = operaTemArg(tTempDev,GET_OPERA,&tGlobalTempArg);
    if(ret !=LS_SUCC){
        ls_err_debug("operaEleSwitch ERR\n");
        emit this->signalTempMsg("读取温控初始化参数失败，显示默认配置");
        tGlobalTempArg.iTempValue[Temp_output]=1;
        tGlobalTempArg.iTempValue[Temp_set_value]=3200;
        tGlobalTempArg.iTempValue[Temp_pro_current]=351;
        tGlobalTempArg.iTempValue[Temp_pro_voltage]=1000;
        tGlobalTempArg.iTempValue[Temp_temp_hh]=2200;
        tGlobalTempArg.iTempValue[Temp_temp_ll]=1100;
    }
    if(tGlobalTempArg.iTempValue[Temp_output]){
        ui->RB_temp_output_on->setChecked(true);
        ui->RB_temp_output_off->setChecked(false);
    }else{
        ui->RB_temp_output_on->setChecked(false);
        ui->RB_temp_output_off->setChecked(true);
    }
    char mBuf[64]={0};
    memset(mBuf,0,sizeof(mBuf));
    sprintf(mBuf,"%.2f",tGlobalTempArg.iTempValue[Temp_set_value]*1.0/100.0);
    str = QString("%1").arg(mBuf);
    ui->LA_temp_set->setText(str);

    memset(mBuf,0,sizeof(mBuf));
    sprintf(mBuf,"%.2f",tGlobalTempArg.iTempValue[Temp_pro_current]*1.0/100.0);
    str = QString("%1").arg(mBuf);
    ui->LE_protect_current->setText(str);

    memset(mBuf,0,sizeof(mBuf));
    sprintf(mBuf,"%.2f",tGlobalTempArg.iTempValue[Temp_pro_voltage]*1.0/100.0);
    str = QString("%1").arg(mBuf);
    ui->LE_protect_voltage->setText(str);

    memset(mBuf,0,sizeof(mBuf));
    sprintf(mBuf,"%.2f",tGlobalTempArg.iTempValue[Temp_temp_hh]*1.0/100.0);
    str = QString("%1").arg(mBuf);
    ui->LE_protect_temp_hh->setText(str);

    memset(mBuf,0,sizeof(mBuf));
    sprintf(mBuf,"%.2f",tGlobalTempArg.iTempValue[Temp_temp_ll]*1.0/100.0);
    str = QString("%1").arg(mBuf);
    ui->LE_protect_temp_ll->setText(str);

    str = "输出电压:0.000V";
    ui->LA_output_voltage_0->setText(str);
    ui->LA_output_voltage_1->setText(str);

    str = "输出电流:0.000A";
    ui->LA_output_current_0->setText(str);
    ui->LA_output_current_1->setText(str);

    str = "过流阈值:0.000A";
    ui->LA_check_protect_current0->setText(str);
    ui->LA_check_protect_current1->setText(str);

    str = "过压阈值:0.000V";
    ui->LA_check_protect_voltage0->setText(str);
    ui->LA_check_protect_voltage1->setText(str);
}

void QTempCtrlDlg::QTempShowOutVoltage(int index,double fValue)
{
    char fValueBuf[64]={0};
    memset(fValueBuf,0,sizeof(fValueBuf));
    sprintf(fValueBuf,"%.3f",fValue);
    QString str = "";
    str = QString("输出电压:%1V").arg(fValueBuf);
    if(index==Ele_DevID_0){
        ui->LA_output_voltage_0->setText(str);
    }else{
        ui->LA_output_voltage_1->setText(str);
    }
}

void QTempCtrlDlg::QTempShowOutCurrent(int index,double fValue)
{
    char fValueBuf[64]={0};
    memset(fValueBuf,0,sizeof(fValueBuf));
    sprintf(fValueBuf,"%.3f",fValue);
    QString str = "";
    str = QString("输出电流:%1A").arg(fValueBuf);
    if(index==Ele_DevID_0){
        ui->LA_output_current_0->setText(str);
    }else{
        ui->LA_output_current_1->setText(str);
    }
}

void QTempCtrlDlg::QTempShowProCurrent(int index,double fValue)
{
    char fValueBuf[64]={0};
    memset(fValueBuf,0,sizeof(fValueBuf));
    sprintf(fValueBuf,"%.3f",fValue);
    QString str = "";
    str = QString("过流阈值:%1A").arg(fValueBuf);
    if(index==Ele_DevID_0){
        ui->LA_check_protect_current0->setText(str);
    }else{
        ui->LA_check_protect_current1->setText(str);
    }
}

void QTempCtrlDlg::QTempShowProVoltage(int index,double fValue)
{
    char fValueBuf[64]={0};
    memset(fValueBuf,0,sizeof(fValueBuf));
    sprintf(fValueBuf,"%.3f",fValue);
    QString str = "";
    str = QString("过压阈值:%1V").arg(fValueBuf);
    if(index==Ele_DevID_0){
        ui->LA_check_protect_voltage0->setText(str);
    }else{
        ui->LA_check_protect_voltage1->setText(str);
    }
}

void QTempCtrlDlg::setTempDev(Ele_Ctrl_Dev mDev)
{
    this->tTempDev = mDev;
}

int QTempCtrlDlg::sendCtrlCmd(void *buf,int len)
{
    LS_S32 ret = 0;
    LS_S8_OUT_PACKET *mData=new LS_S8_OUT_PACKET;
    if(!mData){
        ls_info_debug("new LS_U8_OUT_PACKET ERR");
        return LS_FAIL;
    }

    mData->data = (LS_S8 *)buf;
    mData->size = len;
    mData->devid = (int)tTempDev;
    tempOutQue.PushPacket(mData);
    delete mData;
    mData = nullptr;
    return LS_SUCC;
}

int QTempCtrlDlg::sendCtrlCmd_index(int index,void *buf,int len)
{
    LS_S32 ret = 0;
    LS_S8_OUT_PACKET *mData=new LS_S8_OUT_PACKET;
    if(!mData){
        ls_info_debug("new LS_U8_OUT_PACKET ERR");
        return LS_FAIL;
    }

    mData->data = (LS_S8 *)buf;
    mData->size = len;
    mData->devid = index;
    tempOutQue.PushPacket(mData);
    delete mData;
    mData = nullptr;
    return LS_SUCC;
}

void QTempCtrlDlg::on_PB_check_protect_current0_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    sendLen = getTempProCurValue(sendBuf);
    sendCtrlCmd_index(Ele_DevID_0,sendBuf,sendLen);
}

void QTempCtrlDlg::on_PB_check_protect_voltage0_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    sendLen = getTempProVolValue(sendBuf);
    sendCtrlCmd_index(Ele_DevID_0,sendBuf,sendLen);
}

void QTempCtrlDlg::on_PB_check_protect_current1_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    sendLen = getTempProCurValue(sendBuf);
    sendCtrlCmd_index(Ele_DevID_1,sendBuf,sendLen);
}

void QTempCtrlDlg::on_PB_check_protect_voltage1_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    sendLen = getTempProVolValue(sendBuf);
    sendCtrlCmd_index(Ele_DevID_1,sendBuf,sendLen);
}

void QTempCtrlDlg::on_PB_temp_set_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    QString str = "";
    str = ui->LA_temp_set->text();
    double fValue = str.toDouble();
    sendLen = setTempAdjustValue(sendBuf,fValue);
    sendCtrlCmd(sendBuf,sendLen);
    tGlobalTempArg.iTempValue[Temp_set_value]=(fValue*100);
    operaTemArg(tTempDev,SET_OPERA,&tGlobalTempArg);
}

void QTempCtrlDlg::on_PB_protect_current_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    QString str = "";
    str = ui->LA_temp_set->text();
    double fValue = str.toDouble();
    sendLen = setTempProCurValue(sendBuf,fValue);
    sendCtrlCmd(sendBuf,sendLen);
    tGlobalTempArg.iTempValue[Temp_pro_current]=(fValue*100);
    operaTemArg(tTempDev,SET_OPERA,&tGlobalTempArg);
}

void QTempCtrlDlg::on_PB_portect_voltage_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    QString str = "";
    str = ui->LA_temp_set->text();
    double fValue = str.toDouble();
    sendLen = setTempProVolValue(sendBuf,fValue);
    sendCtrlCmd(sendBuf,sendLen);
    tGlobalTempArg.iTempValue[Temp_pro_voltage]=(fValue*100);
    operaTemArg(tTempDev,SET_OPERA,&tGlobalTempArg);
}

void QTempCtrlDlg::on_PB_protect_temp_hh_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    QString str = "";
    str = ui->LA_temp_set->text();
    double fValue = str.toDouble();
    sendLen = setTempOtpHT(sendBuf,fValue);
    sendCtrlCmd(sendBuf,sendLen);
    tGlobalTempArg.iTempValue[Temp_temp_hh]=(fValue*100);
    operaTemArg(tTempDev,SET_OPERA,&tGlobalTempArg);
}

void QTempCtrlDlg::on_PB_protect_temp_ll_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    QString str = "";
    str = ui->LA_temp_set->text();
    double fValue = str.toDouble();
    sendLen = setTempOtpLT(sendBuf,fValue);
    sendCtrlCmd(sendBuf,sendLen);
    tGlobalTempArg.iTempValue[Temp_temp_ll]=(fValue*100);
    operaTemArg(tTempDev,SET_OPERA,&tGlobalTempArg);
}

void QTempCtrlDlg::on_RB_temp_output_on_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    sendLen = setTemKeyOnff(sendBuf,true);
    tGlobalTempArg.iTempValue[Temp_output]=1;
    operaTemArg(tTempDev,SET_OPERA,&tGlobalTempArg);
    sendCtrlCmd(sendBuf,sendLen);
}

void QTempCtrlDlg::on_RB_temp_output_off_clicked()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    sendLen = setTemKeyOnff(sendBuf,false);
    tGlobalTempArg.iTempValue[Temp_output]=0;
    operaTemArg(tTempDev,SET_OPERA,&tGlobalTempArg);
    sendCtrlCmd(sendBuf,sendLen);
}
