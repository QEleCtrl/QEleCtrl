#include "config.h"
#pragma execution_character_set(“utf-8”)

#define INI_FILE			("cfg.ini")

static int _have_init_xml_path = 0;
static char cfgPath[MAX_PATH]={0};

char iniConfBuf[]={"[EleCtrlDlg]\n"\
                   "x=0\n"\
                   "y=0\n"\
                   "w=960\n"\
                   "h=540\n"\
                   "\n"\
                   "[EleSwitchDlg]\n"
                   "x=0\n"
                   "y=0\n"
                   "w=960\n"
                   "h=540\n"
                   "\n"\
                   "[EleSysCtrlDlg]\n"
                   "x=0\n"
                   "y=0\n"
                   "w=960\n"
                   "h=540\n"
                   "\n"\
                   "[sysSetDlg]\n"
                   "x=0\n"
                   "y=0\n"
                   "w=960\n"
                   "h=540\n"
                   "\n"\
                   "[EleChartDlg]\n"
                   "x=0\n"
                   "y=0\n"
                   "w=960\n"
                   "h=540\n"
                   "\n"\
                   "[EleInfoDlg]\n"
                   "x=0\n"
                   "y=0\n"
                   "w=960\n"
                   "h=540\n"
                   "\n"\
                   "[system]\n" \
                    "soft=4\n" \
                    "dev0-FDB=10\n" \
                    "dev1-FDB=10\n" \
                    "dev0-save=0\n" \
                    "dev1-save=0\n" \
                    "dev0-check=0\n" \
                    "dev1-check=0\n" \
                    "dev0-port=1\n" \
                    "dev1-port=2\n" \
                    "temp0-port=3\n" \
                    "temp1-port=4\n" \
                    "gpsPort=5\n" \
                    "saveDir=D:\\save\n"\
                    "\n"\
                    "[ElePara-dev0]\n" \
                    "ld_para=128\n" \
                    "las_tem=0\n" \
                    "throld=12000\n" \
                    "preshh=0\n" \
                    "presll=0\n" \
                    "coll_count=10\n"\
                    "volta_a=17112\n" \
                    "volta_b=13631\n" \
                    "volta_c=16112\n" \
                    "judge=0\n" \
                    "workmode=0\n" \
                    "rankse_vol=16\n" \
                    "rankse_value=1\n" \
                    "\n"\
                    "[ElePara-dev1]\n" \
                    "ld_para=128\n" \
                    "las_tem=0\n" \
                    "throld=12000\n" \
                    "preshh=0\n" \
                    "presll=0\n" \
                    "coll_count=10\n"\
                    "volta_a=17112\n" \
                    "volta_b=13631\n" \
                    "volta_c=16112\n" \
                    "judge=0\n" \
                    "workmode=0\n" \
                    "rankse_vol=16\n" \
                    "rankse_value=1\n" \
                    "\n"\
                    "[ElePara-all]\n" \
                    "ld_para=128\n" \
                    "las_tem=0\n" \
                    "throld=12000\n" \
                    "preshh=0\n" \
                    "presll=0\n" \
                    "coll_count=10\n"\
                    "volta_a=17112\n" \
                    "volta_b=13631\n" \
                    "volta_c=16112\n" \
                    "judge=0\n" \
                    "workmode=0\n" \
                    "rankse_vol=16\n" \
                    "rankse_value=1\n" \
                    "\n"\
                    "[EleSwitch-dev0]\n" \
                    "boost_pump=1\n" \
                    "vacuum_pump=1\n" \
                    "solenoid1=1\n" \
                    "solenoid2=1\n" \
                    "solenoid3=1\n" \
                    "solenoid4=1\n" \
                    "gate1=1\n" \
                    "gate2=1\n" \
                    "gate3=1\n" \
                    "loop_pump=1\n" \
                    "data_collect=1\n" \
                    "temp_ctrl=1\n" \
                    "dfb_laser=1\n" \
                    "\n"\
                    "[EleSwitch-dev1]\n" \
                    "boost_pump=1\n" \
                    "vacuum_pump=1\n" \
                    "solenoid1=1\n" \
                    "solenoid2=1\n" \
                    "solenoid3=1\n" \
                    "solenoid4=1\n" \
                    "gate1=1\n" \
                    "gate2=1\n" \
                    "gate3=1\n" \
                    "loop_pump=1\n" \
                    "data_collect=1\n" \
                    "temp_ctrl=1\n" \
                    "dfb_laser=1\n" \
                    "\n"\
                    "[EleSwitch-all]\n" \
                    "boost_pump=1\n" \
                    "vacuum_pump=1\n" \
                    "solenoid1=1\n" \
                    "solenoid2=1\n" \
                    "solenoid3=1\n" \
                    "solenoid4=1\n" \
                    "gate1=1\n" \
                    "gate2=1\n" \
                    "gate3=1\n" \
                    "loop_pump=1\n" \
                    "data_collect=1\n" \
                    "temp_ctrl=1\n" \
                    "dfb_laser=1\n" \
                   "\n"\
                   "[temp-dev0]\n" \
                   "output=1\n" \
                   "temp_set=3200\n" \
                   "pro_current=351\n" \
                   "pro_voltage=1000\n" \
                   "pro_temp_hh=2200\n" \
                   "pro_temp_ll=1100\n" \
                   "[temp-dev1]\n" \
                   "output=1\n" \
                   "temp_set=3200\n" \
                   "pro_current=351\n" \
                   "pro_voltage=1000\n" \
                   "pro_temp_hh=2200\n" \
                   "pro_temp_ll=1100\n" \
                   "[temp-all]\n" \
                   "output=1\n" \
                   "temp_set=3200\n" \
                   "pro_current=351\n" \
                   "pro_voltage=1000\n" \
                   "pro_temp_hh=2200\n" \
                   "pro_temp_ll=1100\n" \
                  };
int createConfigFile(char *filePath)
{
    FILE *fp = fopen(filePath,"wb");
    if(!fp){
        ls_err_debug("fopen ERR\n");
        return LS_FAIL;
    }
    ls_info_debug("%s",filePath);
    fwrite(iniConfBuf,1,strlen(iniConfBuf),fp);
    fflush(fp);
    fclose(fp);
    fp = NULL;
    return LS_SUCC;
}

void getConfigAbsolutePath()
{
    char exeDir[MAX_PATH]={0};
    QString str = "";
    if(_have_init_xml_path>0){
        return;
    }
    str = QApplication::applicationDirPath();
    str+="/config";
    memset(exeDir,0,sizeof(exeDir));
    QString2Char(str,exeDir);
    createNewDir(exeDir);
    sprintf(cfgPath,"%s/%s",exeDir,INI_FILE);
    if(access(cfgPath,F_OK)!=0){
        createConfigFile(cfgPath);
    }
    _have_init_xml_path=1;
}

int operaSoftTime(enum OperaINI mOpera,LS_U32 *mValue)
{
    int value = 0;
    char mBuf[20]={0};
    getConfigAbsolutePath();
	if(mOpera==SET_OPERA){
		value = *mValue;
        memset(mBuf,0,sizeof(mBuf));
        sprintf(mBuf,"%d",value);
        if(SetValueToEtcFile(cfgPath, "system", "soft", mBuf)!=ETC_OK){
            return LS_FAIL;
        }
        return LS_SUCC;
	}
    *mValue = 0;
    if(GetIntValueFromEtcFile(cfgPath, "system", "soft", (int *)mValue)!= ETC_OK){
        return LS_FAIL;
    }
    return LS_SUCC;
}

int operaFDBTime(Ele_Ctrl_Dev devid,enum OperaINI mOpera,LS_U32 *mValue)
{
    int value = 0;
    char mBuf[20]={0};
    char pKey[20]={0};
    getConfigAbsolutePath();
    memset(pKey,0,sizeof(pKey));
    if(devid == Ele_DevID_0){
        strcpy(pKey,"dev0-FDB");
    }else{
        strcpy(pKey,"dev1-FDB");
    }
    if(mOpera==SET_OPERA){
        value = *mValue;
        memset(mBuf,0,sizeof(mBuf));
        sprintf(mBuf,"%d",value);
        if(SetValueToEtcFile(cfgPath, "system", pKey, mBuf)!=ETC_OK){
            return LS_FAIL;
        }
        return LS_SUCC;
    }
    *mValue = 0;
    if(GetIntValueFromEtcFile(cfgPath, "system", pKey, (int*)mValue)!= ETC_OK){
        return LS_FAIL;
    }
    return LS_SUCC;
}

int operaSaveDataCount(Ele_Ctrl_Dev devid,enum OperaINI mOpera,LS_U32 *mValue)
{
    int value = 0;
    char mBuf[20]={0};
    char pKey[20]={0};
    getConfigAbsolutePath();
    memset(pKey,0,sizeof(pKey));
    if(devid == Ele_DevID_0){
        strcpy(pKey,"dev0-save");
    }else{
        strcpy(pKey,"dev1-save");
    }
    if(mOpera==SET_OPERA){
        value = *mValue;
        memset(mBuf,0,sizeof(mBuf));
        sprintf(mBuf,"%d",value);
        if(SetValueToEtcFile(cfgPath, "system", pKey, mBuf)!=ETC_OK){
            ls_err_debug("ERR");
            return LS_FAIL;
        }
        return LS_SUCC;
    }
    *mValue = 0;
    if(GetIntValueFromEtcFile(cfgPath, "system", pKey, (int*)mValue)!= ETC_OK){
        ls_err_debug("ERR");
        return LS_FAIL;
    }
    return LS_SUCC;
}

int operaCheckCount(Ele_Ctrl_Dev devid,enum OperaINI mOpera,LS_U32 *mValue)
{
    int value = 0;
    char mBuf[20]={0};
    char pKey[20]={0};
    getConfigAbsolutePath();
    memset(pKey,0,sizeof(pKey));
    if(devid == Ele_DevID_0){
        strcpy(pKey,"dev0-check");
    }else{
        strcpy(pKey,"dev1-check");
    }
    if(mOpera==SET_OPERA){
        value = *mValue;
        memset(mBuf,0,sizeof(mBuf));
        sprintf(mBuf,"%d",value);
        if(SetValueToEtcFile(cfgPath, "system", pKey, mBuf)!=ETC_OK){
            ls_err_debug("ERR");
            return LS_FAIL;
        }
        return LS_SUCC;
    }
    *mValue = 0;
    if(GetIntValueFromEtcFile(cfgPath, "system", pKey, (int*)mValue)!= ETC_OK){
        ls_err_debug("ERR");
        return LS_FAIL;
    }
    return LS_SUCC;
}

int operaSysArg(enum OperaINI mOpera,TSysSet *tSysArg)
{
    char mBuf[30]={0};
    getConfigAbsolutePath();
    if(mOpera==SET_OPERA){
        memset(mBuf,0,sizeof(mBuf));
        sprintf(mBuf,"%d",tSysArg->eleDevPort[Ele_DevID_0]);
        if(SetValueToEtcFile(cfgPath, "system", "dev0-port",mBuf)!=ETC_OK){
            ls_err_debug("ERR");
            return LS_FAIL;
        }

        memset(mBuf,0,sizeof(mBuf));
        sprintf(mBuf,"%d",tSysArg->eleDevPort[Ele_DevID_1]);
        if(SetValueToEtcFile(cfgPath, "system", "dev1-port", mBuf)!=ETC_OK){
            ls_err_debug("ERR");
            return LS_FAIL;
        }
        memset(mBuf,0,sizeof(mBuf));
        sprintf(mBuf,"%d",tSysArg->eleTempPort[Ele_DevID_0]);
        if(SetValueToEtcFile(cfgPath, "system", "temp0-port",mBuf)!=ETC_OK){
            ls_err_debug("ERR");
            return LS_FAIL;
        }

        memset(mBuf,0,sizeof(mBuf));
        sprintf(mBuf,"%d",tSysArg->eleTempPort[Ele_DevID_1]);
        if(SetValueToEtcFile(cfgPath, "system", "temp1-port", mBuf)!=ETC_OK){
            ls_err_debug("ERR");
            return LS_FAIL;
        }
        memset(mBuf,0,sizeof(mBuf));
        sprintf(mBuf,"%d",tSysArg->gpsPort);
        if(SetValueToEtcFile(cfgPath, "system", "gpsPort", mBuf)!=ETC_OK){
            ls_err_debug("ERR");
            return LS_FAIL;
        }
        return LS_SUCC;
    }
    memset(tSysArg,0,sizeof(TSysSet));
    if(GetIntValueFromEtcFile(cfgPath, "system", "dev0-port", &(tSysArg->eleDevPort[Ele_DevID_0]))!= ETC_OK){
        ls_err_debug("ERR");
        return LS_FAIL;
    }

    if(GetIntValueFromEtcFile(cfgPath, "system", "dev1-port", &(tSysArg->eleDevPort[Ele_DevID_1]))!= ETC_OK){
        ls_err_debug("ERR");
        return LS_FAIL;
    }

    if(GetIntValueFromEtcFile(cfgPath, "system", "temp0-port", &(tSysArg->eleTempPort[Ele_DevID_0]))!= ETC_OK){
        ls_err_debug("ERR");
        return LS_FAIL;
    }

    if(GetIntValueFromEtcFile(cfgPath, "system", "temp1-port", &(tSysArg->eleTempPort[Ele_DevID_1]))!= ETC_OK){
        ls_err_debug("ERR");
        return LS_FAIL;
    }

    if(GetIntValueFromEtcFile(cfgPath, "system", "gpsPort", &(tSysArg->gpsPort))!= ETC_OK){
        ls_err_debug("ERR");
        return LS_FAIL;
    }
    return LS_SUCC;
}

int operaSavePath(enum OperaINI mOpera,LS_S8 *path)
{
    LS_S8 filePath[MAX_PATH]={0};
    if(path == NULL) return LS_FAIL;
    getConfigAbsolutePath();
    if(mOpera==SET_OPERA){
        if(SetValueToEtcFile(cfgPath, "system", "saveDir",path)!=ETC_OK){
            ls_err_debug("ERR");
            return LS_FAIL;
        }
        return LS_SUCC;
    }
    memset(filePath,0,sizeof(filePath));
    if(GetValueFromEtcFile(cfgPath, "system", "saveDir", filePath,sizeof(filePath))!= ETC_OK){
        ls_err_debug("ERR");
        return LS_FAIL;
    }
    strcpy(path,filePath);
    return LS_SUCC;
}


int operaSoftRect(enum OperaINI mOpera,TSoftRect *tSoftRect)
{
    char mBuf[10]={0};
    char paraKey[][20]={"EleCtrlDlg","EleSwitchDlg","EleSysCtrlDlg","sysSetDlg","EleChartDlg","EleInfoDlg"};
    int paraCount = (sizeof(paraKey)/sizeof(paraKey[0]));
    getConfigAbsolutePath();
    if(mOpera==SET_OPERA){
        for(int i = 0;i<paraCount;i++){
            memset(mBuf,0,sizeof(mBuf));
            sprintf(mBuf,"%d",tSoftRect->eleRect[i].x);
            if(SetValueToEtcFile(cfgPath, paraKey[i],"x" ,mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
            memset(mBuf,0,sizeof(mBuf));
            sprintf(mBuf,"%d",tSoftRect->eleRect[i].y);
            if(SetValueToEtcFile(cfgPath, paraKey[i],"y" ,mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
            memset(mBuf,0,sizeof(mBuf));
            sprintf(mBuf,"%d",tSoftRect->eleRect[i].w);
            if(SetValueToEtcFile(cfgPath, paraKey[i],"w" ,mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
            memset(mBuf,0,sizeof(mBuf));
            sprintf(mBuf,"%d",tSoftRect->eleRect[i].h);
            if(SetValueToEtcFile(cfgPath, paraKey[i],"h" ,mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
        }
        return LS_SUCC;
    }

    for(int i = 0;i<paraCount;i++){
        if(GetIntValueFromEtcFile(cfgPath, paraKey[i], "x", &(tSoftRect->eleRect[i].x))!= ETC_OK){
            ls_err_debug("%d %s ERR",i,paraKey[i]);
            return LS_FAIL;
        }
        if(GetIntValueFromEtcFile(cfgPath, paraKey[i], "y", &(tSoftRect->eleRect[i].y))!= ETC_OK){
            ls_err_debug("%d %s ERR",i,paraKey[i]);
            return LS_FAIL;
        }
        if(GetIntValueFromEtcFile(cfgPath, paraKey[i], "w", &(tSoftRect->eleRect[i].w))!= ETC_OK){
            ls_err_debug("%d %s ERR",i,paraKey[i]);
            return LS_FAIL;
        }
        if(GetIntValueFromEtcFile(cfgPath, paraKey[i], "h", &(tSoftRect->eleRect[i].h))!= ETC_OK){
            ls_err_debug("%d %s ERR",i,paraKey[i]);
            return LS_FAIL;
        }
    }
    return LS_SUCC;
}

int operaEleSysArg(Ele_Ctrl_Dev devid,enum OperaINI mOpera,TEleSysArg *tSysArg,int index)
{
    char mBuf[10]={0};
    char pSection[20]={0};
    char paraKey[][20]={"ld_para","las_tem","throld","preshh","presll",\
                       "coll_count","volta_a","volta_b","volta_c","judge",\
                       "workmode","rankse_vol","rankse_value"};
    int paraCount = (sizeof(paraKey)/sizeof(paraKey[0]));
    getConfigAbsolutePath();
    memset(pSection,0,sizeof(pSection));
    if(devid==Ele_DevID_0){
        strcpy(pSection,"ElePara-dev0");
    }else if(devid == Ele_DevID_1){
        strcpy(pSection,"ElePara-dev1");
    }else{
        strcpy(pSection,"ElePara-all");
    }
	if(mOpera==SET_OPERA){
        if(index >= Ele_ld_para && index <= Ele_rankse_value){
            memset(mBuf,0,sizeof(mBuf));
            sprintf(mBuf,"%d",tSysArg->iSysValue[index]);
            if(SetValueToEtcFile(cfgPath, pSection, paraKey[index],mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
            return LS_SUCC;
        }
        for(int i = 0;i<paraCount;i++){
            memset(mBuf,0,sizeof(mBuf));
            sprintf(mBuf,"%d",tSysArg->iSysValue[i]);
            if(SetValueToEtcFile(cfgPath, pSection, paraKey[i],mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
        }
        return LS_SUCC;
	}

    for(int i = 0;i<paraCount;i++){
        if(GetIntValueFromEtcFile(cfgPath, pSection, paraKey[i], &(tSysArg->iSysValue[i]))!= ETC_OK){
            ls_err_debug("%d %s ERR",i,paraKey[i]);
            return LS_FAIL;
        }
    }
    return LS_SUCC;
}

int operaEleSwitch(Ele_Ctrl_Dev devid,enum OperaINI mOpera,TEleSwitch *tEleSwitch,int index)
{
    char mBuf[10]={0};
    int value = 0;
    char pSection[20]={0};
    char paraKey[][20]={"boost_pump","vacuum_pump",\
                        "solenoid1","solenoid2","solenoid3","solenoid4",\
                       "gate1","gate2","gate3",\
                        "loop_pump","data_collect","temp_ctrl","dfb_laser"};
    int paraCount = (sizeof(paraKey)/sizeof(paraKey[0]));
    getConfigAbsolutePath();
    memset(pSection,0,sizeof(pSection));
    if(devid==Ele_DevID_0){
        strcpy(pSection,"EleSwitch-dev0");
    }else if(devid == Ele_DevID_1){
        strcpy(pSection,"EleSwitch-dev1");
    }else{
        strcpy(pSection,"EleSwitch-all");
    }
    if(mOpera==SET_OPERA){
        if(index >= Ele_boost_pump && index <= Ele_dfb_laser){
            memset(mBuf,0,sizeof(mBuf));
            if(tEleSwitch->bSwitch[index]){
                sprintf(mBuf,"%d",1);
            }else{
                sprintf(mBuf,"%d",0);
            }
            if(SetValueToEtcFile(cfgPath, pSection, paraKey[index],mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
            return LS_SUCC;
        }
        for(int i = 0;i<paraCount;i++){
            memset(mBuf,0,sizeof(mBuf));
            if(tEleSwitch->bSwitch[index]){
                sprintf(mBuf,"%d",1);
            }else{
                sprintf(mBuf,"%d",0);
            }
            if(SetValueToEtcFile(cfgPath, pSection, paraKey[i],mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
        }
        return LS_SUCC;
    }

    for(int i = 0;i<paraCount;i++){
        if(GetIntValueFromEtcFile(cfgPath, pSection, paraKey[i], &value)!= ETC_OK){
            ls_err_debug("%d %s ERR",i,paraKey[i]);
            return LS_FAIL;
        }
        if(value == 0){
            tEleSwitch->bSwitch[i]=false;
        }else{
            tEleSwitch->bSwitch[i]=true;
        }
    }
    return LS_SUCC;
}

int operaTemArg(Ele_Ctrl_Dev devid,enum OperaINI mOpera,TTemArg *tTemArg,int index)
{
    char mBuf[10]={0};
    int value = 0;
    char pSection[20]={0};
    char paraKey[][20]={"output","temp_set","pro_current","pro_voltage","pro_temp_hh","pro_temp_ll"};
    int paraCount = (sizeof(paraKey)/sizeof(paraKey[0]));
    getConfigAbsolutePath();
    memset(pSection,0,sizeof(pSection));
    if(devid==Ele_DevID_0){
        strcpy(pSection,"temp-dev0");
    }else if(devid == Ele_DevID_1){
        strcpy(pSection,"temp-dev1");
    }else{
        strcpy(pSection,"temp-all");
    }
    if(mOpera==SET_OPERA){
        if(index >= Temp_output && index <= Temp_temp_ll){
            memset(mBuf,0,sizeof(mBuf));
            sprintf(mBuf,"%d",tTemArg->iTempValue[index]);
            if(SetValueToEtcFile(cfgPath, pSection, paraKey[index],mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
            return LS_SUCC;
        }
        for(int i = 0;i<paraCount;i++){
            memset(mBuf,0,sizeof(mBuf));
            sprintf(mBuf,"%d",tTemArg->iTempValue[index]);
            if(SetValueToEtcFile(cfgPath, pSection, paraKey[i],mBuf)!=ETC_OK){
                ls_err_debug("ERR");
                return LS_FAIL;
            }
        }
        return LS_SUCC;
    }

    for(int i = 0;i<paraCount;i++){
        if(GetIntValueFromEtcFile(cfgPath, pSection, paraKey[i], &value)!= ETC_OK){
            ls_err_debug("%d %s ERR",i,paraKey[i]);
            return LS_FAIL;
        }
        tTemArg->iTempValue[i]=value;
    }
    return LS_SUCC;
}
