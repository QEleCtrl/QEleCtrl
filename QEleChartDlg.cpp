#include "QEleChartDlg.h"
#include "ui_QEleChartDlg.h"

QEleChartDlg::QEleChartDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QEleChartDlg)
{
    ui->setupUi(this);
    initEleChartDlg();
}

QEleChartDlg::~QEleChartDlg()
{
    delete ui;
}

void QEleChartDlg::destroyDlg()
{
    bDestroy = true;
}

void QEleChartDlg::initEleChartDlg()
{
    QPalette palette(this->palette());
    palette.setColor(QPalette::Background,QColor(DLG_BK_COLOR));
    this->setPalette(palette);
    ui->GV_chart0->setPalette(palette);
    ui->GV_chart1->setPalette(palette);
    initEleChart();
}

void QEleChartDlg::initEleChart()
{
    QString str="";
    bDestroy = false;
    for(int i=0;i<ELE_MAX_NUM;i++){
        eleChart[i]=new QChart();
        eleChart[i]->setTheme(QChart::ChartThemeQt);
        str = QString("压力值%1-趋势图").arg(i+1);
        eleChart[i]->setTitle(str);
        eleChart[i]->setTitleFont(QFont("微软雅黑",10));
        eleChart[i]->legend()->hide();

        eleSeries[i]=new QSplineSeries();
        eleSeries[i]->setUseOpenGL(true);
        eleSeries[i]->setName("eleSeries1");
        QPen penLine(Qt::darkGreen,ELE_CURVE_WIDTH,Qt::SolidLine,Qt::RoundCap,Qt::RoundJoin);
        eleSeries[i]->setPen(penLine);
        eleChart[i]->addSeries(eleSeries[i]);

        QValueAxis *axisX=new QValueAxis();
        axisX->setTickCount(9);
        axisX->setRange(0, DATA_COUNT);
        axisX->setLabelFormat("%u");
        axisX->setTitleText("采样计次");
        //axisX->setGridLineVisible(false);
        QValueAxis *axisY=new QValueAxis();
        chartMinValue[i] = 0;
        chartMaxValue[i] = MAX_DATA_VALUE;
        axisY->setMin(chartMinValue[i]);
        axisY->setMax(chartMaxValue[i]);
        axisY->setLabelFormat("%u");
        axisY->setTickCount(5);
        axisY->setTitleText("压力值");
        axisY->setLinePenColor(QColor(Qt::darkBlue));
        axisY->setLinePenColor(QColor(Qt::darkGreen));
        axisY->setGridLineVisible(false);
        QPen penY(Qt::darkBlue,3,Qt::SolidLine,Qt::RoundCap,Qt::RoundJoin);
        axisY->setLinePen(penY);
        eleChart[i]->addAxis(axisX,Qt::AlignBottom);
        eleChart[i]->addAxis(axisY,Qt::AlignLeft);
        eleSeries[i]->attachAxis(axisX);
        eleSeries[i]->attachAxis(axisY);
    }
    //把chart显示到窗口上
    ui->GV_chart0->setRenderHint(QPainter::Antialiasing);
    ui->GV_chart1->setRenderHint(QPainter::Antialiasing);
    ui->GV_chart0->setChart(eleChart[0]);
    ui->GV_chart1->setChart(eleChart[1]);
}

#include<iostream>
#include<algorithm>
using namespace std;
void QEleChartDlg::eleChartDrawLines(int index,double *dValue)
{
    if(bDestroy)return;
    double yMax=0,yMin=0;
    double maxValue=0,minValue=0;
    QList<QPointF> mPointList;
    if(index <0||index >=ELE_MAX_NUM)return;
    maxValue=*max_element(dValue,dValue+500);
    minValue=*min_element(dValue+(DATA_COUNT-500),dValue+DATA_COUNT);
    if(((int)maxValue%1000)>500){
        yMax = ((int)(maxValue/1000))*1000+1000;
    }else{
        yMax = ((int)(maxValue/1000))*1000+500;
    }
    if(((int)minValue%1000)<500){
        yMin = ((int)(minValue/1000))*1000;
    }else{
        yMin = ((int)(minValue/1000))*1000+500;
    }
    if(yMin!=chartMinValue[index] || yMax != chartMaxValue[index]){
        chartMinValue[index] = yMin;
        chartMaxValue[index] = yMax;
        eleChart[index]->axisY()->setMin(chartMinValue[index]);
        eleChart[index]->axisY()->setMax(chartMaxValue[index]);
    }
    eleChartsMtx.lock();
    for(int i=0;i<DATA_COUNT;i++)
    {
        QPointF mPointF;
        mPointF.setX(i);
        mPointF.setY(dValue[i]);
        mPointList.insert(i,mPointF);
    }
    eleSeries[index]->replace(mPointList);
    eleChartsMtx.unlock();
}

