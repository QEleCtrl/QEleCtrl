#ifndef QSYSSETDLG_H
#define QSYSSETDLG_H

#include <QDialog>
#include <QListView>
#include <QFileDialog>
#include "util.h"
#include "types.h"
#include "config.h"

namespace Ui {
class QSysSetDlg;
}

class QSysSetDlg : public QDialog
{
    Q_OBJECT

public:
    explicit QSysSetDlg(QWidget *parent = 0);
    ~QSysSetDlg();

private:
    Ui::QSysSetDlg *ui;

public:
    void setSysSetDlg(TSysSet tSysArg);
    void setSysSetSavePath(LS_S8 *path);

signals:
    void signalSysSetPath(QString str);
    void signalSysSetRet(TSysSet *tSysArg,QString str);
    void signalSysSetMsg(QString str,MsgType type=MSG_ERR);
private slots:
    void on_PB_SYSSET_clicked();
    void on_PB_save_path_clicked();
};

#endif // QSYSSETDLG_H
