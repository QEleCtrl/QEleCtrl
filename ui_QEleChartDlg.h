/********************************************************************************
** Form generated from reading UI file 'QEleChartDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QELECHARTDLG_H
#define UI_QELECHARTDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <qchartview.h>

QT_BEGIN_NAMESPACE

class Ui_QEleChartDlg
{
public:
    QGridLayout *gridLayout;
    QChartView *GV_chart0;
    QChartView *GV_chart1;

    void setupUi(QDialog *QEleChartDlg)
    {
        if (QEleChartDlg->objectName().isEmpty())
            QEleChartDlg->setObjectName(QString::fromUtf8("QEleChartDlg"));
        QEleChartDlg->resize(445, 417);
        gridLayout = new QGridLayout(QEleChartDlg);
        gridLayout->setSpacing(1);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(1, 1, 1, 1);
        GV_chart0 = new QChartView(QEleChartDlg);
        GV_chart0->setObjectName(QString::fromUtf8("GV_chart0"));

        gridLayout->addWidget(GV_chart0, 0, 0, 1, 1);

        GV_chart1 = new QChartView(QEleChartDlg);
        GV_chart1->setObjectName(QString::fromUtf8("GV_chart1"));

        gridLayout->addWidget(GV_chart1, 1, 0, 1, 1);


        retranslateUi(QEleChartDlg);

        QMetaObject::connectSlotsByName(QEleChartDlg);
    } // setupUi

    void retranslateUi(QDialog *QEleChartDlg)
    {
        QEleChartDlg->setWindowTitle(QApplication::translate("QEleChartDlg", "\351\207\207\346\240\267\346\233\262\347\272\277\345\233\276", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QEleChartDlg: public Ui_QEleChartDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QELECHARTDLG_H
