/********************************************************************************
** Form generated from reading UI file 'QEleSwitchDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QELESWITCHDLG_H
#define UI_QELESWITCHDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_QEleSwitchDlg
{
public:
    QGridLayout *gridLayout;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_4;
    QRadioButton *RB_vacuum_pump_on;
    QRadioButton *RB_vacuum_pump_off;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_8;
    QRadioButton *RB_gate2_on;
    QRadioButton *RB_gate2_off;
    QGroupBox *groupBox_11;
    QGridLayout *gridLayout_12;
    QRadioButton *RB_solenoid3_on;
    QRadioButton *RB_solenoid3_off;
    QGroupBox *groupBox_9;
    QGridLayout *gridLayout_10;
    QRadioButton *RB_solenoid1_on;
    QRadioButton *RB_solenoid1_off;
    QGroupBox *groupBox_12;
    QGridLayout *gridLayout_13;
    QRadioButton *RB_solenoid4_on;
    QRadioButton *RB_solenoid4_off;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_7;
    QRadioButton *RB_gate1_on;
    QRadioButton *RB_gate1_off;
    QGroupBox *groupBox_10;
    QGridLayout *gridLayout_11;
    QRadioButton *RB_solenoid2_on;
    QRadioButton *RB_solenoid2_off;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_3;
    QRadioButton *RB_boost_pump_on;
    QRadioButton *RB_boost_pump_off;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_6;
    QRadioButton *RB_temp_ctrl_on;
    QRadioButton *RB_temp_ctrl_off;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_9;
    QRadioButton *RB_gate3_on;
    QRadioButton *RB_gate3_off;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QRadioButton *RB_loop_pump_on;
    QRadioButton *RB_loop_pump_off;
    QGroupBox *groupBox_13;
    QGridLayout *gridLayout_14;
    QRadioButton *RB_data_collect_on;
    QRadioButton *RB_data_collect_off;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_5;
    QRadioButton *RB_dfb_laser_on;
    QRadioButton *RB_dfb_laser_off;

    void setupUi(QDialog *QEleSwitchDlg)
    {
        if (QEleSwitchDlg->objectName().isEmpty())
            QEleSwitchDlg->setObjectName(QString::fromUtf8("QEleSwitchDlg"));
        QEleSwitchDlg->resize(480, 398);
        QEleSwitchDlg->setMinimumSize(QSize(0, 0));
        QEleSwitchDlg->setMaximumSize(QSize(65535, 65535));
        gridLayout = new QGridLayout(QEleSwitchDlg);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox_3 = new QGroupBox(QEleSwitchDlg);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_4 = new QGridLayout(groupBox_3);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(-1, 30, -1, -1);
        RB_vacuum_pump_on = new QRadioButton(groupBox_3);
        RB_vacuum_pump_on->setObjectName(QString::fromUtf8("RB_vacuum_pump_on"));

        gridLayout_4->addWidget(RB_vacuum_pump_on, 0, 0, 1, 1);

        RB_vacuum_pump_off = new QRadioButton(groupBox_3);
        RB_vacuum_pump_off->setObjectName(QString::fromUtf8("RB_vacuum_pump_off"));

        gridLayout_4->addWidget(RB_vacuum_pump_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_3, 0, 2, 1, 1);

        groupBox_7 = new QGroupBox(QEleSwitchDlg);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        gridLayout_8 = new QGridLayout(groupBox_7);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_8->setContentsMargins(-1, 30, -1, -1);
        RB_gate2_on = new QRadioButton(groupBox_7);
        RB_gate2_on->setObjectName(QString::fromUtf8("RB_gate2_on"));

        gridLayout_8->addWidget(RB_gate2_on, 0, 0, 1, 1);

        RB_gate2_off = new QRadioButton(groupBox_7);
        RB_gate2_off->setObjectName(QString::fromUtf8("RB_gate2_off"));

        gridLayout_8->addWidget(RB_gate2_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_7, 1, 2, 1, 1);

        groupBox_11 = new QGroupBox(QEleSwitchDlg);
        groupBox_11->setObjectName(QString::fromUtf8("groupBox_11"));
        gridLayout_12 = new QGridLayout(groupBox_11);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        gridLayout_12->setContentsMargins(-1, 30, -1, -1);
        RB_solenoid3_on = new QRadioButton(groupBox_11);
        RB_solenoid3_on->setObjectName(QString::fromUtf8("RB_solenoid3_on"));

        gridLayout_12->addWidget(RB_solenoid3_on, 0, 0, 1, 1);

        RB_solenoid3_off = new QRadioButton(groupBox_11);
        RB_solenoid3_off->setObjectName(QString::fromUtf8("RB_solenoid3_off"));

        gridLayout_12->addWidget(RB_solenoid3_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_11, 2, 2, 1, 1);

        groupBox_9 = new QGroupBox(QEleSwitchDlg);
        groupBox_9->setObjectName(QString::fromUtf8("groupBox_9"));
        gridLayout_10 = new QGridLayout(groupBox_9);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        gridLayout_10->setContentsMargins(-1, 30, -1, -1);
        RB_solenoid1_on = new QRadioButton(groupBox_9);
        RB_solenoid1_on->setObjectName(QString::fromUtf8("RB_solenoid1_on"));
        RB_solenoid1_on->setLayoutDirection(Qt::LeftToRight);

        gridLayout_10->addWidget(RB_solenoid1_on, 0, 0, 1, 1);

        RB_solenoid1_off = new QRadioButton(groupBox_9);
        RB_solenoid1_off->setObjectName(QString::fromUtf8("RB_solenoid1_off"));

        gridLayout_10->addWidget(RB_solenoid1_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_9, 2, 0, 1, 1);

        groupBox_12 = new QGroupBox(QEleSwitchDlg);
        groupBox_12->setObjectName(QString::fromUtf8("groupBox_12"));
        gridLayout_13 = new QGridLayout(groupBox_12);
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        gridLayout_13->setContentsMargins(-1, 30, -1, -1);
        RB_solenoid4_on = new QRadioButton(groupBox_12);
        RB_solenoid4_on->setObjectName(QString::fromUtf8("RB_solenoid4_on"));

        gridLayout_13->addWidget(RB_solenoid4_on, 0, 0, 1, 1);

        RB_solenoid4_off = new QRadioButton(groupBox_12);
        RB_solenoid4_off->setObjectName(QString::fromUtf8("RB_solenoid4_off"));

        gridLayout_13->addWidget(RB_solenoid4_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_12, 2, 3, 1, 1);

        groupBox_6 = new QGroupBox(QEleSwitchDlg);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        gridLayout_7 = new QGridLayout(groupBox_6);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout_7->setContentsMargins(-1, 30, -1, -1);
        RB_gate1_on = new QRadioButton(groupBox_6);
        RB_gate1_on->setObjectName(QString::fromUtf8("RB_gate1_on"));

        gridLayout_7->addWidget(RB_gate1_on, 0, 0, 1, 1);

        RB_gate1_off = new QRadioButton(groupBox_6);
        RB_gate1_off->setObjectName(QString::fromUtf8("RB_gate1_off"));

        gridLayout_7->addWidget(RB_gate1_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_6, 1, 1, 1, 1);

        groupBox_10 = new QGroupBox(QEleSwitchDlg);
        groupBox_10->setObjectName(QString::fromUtf8("groupBox_10"));
        gridLayout_11 = new QGridLayout(groupBox_10);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        gridLayout_11->setContentsMargins(-1, 30, -1, -1);
        RB_solenoid2_on = new QRadioButton(groupBox_10);
        RB_solenoid2_on->setObjectName(QString::fromUtf8("RB_solenoid2_on"));

        gridLayout_11->addWidget(RB_solenoid2_on, 0, 0, 1, 1);

        RB_solenoid2_off = new QRadioButton(groupBox_10);
        RB_solenoid2_off->setObjectName(QString::fromUtf8("RB_solenoid2_off"));

        gridLayout_11->addWidget(RB_solenoid2_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_10, 2, 1, 1, 1);

        groupBox_2 = new QGroupBox(QEleSwitchDlg);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_3 = new QGridLayout(groupBox_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(-1, 30, -1, -1);
        RB_boost_pump_on = new QRadioButton(groupBox_2);
        RB_boost_pump_on->setObjectName(QString::fromUtf8("RB_boost_pump_on"));

        gridLayout_3->addWidget(RB_boost_pump_on, 0, 0, 1, 1);

        RB_boost_pump_off = new QRadioButton(groupBox_2);
        RB_boost_pump_off->setObjectName(QString::fromUtf8("RB_boost_pump_off"));

        gridLayout_3->addWidget(RB_boost_pump_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_2, 0, 1, 1, 1);

        groupBox_5 = new QGroupBox(QEleSwitchDlg);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        gridLayout_6 = new QGridLayout(groupBox_5);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setContentsMargins(-1, 30, -1, -1);
        RB_temp_ctrl_on = new QRadioButton(groupBox_5);
        RB_temp_ctrl_on->setObjectName(QString::fromUtf8("RB_temp_ctrl_on"));

        gridLayout_6->addWidget(RB_temp_ctrl_on, 0, 0, 1, 1);

        RB_temp_ctrl_off = new QRadioButton(groupBox_5);
        RB_temp_ctrl_off->setObjectName(QString::fromUtf8("RB_temp_ctrl_off"));

        gridLayout_6->addWidget(RB_temp_ctrl_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_5, 1, 0, 1, 1);

        groupBox_8 = new QGroupBox(QEleSwitchDlg);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        gridLayout_9 = new QGridLayout(groupBox_8);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        gridLayout_9->setContentsMargins(-1, 30, -1, -1);
        RB_gate3_on = new QRadioButton(groupBox_8);
        RB_gate3_on->setObjectName(QString::fromUtf8("RB_gate3_on"));

        gridLayout_9->addWidget(RB_gate3_on, 0, 0, 1, 1);

        RB_gate3_off = new QRadioButton(groupBox_8);
        RB_gate3_off->setObjectName(QString::fromUtf8("RB_gate3_off"));

        gridLayout_9->addWidget(RB_gate3_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_8, 1, 3, 1, 1);

        groupBox = new QGroupBox(QEleSwitchDlg);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 30, -1, 5);
        RB_loop_pump_on = new QRadioButton(groupBox);
        RB_loop_pump_on->setObjectName(QString::fromUtf8("RB_loop_pump_on"));

        gridLayout_2->addWidget(RB_loop_pump_on, 0, 0, 1, 1);

        RB_loop_pump_off = new QRadioButton(groupBox);
        RB_loop_pump_off->setObjectName(QString::fromUtf8("RB_loop_pump_off"));

        gridLayout_2->addWidget(RB_loop_pump_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_13 = new QGroupBox(QEleSwitchDlg);
        groupBox_13->setObjectName(QString::fromUtf8("groupBox_13"));
        gridLayout_14 = new QGridLayout(groupBox_13);
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        gridLayout_14->setContentsMargins(-1, 30, -1, -1);
        RB_data_collect_on = new QRadioButton(groupBox_13);
        RB_data_collect_on->setObjectName(QString::fromUtf8("RB_data_collect_on"));

        gridLayout_14->addWidget(RB_data_collect_on, 0, 0, 1, 1);

        RB_data_collect_off = new QRadioButton(groupBox_13);
        RB_data_collect_off->setObjectName(QString::fromUtf8("RB_data_collect_off"));

        gridLayout_14->addWidget(RB_data_collect_off, 0, 1, 1, 1);


        gridLayout->addWidget(groupBox_13, 3, 1, 1, 2);

        groupBox_4 = new QGroupBox(QEleSwitchDlg);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_5 = new QGridLayout(groupBox_4);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(-1, 30, -1, -1);
        RB_dfb_laser_on = new QRadioButton(groupBox_4);
        RB_dfb_laser_on->setObjectName(QString::fromUtf8("RB_dfb_laser_on"));

        gridLayout_5->addWidget(RB_dfb_laser_on, 0, 0, 1, 1);

        RB_dfb_laser_off = new QRadioButton(groupBox_4);
        RB_dfb_laser_off->setObjectName(QString::fromUtf8("RB_dfb_laser_off"));

        gridLayout_5->addWidget(RB_dfb_laser_off, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox_4, 0, 3, 1, 1);

        gridLayout->setRowStretch(0, 3);
        gridLayout->setRowStretch(1, 3);
        gridLayout->setRowStretch(2, 3);
        gridLayout->setRowStretch(3, 2);

        retranslateUi(QEleSwitchDlg);

        QMetaObject::connectSlotsByName(QEleSwitchDlg);
    } // setupUi

    void retranslateUi(QDialog *QEleSwitchDlg)
    {
        QEleSwitchDlg->setWindowTitle(QApplication::translate("QEleSwitchDlg", "\347\224\265\346\234\272\345\222\214\347\224\265\347\243\201\351\230\200\346\216\247\345\210\266", nullptr));
        groupBox_3->setTitle(QApplication::translate("QEleSwitchDlg", "\351\232\224\350\206\234\345\242\236\345\216\213\346\263\265", nullptr));
        RB_vacuum_pump_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_vacuum_pump_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_7->setTitle(QApplication::translate("QEleSwitchDlg", "\351\200\211\351\200\232\351\230\2002", nullptr));
        RB_gate2_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_gate2_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_11->setTitle(QApplication::translate("QEleSwitchDlg", "\347\224\265\347\243\201\351\230\2003", nullptr));
        RB_solenoid3_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_solenoid3_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_9->setTitle(QApplication::translate("QEleSwitchDlg", "\347\224\265\347\243\201\351\230\2001", nullptr));
        RB_solenoid1_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_solenoid1_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_12->setTitle(QApplication::translate("QEleSwitchDlg", "\347\224\265\347\243\201\351\230\2004", nullptr));
        RB_solenoid4_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_solenoid4_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_6->setTitle(QApplication::translate("QEleSwitchDlg", "\351\200\211\351\200\232\351\230\2001", nullptr));
        RB_gate1_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_gate1_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_10->setTitle(QApplication::translate("QEleSwitchDlg", "\347\224\265\347\243\201\351\230\2002", nullptr));
        RB_solenoid2_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_solenoid2_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_2->setTitle(QApplication::translate("QEleSwitchDlg", "\351\232\224\350\206\234\347\234\237\347\251\272\346\263\265", nullptr));
        RB_boost_pump_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_boost_pump_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_5->setTitle(QApplication::translate("QEleSwitchDlg", "\346\270\251\346\216\247\346\250\241\345\235\227", nullptr));
        RB_temp_ctrl_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_temp_ctrl_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_8->setTitle(QApplication::translate("QEleSwitchDlg", "\351\200\211\351\200\232\351\230\2003", nullptr));
        RB_gate3_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_gate3_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox->setTitle(QApplication::translate("QEleSwitchDlg", "\346\267\261\346\265\267\345\276\252\347\216\257\346\263\265", nullptr));
        RB_loop_pump_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_loop_pump_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_13->setTitle(QApplication::translate("QEleSwitchDlg", "\346\225\260\346\215\256\351\207\207\351\233\206\346\235\277", nullptr));
        RB_data_collect_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_data_collect_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
        groupBox_4->setTitle(QApplication::translate("QEleSwitchDlg", "DFB\346\277\200\345\205\211\351\251\261\345\212\250", nullptr));
        RB_dfb_laser_on->setText(QApplication::translate("QEleSwitchDlg", "\345\274\200", nullptr));
        RB_dfb_laser_off->setText(QApplication::translate("QEleSwitchDlg", "\345\205\263", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QEleSwitchDlg: public Ui_QEleSwitchDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QELESWITCHDLG_H
