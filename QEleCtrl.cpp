#include "QEleCtrl.h"
#include "ui_QEleCtrl.h"

static bool             bInitDlg=false;
static int              fGlobalTempValue[ELE_MAX_NUM]={0};
SendoutPtrBuffer        eleOutQue;
SendoutPtrBuffer        tempOutQue;

typedef struct _TthreadArg{
    QEleCtrl *pDlg;
    int      index;
}TthreadArg;

void *recvGpsThread(void *arg)
{
    QEleCtrl *pDlg  = (QEleCtrl*)arg;
    LS_S8  gpsRecvBuf[1024*100]={0};
    LS_S32 cacheLen = sizeof(gpsRecvBuf);
    LS_S32 cacheLeftLen  = cacheLen;
    LS_S32 cacheSpendLen = 0;
    LS_S32 recvLen=0,ret=0;
    LS_S32 mHeadLocation[100];
    LS_S32 mHeadCount = 0;
    LS_S32 frameLen = 0;
    LS_S8_DATA_PACKET *gpsData = nullptr;
    while(!bInitDlg){
        myMSleep(1000);
    }
    pDlg->bAllThreadExit=false;
    while(!pDlg->bAllThreadExit)
    {
        gpsData = pDlg->gpsQue->PopPacket();
        if(gpsData==nullptr){
            myMSleep(100);
            continue;
        }
        ls_info_debug("");
        if(gpsData->data==nullptr){
            free(gpsData);
            gpsData=nullptr;
            myMSleep(100);
            continue;
        }
        recvLen = gpsData->size;
        cacheLeftLen=cacheLen-cacheSpendLen;
        if(recvLen>cacheLen||recvLen<=0)
        {
            free(gpsData->data);
            gpsData->data = nullptr;
            delete(gpsData);
            gpsData=nullptr;
            continue;
        }
        if(cacheLeftLen <= recvLen){cacheSpendLen=0;cacheLeftLen=cacheLen;}
        memcpy(gpsRecvBuf+cacheSpendLen,gpsData->data,recvLen);
        {
            free(gpsData->data);
            gpsData->data = nullptr;
            delete(gpsData);
            gpsData=nullptr;
        }
        cacheSpendLen+=recvLen;
        ret = checkGpsHeadCount(gpsRecvBuf,cacheSpendLen,&mHeadCount,mHeadLocation);
        if(ret <= 0){
            continue;
        }
        for(int i=0;i<mHeadCount-1;i++)
        {
            frameLen=mHeadLocation[i+1]-mHeadLocation[i];
            LS_S8 *p = gpsRecvBuf+mHeadLocation[i];
            LS_S32 len = frameLen;
            pDlg->handleRecvGpsBuf(p,len);
        }
        if((unsigned char )gpsRecvBuf[cacheSpendLen-5]==0x2A){
            frameLen=cacheSpendLen-mHeadLocation[mHeadCount-1];
            LS_S8 *p = gpsRecvBuf+mHeadLocation[mHeadCount-1];
            LS_S32 len = frameLen;
            pDlg->handleRecvGpsBuf(p,len);
            cacheSpendLen = 0;
            continue;
        }
        cacheSpendLen = cacheSpendLen-mHeadLocation[mHeadCount-1];
        memmove(gpsRecvBuf,gpsRecvBuf+mHeadLocation[mHeadCount-1],cacheSpendLen);
    }
    ls_info_debug("Thread End");
    return nullptr;
}

void *recvEleThread(void *arg)
{
    TthreadArg *pArg = (TthreadArg *)arg;
    QEleCtrl *pDlg  = pArg->pDlg;
    int index = pArg->index;
    if(index<Ele_DevID_0||index>=ELE_MAX_NUM)return nullptr;
    pDlg->bAllThreadExit=false;
    LS_S8 uartRecvBuf[1024*100] = {0};
    LS_S32 cacheLen = sizeof(uartRecvBuf);
    LS_S32 cacheLeftLen  = cacheLen;
    LS_S32 cacheSpendLen = 0;
    LS_S32 recvLen=0,ret=0;
    LS_S32 mHeadLocation[100];
    LS_S32 mHeadCount = 0;
    LS_S32 frameLen = 0;
    LS_U8 *p=nullptr;
    LS_S8_DATA_PACKET *eleData=nullptr;
    while(!bInitDlg){
        myMSleep(1000);
    }
    pDlg->iGlobalDevRecvDataCount[index] = 0;
    while(!pDlg->bAllThreadExit)
    {
        eleData = pDlg->eleQue[index]->PopPacket();
        if(eleData==nullptr){
            myMSleep(100);
            continue;
        }
        if(eleData->data==nullptr){
            free(eleData);
            eleData=nullptr;
            myMSleep(100);
            continue;
        }
        cacheLeftLen=cacheLen-cacheSpendLen;
        recvLen = eleData->size;
        if(recvLen > cacheLen||recvLen<=0){
            free(eleData->data);
            eleData->data = nullptr;
            delete(eleData);
            eleData=nullptr;
            continue;
        }
        if(cacheLeftLen <= recvLen){cacheSpendLen=0;cacheLeftLen=cacheLen;}
        memcpy(uartRecvBuf+cacheSpendLen,eleData->data,recvLen);
        {
            free(eleData->data);
            eleData->data = nullptr;
            delete(eleData);
            eleData=nullptr;
        }
        pDlg->iGlobalDevRecvDataCount[index]++;
        cacheSpendLen+=recvLen;
        if(uartRecvBuf[0]!=0x24){
            cacheSpendLen = 0;
            cacheLeftLen = sizeof(uartRecvBuf);
        }
        ret = checkEleHeadCount(uartRecvBuf,cacheSpendLen,&mHeadCount,mHeadLocation);
        if(ret <= 0){
            continue;
        }
        for(int i=0;i<mHeadCount-1;i++)
        {
            LS_S8_DATA_PACKET *mData=new LS_S8_DATA_PACKET;
            p = (LS_U8 *)uartRecvBuf+mHeadLocation[i];
            frameLen=mHeadLocation[i+1]-mHeadLocation[i];
            mData->data = uartRecvBuf+mHeadLocation[i];
            mData->size = frameLen;
            if(p[1]==0xAA&&p[2]==0xAA){
                pDlg->dataQue[index]->PushPacket(mData);
            }else{
                pDlg->commQue[index]->PushPacket(mData);
            }
            delete mData;
            mData = nullptr;
        }
        if(((unsigned char )uartRecvBuf[cacheSpendLen-1]==0x0A) && ((unsigned char )uartRecvBuf[cacheSpendLen-2]==0x0D)){
            LS_S8_DATA_PACKET *mData=new LS_S8_DATA_PACKET;
            p = (LS_U8 *)uartRecvBuf+mHeadLocation[mHeadCount-1];
            frameLen=cacheSpendLen-mHeadLocation[mHeadCount-1];
            mData->data = uartRecvBuf+mHeadLocation[mHeadCount-1];
            mData->size = frameLen;
            if(p[1]==0xAA&&p[2]==0xAA){
                pDlg->dataQue[index]->PushPacket(mData);
            }else{
                pDlg->commQue[index]->PushPacket(mData);
            }
            delete mData;
            mData = nullptr;
            cacheSpendLen = 0;
            continue;
        }
        cacheSpendLen = cacheSpendLen-mHeadLocation[mHeadCount-1];;
        memmove(uartRecvBuf,uartRecvBuf+mHeadLocation[mHeadCount-1],cacheSpendLen);
    }
    ls_info_debug("%d Thread End",index);
    return nullptr;
}

void *recvTempThread(void *arg)
{
    TthreadArg *pArg = (TthreadArg *)arg;
    QEleCtrl *pDlg  = pArg->pDlg;
    int index = pArg->index;
    if(index<Ele_DevID_0||index>=ELE_MAX_NUM)return nullptr;
    LS_S8  tempRecvBuf[1024*100]={0};
    LS_S32 cacheLen = sizeof(tempRecvBuf);
    LS_S32 cacheLeftLen  = cacheLen;
    LS_S32 cacheSpendLen = 0;
    LS_S32 recvLen=0,ret=0;
    LS_S32 mHeadLocation[100];
    LS_S32 mHeadCount = 0;
    LS_S32 frameLen = 0;
    LS_S8_DATA_PACKET *pTempPack=nullptr;
    pDlg->bAllThreadExit=false;
    while(!bInitDlg){
        myMSleep(1000);
    }
    while(!pDlg->bAllThreadExit)
    {
        pTempPack = pDlg->tempQue[index]->PopPacket();
        if(!pTempPack){
            myMSleep(50);
            continue;
        }
        if(!pTempPack->data){
            delete pTempPack;
            pTempPack=nullptr;
        }

        //printBufByHex(__FILE__,__LINE__,pTempPack->data,pTempPack->size);
        //qDebug()<<"thread "<<pTempPack->data;
        recvLen = pTempPack->size;
        cacheLeftLen=cacheLen-cacheSpendLen;
        if(recvLen>cacheLen||recvLen<=0){
            free(pTempPack->data);
            pTempPack->data = nullptr;
            delete(pTempPack);
            pTempPack=nullptr;
            continue;
        }
        if(cacheLeftLen <= recvLen){cacheSpendLen=0;cacheLeftLen=cacheLen;}
        memcpy(tempRecvBuf+cacheSpendLen,pTempPack->data,recvLen);
        {
            free(pTempPack->data);
            pTempPack->data = nullptr;
            delete(pTempPack);
            pTempPack=nullptr;
        }
        cacheSpendLen+=recvLen;
        ret = checkTempHeadCount(tempRecvBuf,cacheSpendLen,&mHeadCount,mHeadLocation);
        if(ret <= 0){
            continue;
        }
        for(int i=0;i<mHeadCount-1;i++)
        {
            frameLen=mHeadLocation[i+1]-mHeadLocation[i];
            LS_S8 *p = tempRecvBuf+mHeadLocation[i];
            LS_S32 len = frameLen;
            pDlg->handleRecvTempBuf(index,p,len-1);
        }
        if((unsigned char )tempRecvBuf[cacheSpendLen-1]==0x0D){
            frameLen=cacheSpendLen-mHeadLocation[mHeadCount-1];
            LS_S8 *p = tempRecvBuf+mHeadLocation[mHeadCount-1];
            LS_S32 len = frameLen;
            pDlg->handleRecvTempBuf(index,p,len-1);
            cacheSpendLen = 0;
            continue;
        }
        cacheSpendLen = cacheSpendLen-mHeadLocation[mHeadCount-1];
        memmove(tempRecvBuf,tempRecvBuf+mHeadLocation[mHeadCount-1],cacheSpendLen);
    }
    ls_info_debug("%d Thread End",index);
    return nullptr;
}

void *analyDataMsg(void *arg)
{
    TthreadArg *pArg = (TthreadArg *)arg;
    QEleCtrl *pDlg  = pArg->pDlg;
    int index = pArg->index;
    if(index<Ele_DevID_0||index>=ELE_MAX_NUM)return nullptr;
    LS_S8_DATA_PACKET *pDataPack=nullptr;
    pDlg->bAllThreadExit=false;
    LS_S8 commBuf[256] = {0};
    while(!bInitDlg){
        myMSleep(1000);
    }
    while(!pDlg->bAllThreadExit)
    {
        pDataPack = pDlg->dataQue[index]->PopPacket();
        if(!pDataPack){
            myMSleep(500);
            continue;
        }
        if(!pDataPack->data){
            delete pDataPack;
            pDataPack=nullptr;
            continue;
        }
        pDlg->handleRecvDataBuf(index,pDataPack->data,pDataPack->size);
        free(pDataPack->data);
        pDataPack->data = nullptr;
        delete pDataPack;
        pDataPack=nullptr;
    }
    ls_info_debug("%d Thread End",index);
    return nullptr;
}

void *analyCommMsg(void *arg)
{
    TthreadArg *pArg = (TthreadArg *)arg;
    QEleCtrl *pDlg  = pArg->pDlg;
    int index = pArg->index;
    if(index<Ele_DevID_0||index>=ELE_MAX_NUM)return nullptr;
    LS_S8_DATA_PACKET *pCommPack=nullptr;
    pDlg->bAllThreadExit=false;
    while(!bInitDlg){
        myMSleep(1000);
    }
    while(!pDlg->bAllThreadExit)
    {
        pCommPack = pDlg->commQue[index]->PopPacket();
        if(!pCommPack){
            myMSleep(500);
            continue;
        }
        if(!pCommPack->data){
            delete pCommPack;
            pCommPack=nullptr;
        }
        pDlg->handleRecvCommBuf(index,pCommPack->data,pCommPack->size);
        free(pCommPack->data);
        pCommPack->data = nullptr;
        delete pCommPack;
        pCommPack=nullptr;
    }
    ls_info_debug("%d Thread End",index);
    return nullptr;
}

void *sendoutEleThread(void *arg)
{
    int index = 0,ret=0;
    QString str="";
    LS_S8 uartSendBuf[1024*10]={0};
    LS_S32 bufLen = sizeof(uartSendBuf),sendLen=0;
    LS_S8_OUT_PACKET *sendPacket = nullptr;
    QEleCtrl *pDlg  = (QEleCtrl*)arg;
    pDlg->bAllThreadExit=false;
    while(!bInitDlg){
        myMSleep(1000);
    }
    while(!pDlg->bAllThreadExit)
    {
        sendPacket = eleOutQue.PopPacket();
        if(sendPacket==nullptr){
            myMSleep(50);
            continue;
        }
        LS_S8 *p = (LS_S8*)sendPacket->data;
        LS_S32 len = sendPacket->size;
        index = sendPacket->devid;
        if(!p){
            ls_info_debug("sendPacket->data = NULL ERR should not happened");
            delete sendPacket;
            sendPacket = nullptr;
            continue;
        }
        if(index != Ele_DevID_0 && index != Ele_DevID_1){
            ls_info_debug("sendPacket->devid = %d ERR should not happened",index);
            free(sendPacket->data);
            sendPacket->data = nullptr;
            delete sendPacket;
            sendPacket = nullptr;
            continue;
        }
        if(len<=0){
            ls_info_debug("sendPacket->size = %d ERR should not happened ",len);
            free(sendPacket->data);
            sendPacket->data = nullptr;
            delete sendPacket;
            sendPacket = nullptr;
            continue;
        }
        if(!pDlg->QSEleDevUart[index]->isOpen()){
            free(sendPacket->data);
            sendPacket->data = nullptr;
            delete sendPacket;
            sendPacket = nullptr;
            continue;
        }
        if(len <= bufLen){
            sendLen = len;
            memcpy(uartSendBuf,p,sendLen);
            emit pDlg->mainSendEleUart(index,uartSendBuf,sendLen);
        }
        free(sendPacket->data);
        sendPacket->data = nullptr;
        delete sendPacket;
        sendPacket = nullptr;
    }
    ls_info_debug("Thread End");
    return nullptr;
}

void *sendoutTempThread(void *arg)
{
    int index = 0;
    QString str="";
    LS_S8 uartSendBuf[1024]={0};
    LS_S32 bufLen = sizeof(uartSendBuf),sendLen=0;
    LS_S8_OUT_PACKET *sendPacket = nullptr;
    QEleCtrl *pDlg  = (QEleCtrl*)arg;
    pDlg->bAllThreadExit=false;
    while(!bInitDlg){
        myMSleep(1000);
    }
    while(!pDlg->bAllThreadExit)
    {
        sendPacket = tempOutQue.PopPacket();
        if(sendPacket==nullptr){
            myMSleep(50);
            continue;
        }
        LS_S8 *p = sendPacket->data;
        LS_S32 len = sendPacket->size;
        index = sendPacket->devid;
        if(!p){
            ls_info_debug("sendPacket->data = NULL ERR should not happened");
            delete sendPacket;
            sendPacket = nullptr;
            continue;
        }
        if(index != Ele_DevID_0 && index != Ele_DevID_1){
            ls_info_debug("sendPacket->devid = %d ERR should not happened",index);
            free(sendPacket->data);
            sendPacket->data = nullptr;
            delete sendPacket;
            sendPacket = nullptr;
            continue;
        }
        if(len<=0){
            ls_info_debug("sendPacket->size = %d ERR should not happened ",len);
            free(sendPacket->data);
            sendPacket->data = nullptr;
            delete sendPacket;
            sendPacket = nullptr;
            continue;
        }
        if(!pDlg->QSEleTempUart[index]->isOpen()){
            free(sendPacket->data);
            sendPacket->data = nullptr;
            delete sendPacket;
            sendPacket = nullptr;
            continue;
        }
        if(len <= bufLen){
            sendLen = len;
            memcpy(uartSendBuf,p,sendLen);
            emit pDlg->mainSendTempUart(index,uartSendBuf,sendLen);
        }
        free(sendPacket->data);
        sendPacket->data = nullptr;
        delete sendPacket;
        sendPacket = nullptr;
    }
    ls_info_debug("Thread End");
    return nullptr;
}

QEleCtrl::QEleCtrl(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QEleCtrl)
{
    bInitDlg = false;
    ui->setupUi(this);
    initMyDlg();
    bInitDlg = true;
}

QEleCtrl::~QEleCtrl()
{
    ls_info_debug("");
    delete ui;
}

void QEleCtrl::closeEvent(QCloseEvent *e)
{
    LS_S32 ret = 0;
    //ls_info_debug("system eixt release Res ...");
    stopAllThreads();
    for(int i = Ele_DevID_0;i<ELE_MAX_NUM;i++){
        if(mSave[i]){
            delete mSave[i];
            mSave[i] = nullptr;
        }

        if(commQue[i]){
            delete commQue[i];
            commQue[i] = nullptr;
        }
        if(tempQue[i]){
            delete tempQue[i];
            tempQue[i] = nullptr;
        }

        if(eleQue[i]){
            delete eleQue[i];
            eleQue[i] = nullptr;
        }
    }

    if(gpsQue){
        delete gpsQue;
        gpsQue = nullptr;
    }

    QDlgRect[DLG_ele_ctr] = this->geometry();
    QDlgRect[Dlg_ele_sys_set] = dSysSet->geometry();
    QDlgRect[Dlg_ele_sys_ctrl] = dEleSysCtrl->geometry();
    QDlgRect[Dlg_ele_switch] = dEleSwitch->geometry();
    QDlgRect[Dlg_ele_chart] = dEleChart->geometry();
    QDlgRect[Dlg_ele_info] = dEleInfo->geometry();
    for(int i=0;i<=Dlg_ele_info;i++){
        tDlgRect.eleRect[i].x=QDlgRect[i].x();
        tDlgRect.eleRect[i].y=QDlgRect[i].y();
        tDlgRect.eleRect[i].w=QDlgRect[i].width();
        tDlgRect.eleRect[i].h=QDlgRect[i].height();
    }
    operaSoftRect(SET_OPERA,&tDlgRect);

    ret = operaSoftTime(SET_OPERA,&iGlobalSoftRunTime);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSoftTime ERR\n");
    }

    ret = operaSaveDataCount(Ele_DevID_0,SET_OPERA,&iGlobalSaveCount[Ele_DevID_0]);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSaveDataCount 0 ERR\n");
    }

    ret = operaCheckCount(Ele_DevID_0,SET_OPERA,&iGlobalCheckCount[Ele_DevID_0]);
    if(ret !=LS_SUCC){
        ls_err_debug("operacheckCount 0 ERR\n");
    }

    ret = operaSaveDataCount(Ele_DevID_1,SET_OPERA,&iGlobalSaveCount[Ele_DevID_1]);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSaveDataCount 1 ERR\n");
    }

    ret = operaCheckCount(Ele_DevID_1,SET_OPERA,&iGlobalCheckCount[Ele_DevID_1]);
    if(ret !=LS_SUCC){
        ls_err_debug("operacheckCount 1 ERR\n");
    }
    if(dSysSet){
        dSysSet->hide();
        delete dSysSet;
        dSysSet = nullptr;
    }

    if(dEleSysCtrl){
        dEleSysCtrl->hide();
        delete dEleSysCtrl;
        dEleSysCtrl = nullptr;
    }
    if(dEleSwitch){
        dEleSwitch->hide();
        delete dEleSwitch;
        dEleSwitch = nullptr;
    }
    if(dEleSysCtrl){
        dEleChart->hide();
        dEleChart->destroyDlg();
        delete dEleChart;
        dEleChart = nullptr;
    }

    if(dEleSysCtrl){
        dEleInfo->hide();
        dEleInfo->destroyDlg();
        delete dEleInfo;
        dEleInfo = nullptr;
    }

    if(dTempCtrl){
        dTempCtrl->hide();
        delete dTempCtrl;
        dTempCtrl = nullptr;
    }
}
/*
 * @brief QEleCtrl::initMyDlg
 * 系统入口初始化
 */
void QEleCtrl::initMyDlg()
{
    createRightMenu();
    initVariables();
    initChart();
    initChildWidgets();
    initCtrlShow();
    initQSerials();
    initThreads();
    showMsg("软件准备就绪",MSG_INFO);
    setWidgetTheme();
}

/*
 * @brief QEleCtrl::setWidgetTheme
 * 设置显示主题
 */
void QEleCtrl::setWidgetTheme()
{
    QFile qss(":/theme/black-theme");
    if(!qss.open(QFile::ReadOnly)){
        ls_err_debug("open theme qss ERR\n");
        return;
    }
    this->setStyleSheet(qss.readAll());
    qss.close();
    update();
}

/*
 * @brief QEleCtrl::initVariables
 * 初始化各种变量
 */
void QEleCtrl::initVariables()
{
    LS_S32 ret = 0;
    QString str = "";
    bool bOrgSave=false;
    memset(cGlobalSaveDir,0,sizeof(cGlobalSaveDir));
    ret =operaSavePath(GET_OPERA,cGlobalSaveDir);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSavePath ERR\n");
        bOrgSave = true;
        showMsg("获取数据存储位置失败，默认位置存储",MSG_ERR);
    }else{
        //ls_info_debug("*%s*",cGlobalSaveDir);
        if(strlen(cGlobalSaveDir)<=0){
            ls_err_debug("operaSavePath ERR\n");
            bOrgSave = true;
            showMsg("初次运行，默认位置存储",MSG_ERR);
        }
    }
    if(bOrgSave){
        memset(cGlobalSaveDir,0,sizeof(cGlobalSaveDir));
        str = QApplication::applicationDirPath();
        str+="/data/";
        QString2Char(str,cGlobalSaveDir);
        ls_info_debug("%s",cGlobalSaveDir);
        ret =operaSavePath(SET_OPERA,cGlobalSaveDir);
        if(ret !=LS_SUCC){
            ls_err_debug("set operaSavePath ERR\n");
            bOrgSave = true;
            showMsg("存储位置写入配置失败",MSG_ERR);
        }
    }
    ret = operaSysArg(GET_OPERA,&tGlobalSysSet);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSysArg ERR\n");
        showMsg("读取配置文件失败，使用默认配置",MSG_ERR);
    }

    ret = operaSoftTime(GET_OPERA,&iGlobalSoftRunTime);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSoftTime ERR\n");
    }

    ret = operaSaveDataCount(Ele_DevID_0,GET_OPERA,&iGlobalSaveCount[Ele_DevID_0]);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSaveDataCount  %d ERR\n",Ele_DevID_0);
    }

    ret = operaCheckCount(Ele_DevID_0,GET_OPERA,&iGlobalCheckCount[Ele_DevID_0]);
    if(ret !=LS_SUCC){
        ls_err_debug("operacheckCount %d ERR\n",Ele_DevID_0);
    }

    ret = operaSaveDataCount(Ele_DevID_1,GET_OPERA,&iGlobalSaveCount[Ele_DevID_1]);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSaveDataCount %d ERR\n",Ele_DevID_1);
    }

    ret = operaCheckCount(Ele_DevID_1,GET_OPERA,&iGlobalCheckCount[Ele_DevID_1]);
    if(ret !=LS_SUCC){
        ls_err_debug("operacheckCount %d ERR\n",Ele_DevID_1);
    }

    mSave[Ele_DevID_0] = new saveData(Ele_DevID_0);
    mSave[Ele_DevID_1] = new saveData(Ele_DevID_1);
    mSave[Ele_DevID_0]->setSavePath(cGlobalSaveDir);
    mSave[Ele_DevID_1]->setSavePath(cGlobalSaveDir);

    for(int i=Ele_DevID_0;i<ELE_MAX_NUM;i++){
        commQue[i] = new DataPtrBuffer();
        dataQue[i] = new DataPtrBuffer();
        tempQue[i] = new DataPtrBuffer();
        tempQue[i] = new DataPtrBuffer();
        eleQue[i] = new DataPtrBuffer();
    }
    gpsQue = new DataPtrBuffer();

    countTimer = new QTimer(this);
    countTimer->start(1000);
    connect(countTimer,SIGNAL(timeout()),this,SLOT(countTimerDeal()));

    checkTimer = new QTimer(this);
    checkTimer->start(5000);
    connect(checkTimer,SIGNAL(timeout()),this,SLOT(checkDevOnline()));

    bAllThreadExit=false;
    chartMaxValue[Ele_DevID_0]=0;
    chartMaxValue[Ele_DevID_1]=0;
    chartMinValue[Ele_DevID_0]=0;
    chartMinValue[Ele_DevID_1]=0;
    connect(this,SIGNAL(mainShowMsg(QString,MsgType)),this,SLOT(showMsg(QString,MsgType)),Qt::DirectConnection);

    connect(this,SIGNAL(mainSendTempUart(int,LS_S8*,int)),this,SLOT(slotSendTempUart(int,LS_S8*,int)));

    connect(this,SIGNAL(mainSendEleUart(int,LS_S8*,int)),this,SLOT(slotSendEleUart(int,LS_S8*,int)));
}

/*
 * @brief QEleCtrl::initCtrlShow
 * 初始化控件显示
 */
void QEleCtrl::initCtrlShow()
{
    int screen_w=0,screen_h=0;
    QDesktopWidget* desktopWidget = QApplication::desktop();
    QRect deskRect = desktopWidget->availableGeometry();
    screen_w = deskRect.width();
    screen_h = deskRect.height();
    QPalette palette(this->palette());
    palette.setColor(QPalette::Background,QColor(DLG_BK_COLOR));
    this->setPalette(palette);
    ui->RB_ELE_0->setChecked(true);
    ui->LA_SOFT_RUN->setStyleSheet(LABEL_STYLE);
    ui->LA_saveCount_0->setStyleSheet(LABEL_STYLE);
    ui->LA_checkCount_0->setStyleSheet(LABEL_STYLE);
    ui->LA_saveCount_1->setStyleSheet(LABEL_STYLE);
    ui->LA_checkCount_1->setStyleSheet(LABEL_STYLE);
    ui->LA_BD_LA->setStyleSheet(LABEL_STYLE);
    ui->LA_BD_LO->setStyleSheet(LABEL_STYLE);
    ui->TE_SYS_MSG->setTextColor(MSG_INFO_COLOR);
    ui->TE_SYS_MSG->setAttribute(Qt::WA_TranslucentBackground, true);
    ui->TE_SYS_MSG->setStyleSheet("background: black;font: 10pt \"华文楷体\";");
    ui->LA_BD_LA->setText("北纬00.000000°");
    ui->LA_BD_LO->setText("东经000.000000°");
    ui->GV_CHART_0->setPalette(palette);
    ui->GV_CHART_1->setPalette(palette);
    ui->LA_dev0_online_light->setAlarm(Alarm_gray);
    ui->LA_dev1_online_light->setAlarm(Alarm_gray);
    ui->LA_temp0_online_light->setAlarm(Alarm_gray);
    ui->LA_temp1_online_light->setAlarm(Alarm_gray);
    updateRunTimerShow();

    operaSoftRect(GET_OPERA,&tDlgRect);
    int x=0,y=0,w=0,h=0;
    x=tDlgRect.eleRect[DLG_ele_ctr].x;
    y=tDlgRect.eleRect[DLG_ele_ctr].y;
    w=tDlgRect.eleRect[DLG_ele_ctr].w;
    h=tDlgRect.eleRect[DLG_ele_ctr].h;
    if((x+w)<screen_w&&(y+h)<screen_h){
        this->setGeometry(x,y,w,h);
    }
#if 1
    x=tDlgRect.eleRect[Dlg_ele_sys_set].x;
    y=tDlgRect.eleRect[Dlg_ele_sys_set].y;
    w=tDlgRect.eleRect[Dlg_ele_sys_set].w;
    h=tDlgRect.eleRect[Dlg_ele_sys_set].h;
    if((x!=0||y!=0)&&(x+w)<screen_w&&(y+h)<screen_h){
        dSysSet->setGeometry(x,y,w,h);
    }

    x=tDlgRect.eleRect[Dlg_ele_sys_ctrl].x;
    y=tDlgRect.eleRect[Dlg_ele_sys_ctrl].y;
    w=tDlgRect.eleRect[Dlg_ele_sys_ctrl].w;
    h=tDlgRect.eleRect[Dlg_ele_sys_ctrl].h;
    if((x!=0||y!=0)&&(x<screen_w&&y<screen_h)){
        dEleSysCtrl->setGeometry(x,y,w,h);
    }

    x=tDlgRect.eleRect[Dlg_ele_switch].x;
    y=tDlgRect.eleRect[Dlg_ele_switch].y;
    w=tDlgRect.eleRect[Dlg_ele_switch].w;
    h=tDlgRect.eleRect[Dlg_ele_switch].h;
    if((x!=0||y!=0)&&(x)<screen_w&&(y)<screen_h){
        dEleSwitch->setGeometry(x,y,w,h);
    }

    x=tDlgRect.eleRect[Dlg_ele_chart].x;
    y=tDlgRect.eleRect[Dlg_ele_chart].y;
    w=tDlgRect.eleRect[Dlg_ele_chart].w;
    h=tDlgRect.eleRect[Dlg_ele_chart].h;
    if((x!=0||y!=0)&&(x)<screen_w&&(y)<screen_h){
        dEleChart->setGeometry(x,y,w,h);
    }

    x=tDlgRect.eleRect[Dlg_ele_info].x;
    y=tDlgRect.eleRect[Dlg_ele_info].y;
    w=tDlgRect.eleRect[Dlg_ele_info].w;
    h=tDlgRect.eleRect[Dlg_ele_info].h;
    if((x!=0||y!=0)&&(x+10)<screen_w&&(y+10)<screen_h){
        dEleInfo->setGeometry(x,y,w,h);
    }
#endif
}

/*
 * @brief QEleCtrl::initSignalConnect
 * 初始化qt信号与槽连接
 */
void QEleCtrl::initChildWidgets()
{
    dSysSet = new QSysSetDlg(this);
    dSysSet->setSysSetDlg(tGlobalSysSet);
    dSysSet->setSysSetSavePath(cGlobalSaveDir);
    dSysSet->hide();
    connect(dSysSet,SIGNAL(signalSysSetRet(TSysSet *,QString)),this,SLOT(slotRecvSysSetRet(TSysSet *,QString)));
    connect(dSysSet,SIGNAL(signalSysSetMsg(QString,MsgType)),this,SLOT(showMsg(QString,MsgType)));

    dEleSysCtrl = new QEleSysCtrlDlg(this);
    dEleSysCtrl->hide();
    dEleSysCtrl->setGlobalSysPara(&tGlobalSysSet);
    connect(dEleSysCtrl,SIGNAL(signalEleSysCtrlMsg(QString,MsgType)),this,SLOT(showMsg(QString,MsgType)));

    dEleSwitch = new QEleSwitchDlg(this);
    dEleSwitch->hide();
    dEleSwitch->setGlobalSysPara(&tGlobalSysSet);
    connect(dEleSwitch,SIGNAL(signalEleSwitchMsg(QString,MsgType)),this,SLOT(showMsg(QString,MsgType)));

    dEleChart = new QEleChartDlg(this);
    dEleChart->hide();
    connect(dEleChart,SIGNAL(signalEleChartMsg(QString,MsgType)),this,SLOT(showMsg(QString,MsgType)));

    dEleInfo = new QEleInfoDlg(this);
    dEleInfo->hide();
    connect(this,SIGNAL(mainShowStatus(int ,LS_U8 *)),dEleInfo,SLOT(showStatus(int ,LS_U8 *)));
    connect(this,SIGNAL(mainShowPara(int ,LS_U8 *)),dEleInfo,SLOT(showPara(int ,LS_U8 *)));
    connect(this,SIGNAL(mainShowReserve(int ,float *,int)),dEleInfo,SLOT(showReserve(int ,float *,int)));

    dTempCtrl = new QTempCtrlDlg(this);
    dTempCtrl->hide();
    connect(dTempCtrl,SIGNAL(signalTempMsg(QString,MsgType)),this,SLOT(showMsg(QString,MsgType)));
}

void QEleCtrl::stopAllThreads()
{
    bAllThreadExit = true;
    pthread_join(pRecvGpsTid,nullptr);
    pthread_join(pSendoutTid,nullptr);
    pthread_join(pSendTempTid,nullptr);
    for(int i=0;i<ELE_MAX_NUM;i++){
        pthread_join(pRecvEleDevTid[i],nullptr);
        pthread_join(pAnaComRecvTid[i],nullptr);
        pthread_join(pAnaDataRecvTid[i],nullptr);
        pthread_join(pRecvTempTid[i],nullptr);
    }
}

void QEleCtrl::sendTempCmd(Ele_Ctrl_Dev dev,void *tmp,int len)
{
    LS_S8_OUT_PACKET *requeTempPacket = new LS_S8_OUT_PACKET;
    requeTempPacket->data = (LS_S8*)tmp;
    requeTempPacket->size = len;
    if(dev==Ele_DevID_All){
        requeTempPacket->devid = Ele_DevID_0;
        tempOutQue.PushPacket(requeTempPacket);
        //myMSleep(100);
        requeTempPacket->devid = Ele_DevID_1;
        tempOutQue.PushPacket(requeTempPacket);
    }else{
        requeTempPacket->devid = dev;
        tempOutQue.PushPacket(requeTempPacket);
    }
    delete requeTempPacket;
    requeTempPacket = nullptr;
}

void QEleCtrl::slotSendTempUart(int index,LS_S8 *p,int len)
{
    if(index<Ele_DevID_0||index>=ELE_MAX_NUM)return;
    QSEleTempUart[index]->write(p,len);
}

void QEleCtrl::slotSendEleUart(int index,LS_S8 *p,int len)
{
    if(index<Ele_DevID_0||index>=ELE_MAX_NUM)return;
    QSEleDevUart[index]->write(p,len);
}

void QEleCtrl::showMsg(QString str,MsgType type)
{
    showMsgMtx.lock();
    QDateTime current_date_time = QDateTime::currentDateTime();
    QString current_time = current_date_time.toString("hh:mm:ss");
    if(type == MSG_ERR){
        ui->TE_SYS_MSG->setTextColor(MSG_ERROR_COLOR);
    }else{
        ui->TE_SYS_MSG->setTextColor(MSG_INFO_COLOR);
    }
    ui->TE_SYS_MSG->append(current_time+"\n"+str);
    QTextCursor cursor=ui->TE_SYS_MSG->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->TE_SYS_MSG->setTextCursor(cursor);
    showMsgMtx.unlock();
}

/*
 * @brief QEleCtrl::updateRunTimerShow
 * 更新界面下方计数显示
 */
void QEleCtrl::updateRunTimerShow()
{
    LS_S32 hours=0,minutes=0,seconds;
    QString str="";
    char buf[1024]={0};
    memset(buf,0,sizeof(buf));
    getTimeFromNum(iGlobalSoftRunTime,&hours,&minutes,&seconds);
    sprintf(buf,"%.4d:%.2d:%.2d",hours,minutes,seconds);
    str = QString("软件累计运行 %1").arg(buf);
    ui->LA_SOFT_RUN->setText(str);

    memset(buf,0,sizeof(buf));
    sprintf(buf,"%.6d",iGlobalSaveCount[0]);
    str = QString("设备1存储次数 %1").arg(buf);
    ui->LA_saveCount_0->setText(str);

    memset(buf,0,sizeof(buf));
    sprintf(buf,"%.6d",iGlobalCheckCount[0]);
    str = QString("设备1检测 %1").arg(buf);
    ui->LA_checkCount_0->setText(str);

    memset(buf,0,sizeof(buf));
    sprintf(buf,"%.6d",iGlobalSaveCount[1]);
    str = QString("设备2存储次数 %1").arg(buf);
    ui->LA_saveCount_1->setText(str);

    memset(buf,0,sizeof(buf));
    sprintf(buf,"%.6d",iGlobalCheckCount[1]);
    str = QString("设备2检测 %1").arg(buf);
    ui->LA_checkCount_1->setText(str);
}

/*
 * @brief QEleCtrl::initChart
 * 曲线图划线
 */
void QEleCtrl::drawLine(int index,float value)
{
    int queSize = 0;
    bool bRangeChanged = false;
    if(index <0 || index >= ELE_MAX_NUM)return;
    QDateTime bjtime=QDateTime::currentDateTime();
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    chart[index]->axisX()->setMin(QDateTime::currentDateTime().addSecs(-60*2));
    chart[index]->axisX()->setMax(QDateTime::currentDateTime().addSecs(0));
    /********************** x轴自适应数值变化 *************/
    queSize = chartQue[index].size();
    if(queSize==0){
        if(value<CONCENT_MIN_VLUAE){
            chart[index]->axisY()->setMin(value);
        }else{
            int iValue = (int)value;
            chart[index]->axisY()->setMax(iValue+1);
            chart[index]->axisY()->setMin(iValue);
        }
        chartQue[index].push_front(value);
    }else if(queSize==1){
        float qValue = chartQue[index].head();
        if(qValue < value){
            bRangeChanged = true;
            chartQue[index].push_back(value);
        }
        if(qValue > value){
            bRangeChanged = true;
            chartQue[index].push_front(value);
        }
    }else if(queSize==2){
        float qValue1 = chartQue[index].head();
        float qValue2 = chartQue[index].last();
        if(value<qValue1){
            bRangeChanged = true;
            qValue1 = value;
        }
        if(value>qValue2){
            bRangeChanged = true;
            qValue2 = value;
        }
        if(bRangeChanged){
            chartQue[index].clear();
            chartQue[index].push_front(qValue1);
            chartQue[index].push_back(qValue2);
        }
    }else{
        chartQue[index].clear();
    }
    if(bRangeChanged){
        float fHead = chartQue[index].head();
        float fLast = chartQue[index].last();
        int iHead = (int)fHead;
        int iLast = (int)fLast;
        //ls_info_debug("%.2f %.2f %d %d",fHead,fLast,iHead,iLast);
        if((fHead-iHead*1.0-0.5)>=0){
            chartMinValue[index] = (float)iHead+0.5;
        }else{
            chartMinValue[index] = (float)iHead;

        }
        if((iLast*1.0-fLast-0.5)<0){
            chartMaxValue[index] = (float)iLast+0.5;
        }else{
            chartMaxValue[index] = (float)iLast+1;
        }
        if((chartMaxValue[index] - chartMinValue[index])<0.5){
            chartMaxValue[index] = chartMinValue[index]+0.5;
        }
        //ls_info_debug("%.2f %.2f ",chartMinValue[index],chartMaxValue[index]);
        chart[index]->axisY()->setMin(chartMinValue[index]);
        chart[index]->axisY()->setMax(chartMaxValue[index]);
    }
    /********************** x轴自适应数值变化 end ******************/
    //当曲线上最早的点超出X轴的范围时，剔除最早的点，
    if(series[index]->count()>119)
    {
        series[index]->removePoints(0,series[index]->count()-119);
    }

    series[index]->append(bjtime.toMSecsSinceEpoch(),value);    //增加新的点到曲线末端
}

/*
 * @brief QEleCtrl::initChart
 * 初始化曲线图控件
 */
void QEleCtrl::initChart()
{
    QString str="";
    for(int i=0;i<ELE_MAX_NUM;i++){
        chart[i]=new QChart();
        chart[i]->setTheme(QChart::ChartThemeQt);//设置系统主题
        str = QString("浓度值%1-曲线图").arg(i+1);
        chart[i]->setTitle(str);
        chart[i]->setTitleFont(QFont("微软雅黑",10));
        chart[i]->legend()->hide();

        series[i]=new QSplineSeries();
        series[i]->setUseOpenGL(true);//openGl 加速
        series[i]->setName("series1");//设置曲线的名称
        chart[i]->addSeries(series[i]);//把曲线添加到QChart的实例chart中
        QDateTimeAxis *axisX=new QDateTimeAxis();//声明并初始化X轴、两个Y轴
        axisX->setTickCount(12);//设置X坐标轴上的格点
        axisX->setFormat("hh:mm:ss"); //设置x轴显示格式
        axisX->setTitleText("时间");//设置坐标轴显示的名称
        QValueAxis *axisY=new QValueAxis();
        //设置坐标轴显示的范围
        axisX->setMin(QDateTime::currentDateTime().addSecs(-60*2));//设置X轴显示区间120s
        axisX->setMax(QDateTime::currentDateTime().addSecs(0));

        axisY->setRange(CONCENT_MIN_VLUAE,CONCENT_MAX_VLUAE);
        axisY->setLabelFormat("%.3f"); //设置x轴显示格式
        //axisY->setMin(CONCENT_MIN_VLUAE);
        //axisY->setMax(CONCENT_MAX_VLUAE);
        axisY->setTickCount(9);//设置Y坐标轴上的格点
        axisY->setTitleText("浓度值");//设置坐标轴显示的名称
        axisY->setLinePenColor(QColor(Qt::darkBlue));//设置坐标轴的颜色，粗细，设置网格不显示
        axisY->setLinePenColor(QColor(Qt::darkGreen));
        //axisY->setGridLineVisible(false);
        QPen penY(Qt::darkBlue,3,Qt::SolidLine,Qt::RoundCap,Qt::RoundJoin);
        axisY->setLinePen(penY);
        //把坐标轴添加到chart中，
        //addAxis函数的第二个参数是设置坐标轴的位置，
        //只有四个选项，下方：Qt::AlignBottom，左边：Qt::AlignLeft，右边：Qt::AlignRight，上方：Qt::AlignTop
        chart[i]->addAxis(axisX,Qt::AlignBottom);
        chart[i]->addAxis(axisY,Qt::AlignLeft);
        //把曲线关联到坐标轴
        QPen penLine(Qt::darkYellow,ELE_CURVE_WIDTH,Qt::SolidLine,Qt::RoundCap,Qt::RoundJoin);
        series[i]->setPen(penLine);
        series[i]->attachAxis(axisX);
        series[i]->attachAxis(axisY);
    }
    //把chart显示到窗口上
    ui->GV_CHART_0->setChart(chart[0]);
    ui->GV_CHART_1->setChart(chart[1]);
    ui->GV_CHART_0->setRenderHint(QPainter::Antialiasing);
    ui->GV_CHART_1->setRenderHint(QPainter::Antialiasing);
//    ui->GV_CHART_0->setTransformationAnchor(QChart::SeriesAnimations);
//    ui->GV_CHART_1->setTransformationAnchor(QChart::SeriesAnimations);
}

void QEleCtrl::createRightMenu()
{
    pRightMenu = new QMenu(this);//pMenu 为类成员变量
    QAction *pAction0 = pRightMenu->addAction("软件设置");
    QAction *pAction1 = pRightMenu->addAction("采样曲线图显示");
    QAction *pAction2 = pRightMenu->addAction("电控系统控制");
    QAction *pAction3 = pRightMenu->addAction("电机和电磁阀控制");
    QAction *pAction4 = pRightMenu->addAction("温度控制");
    QAction *pAction5 = pRightMenu->addAction("参数和状态显示");
    QAction *pAction6 = pRightMenu->addAction("窗口展开");
    QAction *pAction7 = pRightMenu->addAction("窗口折叠");
    QAction *pAction8 = pRightMenu->addAction("计数复位");
    QAction *pAction9 = pRightMenu->addAction("日志清空");
    //pAction->setIcon(QIcon(":/new/prefix1/forbidPNG"));//设置图标
    connect(pAction0,SIGNAL(triggered()),this,SLOT(slotSysSet()));
    connect(pAction1,SIGNAL(triggered()),this,SLOT(slotEleChartShow()));
    connect(pAction2,SIGNAL(triggered()),this,SLOT(slotEleCtrl()));
    connect(pAction3,SIGNAL(triggered()),this,SLOT(slotSwithCtrl()));
    connect(pAction4,SIGNAL(triggered()),this,SLOT(slotTempCtrl()));
    connect(pAction5,SIGNAL(triggered()),this,SLOT(slotEleArgStatusShow()));
    connect(pAction6,SIGNAL(triggered()),this,SLOT(slotChildUnfold()));
    connect(pAction7,SIGNAL(triggered()),this,SLOT(slotChildFold()));
    connect(pAction8,SIGNAL(triggered()),this,SLOT(slotCountReset()));
    connect(pAction9,SIGNAL(triggered()),this,SLOT(slotMsgReset()));
    //connect(pRightMenu,SIGNAL(triggered(QAction*)))
}

void QEleCtrl::initThreads()
{
    TthreadArg pArg;
    pArg.pDlg=this;
    for(int i = Ele_DevID_0;i<ELE_MAX_NUM;i++){
        pArg.index = i;
        pthread_create(&pRecvEleDevTid[i],nullptr,recvEleThread,&pArg);
        myMSleep(100);
        pthread_create(&pRecvTempTid[i],nullptr,recvTempThread,&pArg);
        myMSleep(100);
        pthread_create(&pAnaComRecvTid[i],nullptr,analyCommMsg,&pArg);
        myMSleep(100);
        pthread_create(&pAnaDataRecvTid[i],nullptr,analyDataMsg,&pArg);
        myMSleep(100);
    }
    pthread_create(&pRecvGpsTid,nullptr,recvGpsThread,this);
    pthread_create(&pSendoutTid,nullptr,sendoutEleThread,this);
    pthread_create(&pSendTempTid,nullptr,sendoutTempThread,this);
}

void QEleCtrl::initQSerials()
{
    QString str="";
    QString comName="";
    QSGpsUart = new QSerialPort(this);
    QSGpsUart->setBaudRate(QSerialPort::Baud9600);
    QSGpsUart->setParity(QSerialPort::NoParity);
    QSGpsUart->setDataBits(QSerialPort::Data8);
    QSGpsUart->setStopBits(QSerialPort::OneStop);
    QSGpsUart->setFlowControl(QSerialPort::NoFlowControl);
    comName = QString("COM%1").arg(tGlobalSysSet.gpsPort);
    QSGpsUart->setPortName(comName);
    if(!QSGpsUart->open(QIODevice::ReadWrite)){
        str = QString("打开北斗端口COM%1 失败！").arg(tGlobalSysSet.gpsPort);
        showMsg(str);
    }
    for(int i=Ele_DevID_0;i<ELE_MAX_NUM;i++){
        QSEleDevUart[i] = new QSerialPort(this);
        QSEleTempUart[i] = new QSerialPort(this);
        openEleUart((Ele_Ctrl_Dev)i,tGlobalSysSet.eleDevPort[i]);
        openTempUart((Ele_Ctrl_Dev)i,tGlobalSysSet.eleTempPort[i]);
    }

    connect(QSGpsUart, SIGNAL(readyRead()), this, SLOT(readGpsUart()));
    connect(QSEleDevUart[0], SIGNAL(readyRead()), this, SLOT(readEleUart_0()));
    connect(QSEleDevUart[1], SIGNAL(readyRead()), this, SLOT(readEleUart_1()));
    connect(QSEleTempUart[0], SIGNAL(readyRead()), this, SLOT(readTempUart_0()));
    connect(QSEleTempUart[1], SIGNAL(readyRead()), this, SLOT(readTempUart_1()));

    gpsAskTimer = new QTimer(this);
    gpsAskTimer->start(1000);
    connect(gpsAskTimer,SIGNAL(timeout()),this,SLOT(sendGpsAskCmd()));

    eleHeartTimer = new QTimer(this);
    eleHeartTimer->start(1000);
    connect(eleHeartTimer,SIGNAL(timeout()),this,SLOT(sendEleHeart()));


    tempHeartTimer = new QTimer(this);
    tempHeartTimer->start(200);
    connect(tempHeartTimer,SIGNAL(timeout()),this,SLOT(sendTempHeart()));

}

void QEleCtrl::openEleUart(Ele_Ctrl_Dev id ,int port)
{
    QString comName="",str="";
    comName = QString("COM%1").arg(port);
    QSEleDevUart[id]->setPortName(comName);
    if(!QSEleDevUart[id]->open(QIODevice::ReadWrite)){
        str = QString("打开设备%1 电控端口COM%2 失败！").arg(id+1).arg(port);
        showMsg(str);
        return;
    }else{
        str = QString("打开设备%1 电控端口COM%2 成功").arg(id+1).arg(port);
        showMsg(str,MSG_INFO);
    }

    QSEleDevUart[id]->setBaudRate(QSerialPort::Baud115200);
    QSEleDevUart[id]->setParity(QSerialPort::NoParity);
    QSEleDevUart[id]->setDataBits(QSerialPort::Data8);
    QSEleDevUart[id]->setStopBits(QSerialPort::OneStop);
    QSEleDevUart[id]->setFlowControl(QSerialPort::NoFlowControl);
    QSEleDevUart[id]->setReadBufferSize(8192);
}

void QEleCtrl::openTempUart(Ele_Ctrl_Dev id ,int port)
{
    QString comName="",str="";
    comName = QString("COM%1").arg(port);
    QSEleTempUart[id]->setPortName(comName);

    if(!QSEleTempUart[id]->open(QIODevice::ReadWrite)){
        str = QString("打开设备%1 温控端口COM%2 失败！").arg(id+1).arg(port);
        showMsg(str);
        return;
    }else{
        str = QString("打开设备%1 温控端口COM%2 成功").arg(id+1).arg(port);
        showMsg(str,MSG_INFO);
    }
    QSEleTempUart[id]->setBaudRate(QSerialPort::Baud57600);
    QSEleTempUart[id]->setParity(QSerialPort::NoParity);
    QSEleTempUart[id]->setDataBits(QSerialPort::Data8);
    QSEleTempUart[id]->setStopBits(QSerialPort::OneStop);
    QSEleTempUart[id]->setFlowControl(QSerialPort::NoFlowControl);
}

void QEleCtrl::contextMenuEvent(QContextMenuEvent *event)
{
    pRightMenu->popup(event->globalPos());
}

/*
 * @brief QEleCtrl::countTimerDeal
 * 计数信号timer的槽函数，累加并显示运行时间计数
 */
void QEleCtrl::countTimerDeal()
{
    iGlobalSoftRunTime++;
    updateRunTimerShow();
}

void QEleCtrl::checkDevOnline()
{
    if(iGlobalDevRecvDataCount[Ele_DevID_0]>0){
        ui->LA_dev0_online_light->setAlarm(Alarm_green);
        ui->LA_dev0_online->setText("设备1在线");
        iGlobalDevRecvDataCount[Ele_DevID_0]=0;
    }else{
        ui->LA_dev0_online_light->setAlarm(Alarm_gray);
        ui->LA_dev0_online->setText("设备1离线");
    }

    if(iGlobalDevRecvDataCount[Ele_DevID_1]>0){
        ui->LA_dev1_online_light->setAlarm(Alarm_green);
        ui->LA_dev1_online->setText("设备2在线");
        iGlobalDevRecvDataCount[Ele_DevID_1]=0;
    }else{
        ui->LA_dev1_online_light->setAlarm(Alarm_gray);
        ui->LA_dev1_online->setText("设备2离线");
    }
}

void QEleCtrl::sendEleHeart()
{
    LS_S32 len = 0;
    LS_U8 sendBuf[64]={0};
    EleSysPara index = Ele_request_para;
    LS_S8_OUT_PACKET *requeNetPacket = nullptr;
    requeNetPacket = new LS_S8_OUT_PACKET;
    if(requeNetPacket==nullptr)return;
    len = sendOrder(index,sendBuf);
    if(len <= 0){
        ls_err_debug("ERR");
        return;
    }
    requeNetPacket->data = (LS_S8 *)sendBuf;
    requeNetPacket->size = len;
    requeNetPacket->devid = Ele_DevID_0;
    eleOutQue.PushPacket(requeNetPacket);
    requeNetPacket->devid = Ele_DevID_1;
    eleOutQue.PushPacket(requeNetPacket);
    free(requeNetPacket);
    requeNetPacket=nullptr;
}

void QEleCtrl::sendTempHeart()
{
    LS_S8 sendBuf[64]={0};
    LS_S32 sendLen = 0;
    static int iTempCount = 0;
    memset(sendBuf,0,sizeof(sendBuf));
    if(iTempCount%5==0){
        sendLen = getTempActualValue(sendBuf);
    }else if(iTempCount%5==1){
        sendLen = getTempOutputModel(sendBuf);
    }else if(iTempCount%5==2){
        sendLen = getTempOutVolValue(sendBuf);
    }else if(iTempCount%5==3){
        sendLen = getTempOutCurValue(sendBuf);
    }else if(iTempCount%5==4){
        sendLen = getTemProRet(sendBuf);
    }
    iTempCount++;
    sendTempCmd(Ele_DevID_All,sendBuf,sendLen);
}

void QEleCtrl::sendGpsAskCmd()
{
    if(!QSGpsUart->isOpen())return;
    LS_S8  sendGpsBuf[32]={0};
    LS_S32 len = 0;
    LS_S32 ret = 0;
    /*01 03 00 05 00 23 14 12*/
    sendGpsBuf[len++]=0x01;
    sendGpsBuf[len++]=0x03;
    sendGpsBuf[len++]=0x00;
    sendGpsBuf[len++]=0x05;
    sendGpsBuf[len++]=0x00;
    sendGpsBuf[len++]=0x23;
    sendGpsBuf[len++]=0x14;
    sendGpsBuf[len++]=0x12;
    ret = QSGpsUart->write(sendGpsBuf,len);
    if(ret){
        ls_info_debug("gps send ERR %d",ret);
    }
}

void QEleCtrl::readGpsUart()
{
    QByteArray temp = QSGpsUart->readAll();
    LS_S8_DATA_PACKET *gpsData = new LS_S8_DATA_PACKET;
    gpsData->data = temp.data();
    gpsData->size = temp.length();
    gpsQue->PushPacket(gpsData);
    free(gpsData);
    gpsData = nullptr;
}

void QEleCtrl::readEleUart_0()
{
    int index = Ele_DevID_0;
    QByteArray temp = QSEleDevUart[index]->readAll();
    LS_S8_DATA_PACKET *eleData = new LS_S8_DATA_PACKET;
    eleData->data = (LS_S8 *)temp.data();
    eleData->size = temp.length();
    //ls_info_debug("%d\n",eleData->size);
    eleQue[index]->PushPacket(eleData);
    free(eleData);
    eleData = nullptr;
}

void QEleCtrl::readEleUart_1()
{
    int index = Ele_DevID_1;
    QByteArray temp = QSEleDevUart[index]->readAll();
    LS_S8_DATA_PACKET *eleData = new LS_S8_DATA_PACKET;
    eleData->data = (LS_S8 *)temp.data();
    eleData->size = temp.length();
    eleQue[index]->PushPacket(eleData);
    free(eleData);
    eleData = nullptr;
}

void QEleCtrl::readTempUart_0()
{
    int index =Ele_DevID_0;
    int size = 0;
    QByteArray temp = QSEleTempUart[index]->readAll();
    size = temp.length();
    if(size<=0)return;
    LS_S8 *tmp = (LS_S8*)malloc(sizeof(LS_S8)*size);
    if(tmp==nullptr)return;
    LS_S8_DATA_PACKET *tempData = new LS_S8_DATA_PACKET;
    if(tempData==nullptr){
        free(tmp);
        tmp=nullptr;
    }
    memcpy(tmp,temp,size);
    tempData->data = tmp;
    tempData->size = size;
    tempQue[index]->PushPacket(tempData);
    //qDebug()<<"tmp : "<<tmp;
    free(tmp);
    tmp=nullptr;
    free(tempData);
    tempData = nullptr;
}

void QEleCtrl::readTempUart_1()
{
    int index =Ele_DevID_1;
    int size = 0;
    QByteArray temp = QSEleTempUart[index]->readAll();
    size = temp.length();
    if(size<=0)return;
    LS_S8 *tmp = (LS_S8*)malloc(sizeof(LS_S8)*size);
    if(tmp==nullptr)return;
    LS_S8_DATA_PACKET *tempData = new LS_S8_DATA_PACKET;
    if(tempData==nullptr){
        free(tmp);
        tmp=nullptr;
    }
    memcpy(tmp,temp,size);
    tempData->data = tmp;
    tempData->size = size;
    tempQue[index]->PushPacket(tempData);
    qDebug()<<"tmp : "<<tmp;
    free(tmp);
    tmp=nullptr;
    free(tempData);
    tempData = nullptr;
}
/*
 * @brief QEleCtrl::slotSysSet
 * 右键弹出软件设置槽函数
 */
void QEleCtrl::slotSysSet()
{
    dSysSet->show();
}

/*
 * @brief QEleCtrl::slotSysSet
 * 右键弹出软件设置槽函数
 */
void QEleCtrl::slotEleChartShow()
{
    dEleChart->show();
}

/*
 * @brief QEleCtrl::slotEleCtrl
 * 右键弹出电控设置窗口槽函数
 */
void QEleCtrl::slotEleCtrl()
{
    dEleSysCtrl->show();
}

/*
 * @brief QEleCtrl::slotSwithCtrl
 * 右键弹出电机和电磁阀设置窗口槽函数
 */
void QEleCtrl::slotSwithCtrl()
{
    dEleSwitch->show();
}

/*
 * @brief QEleCtrl::slotTempCtrl
 * 右键弹出温控设置窗口槽函数
 */
void QEleCtrl::slotTempCtrl()
{
    dTempCtrl->show();
}

void QEleCtrl::slotEleArgStatusShow()
{
    dEleInfo->show();
}

void QEleCtrl::slotChildUnfold()
{
    dEleSysCtrl->show();
    dEleSwitch->show();
    dEleChart->show();
    dEleInfo->show();
}

void QEleCtrl::slotChildFold()
{
    dEleSysCtrl->hide();
    dEleSwitch->hide();
    dEleChart->hide();
    dEleInfo->hide();
}
/*
 * @brief QEleCtrl::slotCountReset
 * 右键计数清零槽函数
 */
void QEleCtrl::slotCountReset()
{
    LS_S32 ret = 0;
    iGlobalSoftRunTime = 0;
    iGlobalSaveCount[Ele_DevID_0] = 0;
    iGlobalCheckCount[Ele_DevID_0] = 0;
    iGlobalSaveCount[Ele_DevID_1] = 0;
    iGlobalCheckCount[Ele_DevID_1] = 0;
    ret = operaSoftTime(SET_OPERA,&iGlobalSoftRunTime);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSoftTime ERR\n");
    }
    ret = operaSaveDataCount(Ele_DevID_0,SET_OPERA,&iGlobalSaveCount[Ele_DevID_0]);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSaveDataCount ERR\n");
    }
    ret = operaCheckCount(Ele_DevID_0,SET_OPERA,&iGlobalCheckCount[Ele_DevID_0]);
    if(ret !=LS_SUCC){
        ls_err_debug("operacheckCount ERR\n");
    }

    ret = operaSaveDataCount(Ele_DevID_1,SET_OPERA,&iGlobalSaveCount[Ele_DevID_1]);
    if(ret !=LS_SUCC){
        ls_err_debug("operaSaveDataCount ERR\n");
    }
    ret = operaCheckCount(Ele_DevID_1,SET_OPERA,&iGlobalCheckCount[Ele_DevID_1]);
    if(ret !=LS_SUCC){
        ls_err_debug("operacheckCount ERR\n");
    }
    updateRunTimerShow();
}

void QEleCtrl::slotMsgReset()
{
    ui->TE_SYS_MSG->setText("");
}

/*
 * @brief QEleCtrl::slotCountReset
 * 右键软件设置槽函数
 */
void QEleCtrl::slotRecvSysSetRet(TSysSet *arg,QString pathStr)
{
    char recvPath[MAX_PATH]={0};
    QString2Char(pathStr,recvPath);
    bool bSysChanged = false;
    if(strcmp(recvPath,cGlobalSaveDir)!=0){
        memset(cGlobalSaveDir,0,sizeof(cGlobalSaveDir));
        strcpy(cGlobalSaveDir,recvPath);
        mSave[Ele_DevID_0]->setSavePath(cGlobalSaveDir);
        mSave[Ele_DevID_1]->setSavePath(cGlobalSaveDir);
        operaSavePath(SET_OPERA,cGlobalSaveDir);
        bSysChanged = true;
    }
    if(isSysArgChanged(arg,&tGlobalSysSet)){
        operaSysArg(SET_OPERA,arg);
        dEleSysCtrl->setGlobalSysPara(arg);
        dEleSwitch->setGlobalSysPara(arg);
        bSysChanged = true;
    }
//    if(bSysChanged){
//        showMsg("软件设置成功",MSG_INFO);
//    }else{
//        showMsg("设置参数无变化",MSG_INFO);
//        return;
//    }
    if(arg->gpsPort != tGlobalSysSet.gpsPort){
        tGlobalSysSet.gpsPort =arg->gpsPort;
        if(QSGpsUart->isOpen()){
            QSGpsUart->close();
        }
        QSGpsUart->setBaudRate(QSerialPort::Baud9600);
        QSGpsUart->setParity(QSerialPort::NoParity);
        QSGpsUart->setDataBits(QSerialPort::Data8);
        QSGpsUart->setStopBits(QSerialPort::OneStop);
        QSGpsUart->setFlowControl(QSerialPort::NoFlowControl);
        QString comName = QString("COM%1").arg(tGlobalSysSet.gpsPort);
        QSGpsUart->setPortName(comName);
        if(!QSGpsUart->open(QIODevice::ReadWrite)){
            QString str = QString("打开北斗端口COM%1 失败！").arg(tGlobalSysSet.gpsPort);
            showMsg(str);
        }
    }
    TthreadArg pArg;
    pArg.pDlg=this;
    for(int i = Ele_DevID_0;i<ELE_MAX_NUM;i++){
        pArg.index = i;
        if(arg->eleDevPort[i] != tGlobalSysSet.eleDevPort[i]){
            if(QSEleDevUart[i]->isOpen()){
                QSEleDevUart[i]->close();
            }
            openEleUart((Ele_Ctrl_Dev)i,arg->eleDevPort[i]);
        }
        if(arg->eleTempPort[i] != tGlobalSysSet.eleTempPort[i]){
            if(QSEleTempUart[i]->isOpen()){
                QSEleTempUart[i]->close();
            }
            openTempUart((Ele_Ctrl_Dev)i,arg->eleTempPort[i]);
        }
    }

    memcpy(&tGlobalSysSet,arg,sizeof(TSysSet));
    showMsg("软件设置成功",MSG_INFO);
}

void QEleCtrl::handleRecvGpsBuf(void *p, LS_S32 len)
{
    GPS_INFO mInfo;
    QString str = "";
    LS_S8 *buf = (LS_S8 *)p;
    char degreeBuf[128]={0};
    //getTimeFromGPS(buf,len,&mInfo);
    getPlaceFromGPS(buf,len,&mInfo);
    memset(degreeBuf,0,sizeof(degreeBuf));
    sprintf(degreeBuf,"%10.6f",mInfo.latitude.gpsDeg);
    if(mInfo.NS=='N'){
        str = QString("北纬 %1°").arg(degreeBuf);
    }else {
        str = QString("南纬 %1°").arg(degreeBuf);
    }
    ui->LA_BD_LA->setText(str);

    memset(degreeBuf,0,sizeof(degreeBuf));
    sprintf(degreeBuf,"%10.6f",mInfo.longitude.gpsDeg);
    if(mInfo.EW=='E'){
        str = QString("东经 %1°").arg(degreeBuf);
    }else {
        str = QString("西经 %1°").arg(degreeBuf);
    }
    ui->LA_BD_LO->setText(str);
    //show_gps(&mInfo);
}

void QEleCtrl::handleRecvCommBuf(LS_S32 index,void *p,LS_S32 len)
{
    QString str="";
    LS_U8 *buf = (LS_U8 *)p;
    //printBufByHex(__FILE__,__LINE__,buf,10);
    //ls_info_debug("%d %d",index,len);
    LS_S8 flagBuf[10]={0};
    if(index < 0 || index >= ELE_MAX_NUM){
        ls_err_debug("index %d ERR",index);
        return;
    }
    handlComMtx.lock();
    memset(flagBuf,0,sizeof(flagBuf));
    memcpy(flagBuf,(char*)(buf+1),5);
    if(strcmp(flagBuf,"PARAM")==0){
        emit this->mainShowPara(index,buf);
        handlComMtx.unlock();
        return;
        //str = QString("查询设备%1参数").arg(index+1);
    }else if(strcmp((char *)flagBuf,"STATU")==0){
        emit this->mainShowStatus(index,buf);
        handlComMtx.unlock();
        return;
        //str = QString("查询设备%1状态").arg(index+1);
    }else if(strcmp(flagBuf,"LDPAR")==0){
        str = QString("设备%1 LD电流设置").arg(index+1);
    }else if(strcmp(flagBuf,"LDPTE")==0){
        str = QString("设备%1 采集判别设置").arg(index+1);
    }else if(strcmp(flagBuf,"SEPAR")==0){
        str = QString("设备%1 Laser温度设置").arg(index+1);
    }else if(strcmp(flagBuf,"RANKS")==0){
        str = QString("设备%1 采样平均数设置").arg(index+1);
    }else if(strcmp(flagBuf,"TIROL")==0||strcmp(flagBuf,"THROL")==0){
        str = QString("设备%1 采集阈值设置").arg(index+1);
    }else if(strcmp(flagBuf,"PRESH")==0){
        str = QString("设备%1 压力高阈值设置").arg(index+1);
    }else if(strcmp(flagBuf,"PRESL")==0){
        str = QString("设备%1 压力低阈值设置").arg(index+1);
    }else if(strcmp(flagBuf,"VOLTA")==0){
        if(buf[6]==0x01){
            str = QString("设备%1 DA-A通道设置").arg(index+1);
        }else if(buf[6]==0x02){
            str = QString("设备%1 DA-B通道设置").arg(index+1);
        }else if(buf[6]==0x03){
            str = QString("设备%1 DA-C通道设置").arg(index+1);
        }
    }else if(strcmp(flagBuf,"PUMPA")==0){
        str = QString("设备%1 激光器换气设置").arg(index+1);
    }else if(strcmp(flagBuf,"RESET")==0){
        str = QString("设备%1 复位设置").arg(index+1);
    }else if(strcmp(flagBuf,"RUNTO")==0){
        str = QString("设备%1 启动/停止设置").arg(index+1);
    }else if(strcmp(flagBuf,"SAVEP")==0){
        str = QString("设备%1 存储配置设置").arg(index+1);
    }else if(strcmp(flagBuf,"REQUE")==0){
        str = QString("设备%1 获取信息").arg(index+1);
        handlComMtx.unlock();
        return;
    }else if(strcmp(flagBuf,"SQUAN")==0){
        str = QString("设备%1 采集次数设置").arg(index+1);
    }else if(strcmp(flagBuf,"WOMOD")==0){
        str = QString("设备%1 工作模式设置").arg(index+1);
    }else if(strcmp(flagBuf,"SWITC")==0){
        str = QString("设备%1 设备开关设置").arg(index+1);
    }
    if(buf[8]==0x4F && buf[9]==0x4B){
        str += "成功";
        emit mainShowMsg(str,MSG_INFO);
    }else if(buf[8]==0x45 && buf[9]==0x52){
        str += "失败";
        emit mainShowMsg(str);
    }
    handlComMtx.unlock();
}

void QEleCtrl::handleRecvDataBuf(LS_S32 index,void *p,LS_S32 len)
{
    QString str;
    LS_U8 *buf = (LS_U8 *)p;
    char valueBuf[1024]={0};
    double dValue[DATA_COUNT]={0};
    float  fConValue[3]={0};
    double *dp = dValue;
    if(index < 0 || index >= ELE_MAX_NUM){
        ls_err_debug("index %d ERR",index);
        return;
    }
    if(len != DATA_LEN){
        ls_err_debug("recv data len %d ERR",index);
        //str = QString("收到数据长度%1 != %2").arg(len).arg(DATA_LEN);
        //emit mainShowMsg(str);
        return;
    }
    handlDataMtx.lock();
    for(int i=5;i<DATA_LEN-5;i+=2){
        int num = Hex2Int(buf+i,2);
        *dp++=(double)num;
    }
    iGlobalSaveCount[index]++;
    iGlobalCheckCount[index]++;
    dEleChart->eleChartDrawLines(index,dValue);
    memset(&fConValue,0,sizeof(fConValue));
    countCouncent(dValue,fConValue);
    //ls_info_debug("%.3f",fConValue[1]);
    drawLine(index,fConValue[1]);
    mSave[index]->saveRecvPressData(buf,len);
    mSave[index]->saveRecvPressValue(dValue,DATA_COUNT);
    mSave[index]->saveConcentValue(iGlobalSaveCount[index],fGlobalTempValue[index],fConValue);
    handlDataMtx.unlock();
}

void QEleCtrl::handleRecvTempBuf(LS_S32 index,void *p,LS_S32 len)
{
    QString str;
    LS_S8 buf[1024] = {0};
    LS_S8 tcoeBuf[]="TC1:TCOE";
    LS_S8 ackBuf[]="CMD:REPLY=";
    LS_S8 actualTempBuf[]="TC1:TCACTUALTEMP";
    LS_S8 actualOutVolBuf[]="TC1:TCACTUALVOLTAGE";
    LS_S8 actualOutCurBuf[]="TC1:TCACTUALCURRENT";
    LS_S8 outProVolCheckBuf[]="TC1:TCOVPV";
    LS_S8 outProCurCheckBuf[]="TC1:TCOCPC";
    LS_S8 outTemProCheckBuf[]="TC1:TCOTEF";
    LS_S8 *tmp=nullptr;
    LS_F64 fValue=0;
    LS_S32 actualTempLen = strlen(actualTempBuf);
    LS_S32 tcoeLen = strlen(tcoeBuf);
    LS_S32 ackLen = strlen(ackBuf);
    if(index < 0 || index >= ELE_MAX_NUM){
        ls_err_debug("index %d ERR",index);
        return;
    }
    handlTempMtx.lock();
    memset(buf,0,len);
    memcpy(buf,p,len);
    //printBufByHex(__FILE__,__LINE__,buf,len);
    //ls_info_debug("%d recv:%d %s!",index,len,buf)
    if(tmp = strstr(buf,ackBuf)){
        fValue = atof(tmp+(ackLen));
        if(fabs(fValue-1.0)<1e-5){
            str = QString("温控%1 设置失败").arg(index);
            emit mainShowMsg(str);
        }
    }
    if(tmp = strstr(buf,tcoeBuf)){
        static bool bTempOutout[2]={false,false};
        fValue = atof(tmp+(tcoeLen+1));
        if(fabs(fValue-0)<1e-5){
            if(bTempOutout[index]){
                if(index==Ele_DevID_0)
                    ui->LA_temp0_online_light->setAlarm(Alarm_gray);
                else
                    ui->LA_temp1_online_light->setAlarm(Alarm_gray);
            }
            bTempOutout[index]=false;
        }else{
            if(!bTempOutout[index]){if(index==Ele_DevID_0)
                    ui->LA_temp0_online_light->setAlarm(Alarm_green);
                else
                    ui->LA_temp1_online_light->setAlarm(Alarm_green);
            }
            bTempOutout[index]=true;
        }
    }
    if(tmp = strstr(buf,actualTempBuf)){
        fValue = atof(tmp+(actualTempLen+1));
        char fValueBuf[64]={0};
        memset(fValueBuf,0,sizeof(fValueBuf));
        sprintf(fValueBuf,"%.3f",fValue);
        QString str = "";
        str = QString("设备%1温度%2°").arg(index+1).arg(fValueBuf);
        if(index==Ele_DevID_0){
            ui->LA_temp0_value->setText(str);
        }else{
            ui->LA_temp1_value->setText(str);
        }
    }
    if(tmp = strstr(buf,actualOutVolBuf)){
        fValue = atof(tmp+(strlen(actualOutVolBuf)+1));
        dTempCtrl->QTempShowOutVoltage(index,fValue);
    }
    if(tmp = strstr(buf,outProVolCheckBuf)){
        fValue = atof(tmp+(strlen(outProVolCheckBuf)+1));
        dTempCtrl->QTempShowProVoltage(index,fValue);
    }
    if(tmp = strstr(buf,actualOutCurBuf)){
        fValue = atof(tmp+(strlen(actualOutCurBuf)+1));
        dTempCtrl->QTempShowOutCurrent(index,fValue);
    }
    if(tmp = strstr(buf,outProCurCheckBuf)){
        fValue = atof(tmp+(strlen(outProCurCheckBuf)+1));
        dTempCtrl->QTempShowProCurrent(index,fValue);
    }
    if(tmp = strstr(buf,outTemProCheckBuf)){
        static int iErrCount = 0;
        int ret = atoi(tmp+(strlen(outTemProCheckBuf)+1));
        if(ret==0){
            iErrCount ++;
            if(iErrCount>0){
                fValue=0.0;
            }
        }else{
            iErrCount = 0;
        }
    }
    handlTempMtx.unlock();
}

void QEleCtrl::on_RB_ELE_0_clicked()
{
    dEleSysCtrl->setCtrlDev(Ele_DevID_0);
    dEleSwitch->setCtrlDev(Ele_DevID_0);
}

void QEleCtrl::on_RB_ELE_1_clicked()
{
    dEleSysCtrl->setCtrlDev(Ele_DevID_1);
    dEleSwitch->setCtrlDev(Ele_DevID_1);
}

void QEleCtrl::on_RB_ELE_BOTH_clicked()
{
    dEleSysCtrl->setCtrlDev(Ele_DevID_All);
    dEleSwitch->setCtrlDev(Ele_DevID_All);
}
