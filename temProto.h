#ifndef TEM_PROTO_H
#define TEM_PROTO_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "util.h"

int getTempActualValue(LS_S8 *buf);

int getTempOutVolValue(LS_S8 *buf);

int getTempOutCurValue(LS_S8 *buf);

int getTemProRet(LS_S8 *buf);

int getTempOutputModel(LS_S8 *buf);

int setTempAdjustValue(LS_S8 *buf,float value);

int setTempMaxValue(LS_S8 *buf,float value);

int setTempMinValue(LS_S8 *buf,float value);

int setTempOtpLT(LS_S8 *buf,float value);

int setTempOtpHT(LS_S8 *buf,float value);

int setTemKeyOnff(LS_S8 *buf,int onoff);

int setTempProVolValue(LS_S8 *buf,float fValue);

int getTempProVolValue(LS_S8 *buf);

int setTempProCurValue(LS_S8 *buf,float fValue);

int getTempProCurValue(LS_S8 *buf);

#endif // TEMPPROTO_H
